# Project: Protos22-Prod

A multi-threaded version of the Monte Carlo event egenrator Protos (PROgram for TOp Simulations) is presented. This is based on [Protos 2.2](http://jaguilar.web.cern.ch/jaguilar/protos/) and includes:

* Single top-quark and top-quark pair production with anomalous Wtb couplings
* Top-quark pair production with FCN decays and single FCN top-quark production
* Top-quark processes with four-fermion effective operators
* Heavy vector-like quark production
* Triada 1.1: a generator for seesaw messengers (and more)

There are two executables: ```protos-jfb``` and ```toAtlas```. The first generates data in a binary format. The second converts it to the standard ASCII format.

This version requires C++11. Note that gcc 4.8.1 was the first feature-complete implementation of the 2011 C++ standard.

## Getting started

Follow these instructions to install and compile this project.

### How to compile and run on lxplus

Follow these instructions to install and compile the Protos22-Prod package.

```
setupATLAS --quiet
lsetup git
lsetup root
git clone https://:@gitlab.cern.ch:8443/$USER/Protos22-Prod.git
```

Note that you may need to fork the Protos22-Prod project if you are planning to contribute to our code development. Otherwise, use ```git clone https://:@gitlab.cern.ch:8443/boudreau/Protos22-Prod.git```.

Now you can compile ```toAtlas``` and run a test by simply doing: 

```
cd Protos22-Prod
make toAtlas
./toAtlas test/test10K24R01.dat 1 1000 > outputFile.ascii
head -8 outputFile.ascii
tail -n 8 outputFile.ascii
```

If then running ```toAtlas``` the error ```'GLIBCXX_3.4.21' not found``` is shown, remember that gcc 4.8.1 is mandatory for compiling C++11 code (therefore, do ```setupATLAS --quiet; lsetup root```).

For preparing ATLAS-standard output files, you can use the script ```prepareDS.py```. One can run a simple test by doing:

```
python prepareDS.py -d
```

This will produced the standard input dataset (with 48 files; 5k events per file) from a binary test file containing 240k unweighted events, found in the ```test``` directory. For more options, just do ```python prepareDS.py -h```.

For compiling properly ```protos-jfb```, LHAPDF is mandatory. You can use the script ```compileLHAPDF.py```:

```
python compileLHAPDF.py
```

## For developers

### Configuring a remote for your fork

You must configure a remote that points to the upstream repository in GitLab to sync changes you make in your fork with the original repository. This also allows you to sync changes made in the original repository with your fork.

To list the current configured remote repository for your fork, just type:

```
git remote -v
```

To specify a (new) remote upstream repository that will be synced with the fork:
```
git remote add upstream https://:@gitlab.cern.ch:8443/boudreau/Protos22-Prod.git
```

### Syncing your fork with the upstream

You should sync your fork of a repository to keep it up-to-date with the upstream repository. So, you have to fetch the branches and their respective commits from the upstream repository. Commits to master will be stored in a local branch, upstream/master. Then, you can pull changes from the upstream master to your local master.

```
git fetch upstream
git pull upstream master
```

Use ```git pull upstream master -p``` or ```git pull upstream master -f``` in order to prune tracking branches no longer on remote or force overwrite of local branch, respectively.