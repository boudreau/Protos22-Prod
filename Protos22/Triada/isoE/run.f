      SUBROUTINE CHGPAR(id,step)
      IMPLICIT NONE

!     Arguments

      INTEGER id
      REAL*8 step

!     Parameters that can be changed

      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 mE,GE
      COMMON /Smass/ mE,GE
      REAL*8 VlN(3)
      COMMON /Scoup/ VlN

      IF (id .EQ. -1) THEN
        idum0=idum0+1
      ELSE IF (id .EQ. 1) THEN
        mE=mE+step
      ELSE IF (id .EQ. 2) THEN
        VlN(1)=VlN(1)+step
      ELSE IF (id .EQ. 3) THEN
        VlN(2)=VlN(2)+step
      ELSE IF (id .EQ. 4) THEN
        VlN(3)=VlN(3)+step
      ELSE
        print *,id
        PRINT 100
        STOP
      ENDIF

      RETURN

100   FORMAT ('Unsupported parameter scan')
      END


      SUBROUTINE LOGINI(IMODE,id)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE,id

!     For logging

      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot
      REAL*8 mE,GE
      COMMON /Smass/ mE,GE
      REAL*8 VlN(3)
      COMMON /Scoup/ VlN

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l

!     Local

      REAL*8 scpar

      IF (IMODE .EQ. -1) THEN 
        OPEN (40,FILE='output/'//PROCNAME(1:l)//'.log',
     &    status='unknown')
        RETURN
      ELSE IF (IMODE .EQ. 1) THEN
        CLOSE (40)
        RETURN
      ELSE IF (IMODE .NE. 0) THEN
        PRINT 99
        STOP
      ENDIF

!     Write results

      IF ((id .EQ. 0) .OR. (id .EQ. -1)) THEN
        scpar=0d0
      ELSE IF (id .EQ. 1) THEN
        scpar=mE
      ELSE IF (id .EQ. 2) THEN
        scpar=VlN(1)
      ELSE IF (id .EQ. 3) THEN
        scpar=VlN(2)
      ELSE IF (id .EQ. 4) THEN
        scpar=VlN(3)
      ELSE
        PRINT 100
        STOP
      ENDIF

      WRITE (40,200) scpar,SIG_tot

      RETURN
99    FORMAT ('Error in WRITELOG call')
100   FORMAT ('Unsupported parameter scan')
200   FORMAT (F7.2,' ',D12.6)
      END


