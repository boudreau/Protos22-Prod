      REAL*8 FUNCTION QQ_D2D2(P1,P2,P3a,P3b,P4a,P4b,NHEL)

!     FOR PROCESS : q qbar  -> D++ D--, D++ D-, D-- D+, D+ D-

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

C     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(3),W4(3),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 MD,GD
      COMMON /Dmass/ MD,GD
      COMPLEX*16 Y(3,3)
      REAL*8 Ysum
      COMMON /DLcoup/ Y,Ysum
      INTEGER IP,IL1,IL2,IL3,IL4,IQ
      COMMON /Dflags/ IP,IL1,IL2,IL3,IL4,IQ

!     Couplings and other

      REAL*8 GWF(2),GZqq(2),GAqq(2)
      COMPLEX*16 GZDD,GADD,GWDD,GDll1(2),GDll2(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 mL1,mL2,mL3,mL4

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  
      DATA EIGEN_VEC(2,1) /1d0 /                  

!     -------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     Coupling of Z/gamma to initial quarks

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0
      
      IF (IQ .EQ. 1) THEN                   ! u,c
        GZqq(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
        GZqq(2)=gcwsw2*2d0/3d0
        GAqq(1)=-2d0/3d0*e
        GAqq(2)=-2d0/3d0*e
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZqq(1)=-gcw*(-1d0/2d0+sw2/3d0)
        GZqq(2)=-gcwsw2/3d0
        GAqq(1)=1d0/3d0*e
        GAqq(2)=1d0/3d0*e
      ELSE
        PRINT *,'Wrong IQ in matr.f'
        STOP
      ENDIF

!     Triplet coupling to gauge bosons

      IF (IP .EQ. 1) THEN            ! Z D++ D--, gamma D++ D--
        GZDD=-gcw*(1d0-2d0*sw2)
        GADD=-2d0*e
      ELSE IF (IP .EQ. 2) THEN       ! W+ -> D++ D- / W- ->  D+ D-- (-1)*(-1)
        GWDD=-g
      ELSE IF (IP .EQ. 3) THEN       ! Z D+ D-, gamma D+ D-
        GZDD=gcw*sw2
        GADD=-e
      ENDIF

!     Triplet coupling to leptons

      IF (IP .EQ. 1) THEN
        GDll1(1)=-2d0*Y(IL1,IL2)                    ! D++
        GDll1(2)=0d0
        GDll2(1)=0d0                                ! D--
        GDll2(2)=-2d0*CONJG(Y(IL3,IL4))
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 1)) THEN
        GDll1(1)=-2d0*Y(IL1,IL2)                    ! D++
        GDll1(2)=0d0
        GDll2(1)=0d0                                ! D-
        GDll2(2)=SQRT(2d0)*CONJG(Y(IL3,IL4))
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 2)) THEN
        GDll1(1)=SQRT(2d0)*Y(IL1,IL2)               ! D+
        GDll1(2)=0d0
        GDll2(1)=0d0                                ! D--
        GDll2(2)=-2d0*CONJG(Y(IL3,IL4))
      ELSE IF (IP .EQ. 3) THEN
        GDll1(1)=SQRT(2d0)*Y(IL1,IL2)               ! D+
        GDll1(2)=0d0
        GDll2(1)=0d0                                ! D-
        GDll2(2)=SQRT(2d0)*CONJG(Y(IL3,IL4))
      ELSE
        PRINT *,'Error in matr.f'
      ENDIF

!     Masses of the final state leptons

      mL1=0d0
      mL2=0d0
      mL3=0d0
      mL4=0d0
      IF (IP .EQ. 1) THEN                           !   D1 = D++   D2 = D--
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL2 .EQ. 3) mL2=mtau
        IF (IL3 .EQ. 3) mL3=mtau
        IF (IL4 .EQ. 3) mL4=mtau
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 1)) THEN  !   D1 = D++   D2 = D-
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL2 .EQ. 3) mL2=mtau
        IF (IL3 .EQ. 3) mL3=mtau
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 2)) THEN  !   D1 = D+    D2 = D--
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL2 .EQ. 3) mL2=mtau
        IF (IL3 .EQ. 3) mL3=mtau
      ELSE IF (IP .EQ. 3) THEN                      !   D1 = D+    D2 = D-
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL3 .EQ. 3) mL3=mtau
      else
      print *,'error'
      ENDIF

!     ------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar
      
!     D++ / D+ decay

      CALL IXXXXX(P3a,mL1,NHEL(3),-1,W3a)            ! l1+
      CALL OXXXXX(P3b,mL2,NHEL(4),1,W3b)             ! l2+ reversed or nu2
      CALL HIOXXX(W3a,W3b,GDll1,MD,GD,W3)

!     D-- / D- decay

      CALL OXXXXX(P4a,mL3,NHEL(5),1,W4a)             ! l3-
      CALL IXXXXX(P4b,mL4,NHEL(6),-1,W4b)            ! l4- reversed or nu4
      CALL HIOXXX(W4b,W4a,GDll2,MD,GD,W4)

      IF ((IP .EQ. 1) .OR. (IP .EQ. 3)) THEN
        CALL JIOXXX(W1,W2,GZqq,MZ,GZ,W5)
        CALL VSSXXX(W5,W4,W3,GZDD,AMP(1))
        CALL JIOXXX(W1,W2,GAqq,0d0,0d0,W6)
        CALL VSSXXX(W6,W4,W3,GADD,AMP(2))
      ELSE
        CALL JIOXXX(W1,W2,GWF,MW,GW,W5)
        CALL VSSXXX(W5,W4,W3,GWDD,AMP(1))
        AMP(2)=0d0
      ENDIF


      QQ_D2D2 = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          QQ_D2D2 = QQ_D2D2 + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END




