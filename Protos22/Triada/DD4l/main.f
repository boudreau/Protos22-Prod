      PROGRAM Triada
!
!     Generator for fermion triplet production
!
!     By J. A. Aguilar-Saavedra, May 2008
!
!     Includes adapted VEGAS for multigrid integration
!     Uses HELAS for matrix element evaluation
!
      IMPLICIT NONE
      
!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions used

      REAL*8 FXN
      INTEGER LONGITUD
      EXTERNAL FXN,LONGITUD

!     Flags

      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      INTEGER IP,IL1,IL2,IL3,IL4,IQ
      COMMON /Dflags/ IP,IL1,IL2,IL3,IL4,IQ

!     Input triplet parameters 

      REAL*8 MD,GD
      COMMON /Dmass/ MD,GD

!     Other input data

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     For VEGAS

      REAL*8 XL(MAXDIM),XU(MAXDIM),ACC,AVG1,SD1,CHI2A1
      INTEGER NCALL,ITMX,NPRN,ND
      COMMON/BVEG1/XL,XU,ACC,ND,NCALL,ITMX,NPRN

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC
      REAL*8 GRWGT(MAXGR)
      COMMON /multigr2/ GRWGT
      REAL*8 GRAPROB(0:MAXGR)
      COMMON /multigr3/ GRAPROB

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For BR check

      REAL*8 countL(3,3)
      common /testdec/ countL

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     For logging

      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     For ran2 calls

      INTEGER idum0
      COMMON /seed_ini/ idum0

!     For parameter scan

      INTEGER ISCAN
      REAL*8 SCANSTEP

!     Local

      INTEGER i,j
      INTEGER ILOADGRID,ISAVEGRID,NCALL1,NCALL2,ITMX1,ITMX2
      INTEGER IEVTOUT,IPLOT,ILOG,IPRSUB,IPRAS,IVERB
      REAL*8 GRPROB(NPROC)
      REAL*8 ncount

!     --------------------------------------------

      OPEN (32,FILE='input/run.dat',status='old')
      READ (32,*) PROCNAME
      l=LONGITUD(PROCNAME)
      READ (32,*) IP
      READ (32,*) ET
      READ (32,*) IPPBAR
      READ (32,*) NRUNS
      READ (32,*) ILOADGRID,ISAVEGRID
      READ (32,*) NCALL1,ITMX1
      READ (32,*) NCALL2,ITMX2
      READ (32,*) idum0
      READ (32,*) ICUT
      READ (32,*) IEVTOUT,IPLOT,ILOG
      READ (32,*) IVERB,IPRSUB,IPRAS
      READ (32,*) MD
      READ (32,*) ISCAN,SCANSTEP
      READ (32,*) IPDSET
      READ (32,*) QFAC
      CLOSE (32)

!     Check parameter consistency

      IF ((IP .LT. 1) .OR. (IP .GT. 3)) THEN
        PRINT 985
        STOP
      ENDIF

      IF ((IEVTOUT .NE. 0) .AND. (NRUNS .GT. 99)) THEN
        PRINT 986
        STOP
      ENDIF

!     Override settings for special cases

      IF (NRUNS .GT. 1) THEN
        ISAVEGRID=0
      ENDIF
!
!     BEGIN
!

      ND=NDIM
      CALL INITPAR

      IF (ILOG .NE. 0) CALL LOGINI(-1,0)

      DO IRUN=1,NRUNS                     !   Different runs

      IF (NRUNS .GT. 1) THEN
        PRINT 1110,IRUN
        PRINT 999
      ENDIF

      CALL REINITPAR
      CALL RAN2RESET(idum0)

      IF (IEVTOUT .EQ. 1) CALL EVTOUT(-1)
      CALL PLOTINI(-1)
      CALL ASINI(-1)

!     Check BR

      DO i=1,3
        DO j=1,3
          countL(i,j)=0d0
        ENDDO
      ENDDO

!     First integration: warmup

      DO i=1,ND
        XL(i)=0d0
        XU(i)=1d0
      ENDDO
      ACC=-1d0

      IF (ILOADGRID .EQ. 1) GOTO 20

      DO i=1,MAXGR
        SIG(i)=0d0
        SIG2(i)=0d0
      ENDDO

      IRECORD=0
      IWRITE=0
      IHISTO=0
      NCALL=NCALL1
      ITMX=ITMX1
      NPRN=0
      WT_ITMX=1d0/FLOAT(ITMX)

      INITGRID=1
      DO IPROC=1,NPROC
      PRINT 1100,IPROC
        CALL VEGAS(FXN,AVG1,SD1,CHI2A1)
        SIG(IPROC)=AVG1
      ENDDO

      SIG_tot=0d0
      DO IPROC=1,NPROC
        SIG_tot=SIG_tot+SIG(IPROC)
      ENDDO
      
      GRAPROB(0)=0d0
      DO i=1,MAXGR
        GRPROB(i)=SIG(i)/SIG_tot
        GRAPROB(i)=GRAPROB(i-1)+GRPROB(i)
        IF (GRPROB(i) .GT. 0d0) THEN
          GRWGT(i)=1d0/GRPROB(i)
        ELSE
          GRWGT(i)=0d0
        ENDIF
      ENDDO
      IF (ABS(GRAPROB(NPROC)-1d0) .GT. 1d-8) THEN
        PRINT 999
        PRINT 987,1d0-graprob(nproc)
      ENDIF
      GRAPROB(NPROC)=1d0

      IF (ISAVEGRID .EQ. 1) THEN
        OPEN (33,file='input/grid.ini',status='unknown')
        CALL SAVE(ND)
        CLOSE (33)
        OPEN (33,file='input/proc.ini',status='unknown')
        WRITE (33,2010) GRAPROB
        WRITE (33,2011) GRWGT
        CLOSE (33)
      ENDIF
      
20    CONTINUE

!     Second integration run

      DO i=1,MAXGR
        SIG(i)=0d0
        SIG2(i)=0d0
	ERR(i)=0d0
      ENDDO
      NIN=0
      NOUT=0

      IF (ILOADGRID .EQ. 1) THEN
        OPEN (33,file='input/grid.ini',status='old')
        CALL RESTR(ND)
        CLOSE (33)
        OPEN (33,file='input/proc.ini',status='old')
        READ (33,2010) GRAPROB
        READ (33,2011) GRWGT
        CLOSE (33)
      ENDIF

      IRECORD=1
      IF (IEVTOUT .EQ. 1) IWRITE=1
      IF ((IPLOT .NE. 0) .OR. (ILOG .NE. 0) .OR. (IPRAS .NE. 0))
     &   IHISTO=1
      NCALL=NCALL2
      ITMX=ITMX2
      NPRN=IVERB
      WT_ITMX=1d0/FLOAT(ITMX)
      INITGRID=0
      CALL VEGAS1(FXN,AVG1,SD1,CHI2A1)

      SIG_tot=SIG(1)+SIG(2)+SIG(5)+SIG(6)
     &  + SIG(3)+SIG(4)+SIG(7)+SIG(8)
      ERR_tot=ERR(1)+ERR(2)+ERR(5)+ERR(6)
     &  + ERR(3)+ERR(4)+ERR(7)+ERR(8)
      PRINT 990

      IF ((IPRAS .NE. 0) .OR. (ILOG .NE. 0)) CALL ASINI(1)
      IF (IPRAS .NE. 0) CALL ASINI(2)
      IF (ILOG .NE. 0) CALL LOGINI(0,ISCAN)
      IF (IPLOT .EQ. 1) CALL PLOTINI(1)
      IF (IEVTOUT .EQ. 1) CALL EVTOUT(1)

      ncount=0
      DO i=1,3
        DO j=1,3
          ncount=ncount+countL(i,j)
        ENDDO
      ENDDO
      PRINT 1305
      PRINT 999
      PRINT 1306
      PRINT 1307,(countL(1,i)/ncount,i=1,3)
      PRINT 1308,(countL(2,i)/ncount,i=1,3)
      PRINT 1309,(countL(3,i)/ncount,i=1,3)
      PRINT 999

      IF ((IPRSUB .EQ. 1) .AND. ((IP .EQ. 1) .OR. (IP .EQ. 3))) THEN
        PRINT 1140,SIG(1)+SIG(5),ERR(1)+ERR(5)
        PRINT 1141,SIG(2)+SIG(6),ERR(2)+ERR(6)
        PRINT 1142,SIG(3)+SIG(7),ERR(3)+ERR(7)
        PRINT 1143,SIG(4)+SIG(8),ERR(4)+ERR(8)
        PRINT 999
      ELSE IF ((IPRSUB .EQ. 1) .AND. (IP .EQ. 2)) THEN
        PRINT 1240,SIG(1)+SIG(5),ERR(1)+ERR(5)
        PRINT 1241,SIG(2)+SIG(6),ERR(2)+ERR(6)
        PRINT 1242,SIG(3)+SIG(7),ERR(3)+ERR(7)
        PRINT 1243,SIG(4)+SIG(8),ERR(4)+ERR(8)
        PRINT 999
      ENDIF
      PRINT 1130,SIG_tot,ERR_tot
      PRINT 990
     
      IF (ISCAN .NE. 0) CALL CHGPAR(ISCAN,SCANSTEP)

      ENDDO                                             ! End of runs

      IF (ILOG .NE. 0) CALL LOGINI(1,0)

      STOP

985   FORMAT ('ERROR: incorrect IP')
986   FORMAT ('ERROR: NRUNS out of range')
987   FORMAT ('Warning: in initialisation acc sum = ',D9.4)
990   FORMAT ('===================================================')
995   FORMAT ('---------------------------------------------------')
999   FORMAT ('')
1100  FORMAT ('Initialising grid ',I2,'...')
1110  FORMAT ('Run: ',I2)
1120  FORMAT ('Kinematical cuts: ',I1)
1130  FORMAT ('Total xsec = ',D9.4,' +- ',D9.4,' fb')

1140  FORMAT ('xsec uu~   = ',D10.4,' +- ',D9.4)
1141  FORMAT ('xsec dd~   = ',D10.4,' +- ',D9.4)
1142  FORMAT ('xsec cc~  = ',D10.4,' +- ',D9.4)
1143  FORMAT ('xsec ss~  = ',D10.4,' +- ',D9.4)
1240  FORMAT ('xsec ud~   = ',D10.4,' +- ',D9.4)
1241  FORMAT ('xsec du~   = ',D10.4,' +- ',D9.4)
1242  FORMAT ('xsec cs~  = ',D10.4,' +- ',D9.4)
1243  FORMAT ('xsec sc~  = ',D10.4,' +- ',D9.4)
1305  FORMAT ('Fraction of generated lepton flavours')
1306  FORMAT ('         e       mu     tau')
1307  FORMAT (' e    ',F6.4,'  ',F6.4,'  ',F6.4)
1308  FORMAT (' mu   ',F6.4,'  ',F6.4,'  ',F6.4)
1309  FORMAT (' tau  ',F6.4,'  ',F6.4,'  ',F6.4)
2010  FORMAT (F8.6)
2011  FORMAT (D12.6)

      END
