      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 MD,GD
      COMMON /Dmass/ MD,GD
      REAL*8 BRch(9),BRch_ac(0:9),WTch(9)
      COMMON /Ddecay/ BRch,BRch_ac,WTch
      REAL*8 NGD
      COMMON /BWdata/ NGD
      REAL*8 pi,ET
      COMMON /const/ pi,ET

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      INTEGER IP,IL1,IL2,IL3,IL4,IQ
      COMMON /Dflags/ IP,IL1,IL2,IL3,IL4,IQ
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4
      REAL*8 PD1(0:3),PD2(0:3)
      COMMON /MOMINT/ PD1,PD2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 countL(3,3)
      COMMON /testdec/ countL

!     Local variables

      REAL*8 EPS,y1,y2,flux
      REAL*8 PCM_LAB(0:3)
      REAL*8 QD1,QD2,ml1,ml2,ml3,ml4,m1,m2
      REAL*8 WT1,WT2,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS
      REAL*8 RCH,ICH

!     --------------------------------------------

      WTPS=0d0

      IDIR=0
      IF (IPROC .GT. 4) IDIR=1
      IF ((IPROC .EQ. 1) .OR. (IPROC .EQ. 3) .OR. (IPROC .EQ. 5)
     &   .OR. (IPROC .EQ. 7)) THEN
        IQ=1        !  u u~  c c~ for IP=1,3 /  u d~   c s~ for IP=2
      ELSE
        IQ=2        !  d d~  s s~ for IP=1,3 /  d u~   s c~ for IP=2
      ENDIF

!     Select final state

      WTFS=1d0

      RCH=ran2(idum)
      ICH=0
      DO WHILE (BRch_ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      WTFS=WTFS*WTch(ICH)
      IL1=(ICH-1)/3+1
      IL2=MOD(ICH-1,3)+1

      countL(IL1,IL2)=countL(IL1,IL2)+1d0

      RCH=ran2(idum)
      ICH=0
      DO WHILE (BRch_ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      WTFS=WTFS*WTch(ICH)
      IL3=(ICH-1)/3+1
      IL4=MOD(ICH-1,3)+1

      countL(IL3,IL4)=countL(IL3,IL4)+1d0
      
      mL1=0d0
      mL2=0d0
      mL3=0d0
      mL4=0d0
      IF (IP .EQ. 1) THEN                           !   D1 = D++   D2 = D--
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL2 .EQ. 3) mL2=mtau
        IF (IL3 .EQ. 3) mL3=mtau
        IF (IL4 .EQ. 3) mL4=mtau
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 1)) THEN  !   D1 = D++   D2 = D-
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL2 .EQ. 3) mL2=mtau
        IF (IL3 .EQ. 3) mL3=mtau
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 2)) THEN  !   D1 = D+    D2 = D--
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL2 .EQ. 3) mL2=mtau
        IF (IL3 .EQ. 3) mL3=mtau
      ELSE IF (IP .EQ. 3) THEN                      !   D1 = D+    D2 = D-
        IF (IL1 .EQ. 3) mL1=mtau
        IF (IL3 .EQ. 3) mL3=mtau
      ELSE
        PRINT *,'Error in genmom'
        STOP
      ENDIF

!     Generation of the masses of the virtual t, W
      
      CALL BREIT(MD,GD,NGD,QD1,WT1)
      CALL BREIT(MD,GD,NGD,QD2,WT2)
      WT_BREIT=WT1*WT2*(2d0*pi)**6

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(QD1+QD2)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system

      m1=0d0
      m2=0d0
      
      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Generation

      CALL PHASE2sym(SQRT(s),QD1,QD2,PCM_LAB,PD1,PD2,WT1)
      WT_PROD=(2d0*pi)**4*WT1

      CALL PHASE2(QD1,ml1,ml2,PD1,P1,P2,WT)                     ! D++ decay
      WT_DEC=WT

      CALL PHASE2(QD2,ml3,ml4,PD2,P3,P4,WT)                 ! D-- decay
      WT_DEC=WT_DEC*WT

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux
      
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     --------------------------------------------

      INOT=1


98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 QQ_D2D2

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 MD,GD
      COMMON /Dmass/ MD,GD
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4
      INTEGER IP,IL1,IL2,IL3,IL4,IQ
      COMMON /Dflags/ IP,IL1,IL2,IL3,IL4,IQ
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(16*NPROC,3**NPART)
      INTEGER ncomb,ntry(16*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=MD*QFAC
     
      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .LT. 0) RETURN

!     q qbar

      IF ((IP .EQ. 1) .OR. (IP .EQ. 3)) THEN
        IF (IPPBAR .EQ. 0) THEN
          STR(1)=fu1*fus2         ! u  u~ 
          STR(2)=fd1*fds2         ! d  d~
          STR(5)=fus1*fu2         ! u~ u
          STR(6)=fds1*fd2         ! d~ d
        ELSE
          STR(1)=fu1*fu2          ! u  u~ 
          STR(2)=fd1*fd2          ! d  d~
          STR(5)=fus1*fus2        ! u~ u
          STR(6)=fds1*fds2        ! d~ d
        ENDIF
        STR(3)=fc1*fc2          ! c  c~
        STR(4)=fs1*fs2          ! s  s~
        STR(7)=fc1*fc2          ! c~ c
        STR(8)=fs1*fs2          ! s~ s
      ELSE
        IF (IPPBAR .EQ. 0) THEN
          STR(1)=fu1*fds2         ! u  d~ 
          STR(2)=fd1*fus2         ! d  u~ 
          STR(5)=fds1*fu2         ! d~ u 
          STR(6)=fus1*fd2         ! u~ d   
        ELSE
          STR(1)=fu1*fd2         ! u  d~ 
          STR(2)=fd1*fu2         ! d  u~ 
          STR(5)=fds1*fus2       ! d~ u 
          STR(6)=fus1*fds2       ! u~ d   
        ENDIF
        STR(3)=fc1*fs2          ! c  s~ 
        STR(4)=fs1*fc2          ! s  c~ 
        STR(7)=fs1*fc2          ! s~ c 
        STR(8)=fc1*fs2          ! c~ s   
      ENDIF

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IL1 .EQ. 3) IAMP=IAMP+NPROC
      IF (IL2 .EQ. 3) IAMP=IAMP+2*NPROC
      IF (IL3 .EQ. 3) IAMP=IAMP+4*NPROC
      IF (IL4 .EQ. 3) IAMP=IAMP+8*NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 500) ntry(IAMP)=500

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 200) THEN
          M1=QQ_D2D2(Q1,Q2,P1,P2,P3,P4,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
     
      RETURN
      END



