      REAL*8 FUNCTION EE_BB(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> E+ E- with decay to W/Z & W/Z

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),P3b(0:3),P3c(0:3)
      REAL*8 P4a(0:3),P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mL,GE,GN
      COMMON /Dmass/ mL,GE,GN
      REAL*8 VlN(3)
      COMMON /Dcoup/ VlN
      INTEGER IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Dflags/ IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GWEN(2),GZEE(2),GZNN(2),GAEE(2)
      REAL*8 GWEv(2),GWlN(2),GZEl(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHEl(2),GHlE(2),GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZqq(2),GAqq(2)
      REAL*8 MB1,GB1,MB2,GB2
      REAL*8 GB1ff(2),GB2ff(2),G3B1(2),G3B2(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  
      DATA EIGEN_VEC(2,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z, gamma coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
        GAqq(1)=-2d0/3d0*e
        GAqq(2)=-2d0/3d0*e
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
        GAqq(1)=1d0/3d0*e
        GAqq(2)=1d0/3d0*e
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        G3B1(1)=GWlN(1)
        G3B1(2)=GWlN(2)
        GB1ff(1)=GWF(1)
        GB1ff(2)=GWF(2)
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        G3B1(1)=GZEl(1)
        G3B1(2)=GZEl(2)
        IF (IF1 .EQ. 0) THEN
          GB1ff(1)=GZvv(1)
          GB1ff(2)=GZvv(2)
        ELSE IF (IF1 .EQ. 1) THEN
          GB1ff(1)=GZll(1)
          GB1ff(2)=GZll(2)
        ELSE IF (IF1 .EQ. 2) THEN
          GB1ff(1)=GZuu(1)
          GB1ff(2)=GZuu(2)
        ELSE IF (IF1 .EQ. 3) THEN
          GB1ff(1)=GZdd(1)
          GB1ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF1 = ',IF1
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in EE_BB'
        STOP
      ENDIF

      IF (IB2 .EQ. 1) THEN
        MB2=MW
        GB2=GW
        G3B2(1)=GWlN(1)
        G3B2(2)=GWlN(2)
        GB2ff(1)=GWF(1)
        GB2ff(2)=GWF(2)
      ELSE IF (IB2 .EQ. 2) THEN
        MB2=MZ
        GB2=GZ
        G3B2(1)=GZEl(1)
        G3B2(2)=GZEl(2)
        IF (IF2 .EQ. 0) THEN
          GB2ff(1)=GZvv(1)
          GB2ff(2)=GZvv(2)
        ELSE IF (IF2 .EQ. 1) THEN
          GB2ff(1)=GZll(1)
          GB2ff(2)=GZll(2)
        ELSE IF (IF2 .EQ. 2) THEN
          GB2ff(1)=GZuu(1)
          GB2ff(2)=GZuu(2)
        ELSE IF (IF2 .EQ. 3) THEN
          GB2ff(1)=GZdd(1)
          GB2ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF2 = ',IF2
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in EE_BB'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar
      
!     B1 decay products: W+ -> u d~ / Z -> f fbar

      CALL OXXXXX(P3b,0d0,NHEL(4),1,W3b)         ! f1:    u, nu  / q, l-, nu
      CALL IXXXXX(P3c,0d0,NHEL(5),-1,W3c)        ! f1bar: d~, l+ / q~, l+, nu~ 
      CALL JIOXXX(W3c,W3b,GB1ff,MB1,GB1,W3d)     ! W+ / Z

!     E+ -> nu W+ / E+ -> l+ Z

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! 
      CALL FVIXXX(W3a,W3d,G3B1,mL,GE,W3)       ! E+

!     ------------------------------------------------

!     B2 decay products: W- -> d u~ / Z -> f fbar
  
      CALL OXXXXX(P4b,0d0,NHEL(7),1,W4b)         ! f2:    d, l-  / q, l-, nu
      CALL IXXXXX(P4c,0d0,NHEL(8),-1,W4c)        ! f2bar: u~,nu~ / q~, l+, nu~
      CALL JIOXXX(W4c,W4b,GB2ff,MB2,GB2,W4d)     ! W- / Z

!     E- -> nu W- / E- -> l- Z

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)       ! 
      CALL FVOXXX(W4a,W4d,G3B2,mL,GE,W4)       ! E-

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZqq,MZ,GZ,W5)
      CALL JIOXXX(W1,W2,GAqq,0d0,0d0,W6)

      CALL IOVXXX(W3,W4,W5,GZEE,AMP(1))
      CALL IOVXXX(W3,W4,W6,GAEE,AMP(2))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)
      AMP(2)=AMP(2)*Vln(IL1)*Vln(IL2)

      EE_BB = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          EE_BB = EE_BB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END




      REAL*8 FUNCTION EE_HB(P1,P2,P3a,PH,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> E+ E- with decay to H & W/Z

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),PH(0:3)
      REAL*8 P4a(0:3),P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mL,GE,GN
      COMMON /Dmass/ mL,GE,GN
      REAL*8 VlN(3)
      COMMON /Dcoup/ VlN
      INTEGER IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Dflags/ IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GWEN(2),GZEE(2),GZNN(2),GAEE(2)
      REAL*8 GWEv(2),GWlN(2),GZEl(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHEl(2),GHlE(2),GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZqq(2),GAqq(2)
      REAL*8 MB2,GB2
      REAL*8 GB2ff(2),G3B2(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  
      DATA EIGEN_VEC(2,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z, gamma coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
        GAqq(1)=-2d0/3d0*e
        GAqq(2)=-2d0/3d0*e
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
        GAqq(1)=1d0/3d0*e
        GAqq(2)=1d0/3d0*e
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .NE. 3) THEN
        PRINT *,'Wrong IB1 = ',IB1,' in EE_HB'
        STOP
      ENDIF

      IF (IB2 .EQ. 1) THEN
        PRINT *,'Wrong IB1 = ',IB1,' in EE_HB'
        STOP
      ELSE IF (IB2 .EQ. 2) THEN
        MB2=MZ
        GB2=GZ
        G3B2(1)=GZEl(1)
        G3B2(2)=GZEl(2)
        IF (IF2 .EQ. 0) THEN
          GB2ff(1)=GZvv(1)
          GB2ff(2)=GZvv(2)
        ELSE IF (IF2 .EQ. 1) THEN
          GB2ff(1)=GZll(1)
          GB2ff(2)=GZll(2)
        ELSE IF (IF2 .EQ. 2) THEN
          GB2ff(1)=GZuu(1)
          GB2ff(2)=GZuu(2)
        ELSE IF (IF2 .EQ. 3) THEN
          GB2ff(1)=GZdd(1)
          GB2ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF2 = ',IF2
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in EE_HB'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar

!     E+ -> l+ H

      CALL SXXXXX(PH,1,W3d)                      ! H
      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! 
      CALL FSIXXX(W3a,W3d,GHlE,mL,GE,W3)       ! E+  (l in E out)

!     ------------------------------------------------

!     B2 decay products: W- -> d u~ / Z -> f fbar
  
      CALL OXXXXX(P4b,0d0,NHEL(5),1,W4b)         ! f2:    d, l-  / q, l-, nu
      CALL IXXXXX(P4c,0d0,NHEL(6),-1,W4c)        ! f2bar: u~,nu~ / q~, l+, nu~
      CALL JIOXXX(W4c,W4b,GB2ff,MB2,GB2,W4d)     ! W- / Z

!     E- -> nu W- / E- -> l- Z

      CALL OXXXXX(P4a,0d0,NHEL(4),1,W4a)       ! 
      CALL FVOXXX(W4a,W4d,G3B2,mL,GE,W4)       ! E-

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZqq,MZ,GZ,W5)
      CALL JIOXXX(W1,W2,GAqq,0d0,0d0,W6)

      CALL IOVXXX(W3,W4,W5,GZEE,AMP(1))
      CALL IOVXXX(W3,W4,W6,GAEE,AMP(2))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)
      AMP(2)=AMP(2)*Vln(IL1)*Vln(IL2)

      EE_HB = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          EE_HB = EE_HB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END




      REAL*8 FUNCTION EE_BH(P1,P2,P3a,P3b,P3c,P4a,PH,NHEL)

!     FOR PROCESS : q q~  -> E+ E- with decay to W/Z & H

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),P3b(0:3),P3c(0:3)
      REAL*8 P4a(0:3),PH(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mL,GE,GN
      COMMON /Dmass/ mL,GE,GN
      REAL*8 VlN(3)
      COMMON /Dcoup/ VlN
      INTEGER IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Dflags/ IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GWEN(2),GZEE(2),GZNN(2),GAEE(2)
      REAL*8 GWEv(2),GWlN(2),GZEl(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHEl(2),GHlE(2),GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZqq(2),GAqq(2)
      REAL*8 MB1,GB1
      REAL*8 GB1ff(2),G3B1(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  
      DATA EIGEN_VEC(2,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z, gamma coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
        GAqq(1)=-2d0/3d0*e
        GAqq(2)=-2d0/3d0*e
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
        GAqq(1)=1d0/3d0*e
        GAqq(2)=1d0/3d0*e
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .EQ. 1) THEN
        PRINT *,'Wrong IB1 = ',IB1,' in EE_BH'
        STOP
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        G3B1(1)=GZEl(1)
        G3B1(2)=GZEl(2)
        IF (IF1 .EQ. 0) THEN
          GB1ff(1)=GZvv(1)
          GB1ff(2)=GZvv(2)
        ELSE IF (IF1 .EQ. 1) THEN
          GB1ff(1)=GZll(1)
          GB1ff(2)=GZll(2)
        ELSE IF (IF1 .EQ. 2) THEN
          GB1ff(1)=GZuu(1)
          GB1ff(2)=GZuu(2)
        ELSE IF (IF1 .EQ. 3) THEN
          GB1ff(1)=GZdd(1)
          GB1ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF1 = ',IF1
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in EE_BH'
        STOP
      ENDIF

      IF (IB2 .NE. 3) THEN
        PRINT *,'Wrong IB2 = ',IB2,' in EE_BH'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar
      
!     B1 decay products: W+ -> u d~ / Z -> f fbar

      CALL OXXXXX(P3b,0d0,NHEL(4),1,W3b)         ! f1:    u, nu  / q, l-, nu
      CALL IXXXXX(P3c,0d0,NHEL(5),-1,W3c)        ! f1bar: d~, l+ / q~, l+, nu~ 
      CALL JIOXXX(W3c,W3b,GB1ff,MB1,GB1,W3d)     ! W+ / Z

!     E+ -> nu W+ / E+ -> l+ Z

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! 
      CALL FVIXXX(W3a,W3d,G3B1,mL,GE,W3)       ! E+

!     ------------------------------------------------

!     E- -> l- H

      CALL SXXXXX(PH,1,W4d)                      ! H
      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)       ! 
      CALL FSOXXX(W4a,W4d,GHEl,mL,GE,W4)       ! E- (E in l out) 

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZqq,MZ,GZ,W5)
      CALL JIOXXX(W1,W2,GAqq,0d0,0d0,W6)

      CALL IOVXXX(W3,W4,W5,GZEE,AMP(1))
      CALL IOVXXX(W3,W4,W6,GAEE,AMP(2))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)
      AMP(2)=AMP(2)*Vln(IL1)*Vln(IL2)

      EE_BH = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          EE_BH = EE_BH + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END





      REAL*8 FUNCTION EE_HH(P1,P2,P3a,PH1,P4a,PH2,NHEL)

!     FOR PROCESS : q q~  -> E+ E- with decay to H & H

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),PH1(0:3)
      REAL*8 P4a(0:3),PH2(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3d(6)
      COMPLEX*16 W4a(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mL,GE,GN
      COMMON /Dmass/ mL,GE,GN
      REAL*8 VlN(3)
      COMMON /Dcoup/ VlN
      INTEGER IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Dflags/ IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GWEN(2),GZEE(2),GZNN(2),GAEE(2)
      REAL*8 GWEv(2),GWlN(2),GZEl(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHEl(2),GHlE(2),GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZqq(2),GAqq(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  
      DATA EIGEN_VEC(2,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z, gamma coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
        GAqq(1)=-2d0/3d0*e
        GAqq(2)=-2d0/3d0*e
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
        GAqq(1)=1d0/3d0*e
        GAqq(2)=1d0/3d0*e
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .NE. 3) THEN
        PRINT *,'Wrong IB1 = ',IB1,' in EE_HH'
        STOP
      ENDIF

      IF (IB2 .NE. 3) THEN
        PRINT *,'Wrong IB2 = ',IB2,' in EE_HH'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar
      
!     E+ -> l+ H

      CALL SXXXXX(PH1,1,W3d)                      ! H
      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! 
      CALL FSIXXX(W3a,W3d,GHlE,mL,GE,W3)       ! E+  (l in E out)

!     ------------------------------------------------

!     E- -> l- H

      CALL SXXXXX(PH2,1,W4d)                      ! H
      CALL OXXXXX(P4a,0d0,NHEL(4),1,W4a)       ! 
      CALL FSOXXX(W4a,W4d,GHEl,mL,GE,W4)       ! E- (E in l out) 

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZqq,MZ,GZ,W5)
      CALL JIOXXX(W1,W2,GAqq,0d0,0d0,W6)

      CALL IOVXXX(W3,W4,W5,GZEE,AMP(1))
      CALL IOVXXX(W3,W4,W6,GAEE,AMP(2))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)
      AMP(2)=AMP(2)*Vln(IL1)*Vln(IL2)

      EE_HH = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          EE_HH = EE_HH + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


