!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Z couplings to SM fermions

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

!     W' couplings to SM fermions

      GWFR(1)=0d0
      GWFR(2)=-gR/SQRT(2d0)

!     N-light fermion-gauge boson and Higgs
!     Normalised to Vln = 1

      GWlN(1)=GWF(1)                   ! WlN
      GWlN(2)=0d0

      GWlN_I(1)=-GWlN(2)               ! WlN inverted
      GWlN_I(2)=-GWlN(1)

      GZNv(1)=-gcw/2d0                 ! ZNv, N in v out
      IF (IMA .EQ. 1) THEN             ! Majorana only
        GZNv(2)=-GZNv(1)               ! Complex conjugate, actually
      ELSE
        GZNv(2)=0d0
      ENDIF
      
      GZvN(1)=GZNv(1)                  ! N out v in is the complex conjugate
      GZvN(2)=GZNv(2)
      
      GHNv(2)=-g/2d0*(mN/MW)           ! HNv, N in v out
      IF (IMA .EQ. 1) THEN             ! Majorana only
        GHNv(1)=GHNv(2)                ! Complex conjugate, actually
      ELSE
        GHNv(1)=0d0
      ENDIF
      
      GHvN(1)=GHNv(2)                  ! N out v in (actually, conjg)
      GHvN(2)=GHNv(1)

      GWplN(1)=0d0                     ! W'lN
      GWplN(2)=GWFR(2)
      
      GWplN_I(1)=-GWplN(2)             ! W'lN inverted
      GWplN_I(2)=-GWplN(1)
      

