      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MW  mt           mN    MW'
      DATA varmin  /0.  ,100.   ,0.   ,0.,     0.,-1.,-1.,-1.,-1.,-1./
      DATA varmax  /100.,250.,150.,4000.,4000., 1., 1., 1., 1., 1./
      DATA nbintot /100 ,75  ,75  ,200,   200, 40, 40, 40, 40, 40/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO
      
      
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

!     Set to zero dummy bins

      varsig(1,1)=0d0
      varsig(2,1)=0d0
      varsig(3,1)=0d0

      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      LOGICAL GOODASYM(n_a)

      DATA asym_cent /0.,0.,0.,0.,0.,0./

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      PRINT 999
      IF (GOODASYM(1)) PRINT 1151,asym(1),err_asym(1)
      IF (GOODASYM(2)) PRINT 1152,asym(2),err_asym(2)
      PRINT 999
      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB  ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('AFB W  = ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('AFB Z  = ',F8.5,' +- ',F8.5,' (MC)')
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 DOT

!     External momenta

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PN(0:3),PB1(0:3),Pt(0:3),PW(0:3)
      COMMON /MOMINT/ PN,PB1,Pt,PW
      REAL*8 Pl1(0:3),Pl2(0:3),Pf1(0:3),Pfb1(0:3)
      COMMON /MOMEXT/ Pl1,Pl2,Pf1,Pfb1
      REAL*8 Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXTRA/ Pf2,Pfb2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      INTEGER IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IF1,ILNV

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     Local kinematical quantities

      REAL*8 PWprec(0:3),PNrec(0:3)

!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

      IF (IB1 .EQ. 5) var(1)=SQRT(DOT(Pf2,Pfb2))
      IF (IB1 .EQ. 5) THEN
        IF (     ((IQ .EQ. 1) .AND. (ILNV .EQ. 0))
     &    .OR. ((IQ .EQ. 2) .AND. (ILNV .EQ. 1)) ) THEN
          CALL SUMVEC3(Pf2,Pfb2,Pf1,pdum1)
        ELSE
          CALL SUMVEC3(Pf2,Pfb2,Pfb1,pdum1)
        ENDIF
        var(2)=SQRT(DOT(pdum1,pdum1))
      ENDIF

      IF (IB1 .LE. 3) THEN
      var(3)=SQRT(DOT(PB1,PB1))
      ENDIF

      CALL SUMVEC3(Pl1,Pf1,Pfb1,pdum1)
      IF (IB1 .EQ. 5) THEN
        CALL SUMVEC3(Pf2,Pfb2,pdum1,PNrec)
      ELSE
        CALL STORE(pdum1,PNrec)
      ENDIF

      var(4)=SQRT(DOT(PNrec,PNrec))

      CALL SUMVEC(Pl2,PNrec,PWprec)
      var(5)=SQRT(DOT(PWprec,PWprec))


      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







