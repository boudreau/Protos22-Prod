      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Input triplet parameters 

      REAL*8 m3,GE,GN,GT2
      COMMON /Trmass/ m3,GE,GN,GT2
      REAL*8 VlN(3)
      COMMON /Trcoup/ VlN

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER IP,IE,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Trflags/ IP,IE,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      INTEGER IDW1,IDZ1,IDH1,IDW2,IDZ2,IDH2
      COMMON /Trfstate/ IDW1,IDZ1,IDH1,IDW2,IDZ2,IDH2

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      REAL*8 WTT1(3),WTT2(3),WTFL(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRT2ac(0:3),BRFLac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTT2,WTFL,WTZ,BRT1ac,BRT2ac,BRFLac,BRZac

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGE,NGT2,NGW,NGZ,NGH
      COMMON /BWdata/ NGE,NGT2,NGW,NGZ,NGH
      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &        nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,nhel4,GOODHEL,
     &  ncomb8,ncomb6,ncomb4,ntry

!     Variables for weighting of final states

      REAL*8 PW_T1,PW_T2
      REAL*8 PRT1(3),PRT2(3),PRZ(4),PRFL(3)

!     Local variables

      INTEGER i,j
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 GE_Wv,GE_Zl,GE_Hl,GN_Wl,GN_Zv,GN_Hv
      REAL*8 xW,xb,EW,qW
      REAL*8 r_W,r_Z,r_H,Vsq
      REAL*8 gevpb
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb*1000d0                   ! Cross section in fb
      ENDDO
      
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(M3)
      CALL SETALPHAS(M3)
!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)

!     Z boson

      cw=SQRT(1d0-sw2)
      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)

!     Higgs  -- not used

      GH=1d0

!     Heavy E

      r_Z = (MZ/m3)**2
      r_W = (MW/m3)**2
      r_H = (MH/m3)**2
      Vsq = VlN(1)**2 + VlN(2)**2 + VlN(3)**2

      GE_Wv = g**2/(32d0*pi)*Vsq*
     &  (m3**3/MW**2)*(1d0-r_W)*(1d0+r_W-2d0*r_W**2)
      IF (MW .GT. m3) GE_Wv=0d0

      GE_Zl = g**2/(64d0*pi*cw**2)*Vsq*
     &  (m3**3/MZ**2)*(1d0-r_Z)*(1d0+r_Z-2d0*r_Z**2)
      IF (MZ .GT. m3) GE_Zl=0d0

      GE_Hl = g**2/(64d0*pi)*Vsq*
     &  (m3**3/MW**2)*(1d0-r_H)**2
      IF (MH .GT. m3) GE_Hl=0d0

!     Heavy N

      GN_Wl = g**2/(64d0*pi)*Vsq*
     &  (m3**3/MW**2)*(1d0-r_W)*(1d0+r_W-2d0*r_W**2)
      IF (MW .GT. m3) GN_Wl=0d0

      GN_Zv = g**2/(128d0*pi*cw**2)*Vsq*                    ! For Dirac N
     &  (m3**3/MZ**2)*(1d0-r_Z)*(1d0+r_Z-2d0*r_Z**2)
      IF (MZ .GT. m3) GN_Zv=0d0

      GN_Hv = g**2/(128d0*pi)*Vsq*                    ! For Dirac N
     &  (m3**3/MW**2)*(1d0-r_H)**2
      IF (MH .GT. m3) GN_Hv=0d0

      GN=GN_Wl+GN_Zv+GN_Hv
      IF (IE .EQ. 1) THEN
        GE_Wv=0d0
        GE=GE_Zl+GE_Hl
      ELSE IF (IE .EQ. 2) THEN
        GE_Zl=0d0
        GE_Hl=0d0
        GE=GE_Wv
      ELSE
        PRINT *,'Wrong IE'
      ENDIF

      IF ((IP .EQ. 1) .AND. (IE .EQ. 1)) PRINT 1050
      IF ((IP .EQ. 1) .AND. (IE .EQ. 2)) PRINT 1051
      IF ((IP .EQ. 2) .AND. (IE .EQ. 1)) PRINT 1052
      IF ((IP .EQ. 2) .AND. (IE .EQ. 2)) PRINT 1053
      PRINT 1100,m3,GE,GN
      PRINT 1203,GE_Wv/GE,GE_Zl/GE,GE_Hl/GE
      PRINT 1204,GN_Wl/GN,GN_Zv/GN,GN_Hv/GN
      PRINT 999
      PRINT 1201,GZ_nu/GZ,GZ_emt/GZ
      PRINT 1202,GZ_uc/GZ,GZ_dsb/GZ
      PRINT 999

!     Probabilities for each decay channel

      PW_T1=GE_Wv*DFLOAT(IDW1)+GE_Zl*DFLOAT(IDZ1)+GE_Hl*DFLOAT(IDH1)
      PRT1(1)=GE_Wv*DFLOAT(IDW1)/PW_T1
      PRT1(2)=GE_Zl*DFLOAT(IDZ1)/PW_T1
      PRT1(3)=GE_Hl*DFLOAT(IDH1)/PW_T1

      IF (IP .EQ. 1) THEN
        PW_T2=GE_Wv*DFLOAT(IDW2)+GE_Zl*DFLOAT(IDZ2)+GE_Hl*DFLOAT(IDH2)
        PRT2(1)=GE_Wv*DFLOAT(IDW2)/PW_T2
        PRT2(2)=GE_Zl*DFLOAT(IDZ2)/PW_T2
        PRT2(3)=GE_Hl*DFLOAT(IDH2)/PW_T2
      ELSE
        PW_T2=GN_Wl*DFLOAT(IDW2)+GN_Zv*DFLOAT(IDZ2)+GN_Hv*DFLOAT(IDH2)
        PRT2(1)=GN_Wl*DFLOAT(IDW2)/PW_T2
        PRT2(2)=GN_Zv*DFLOAT(IDZ2)/PW_T2
        PRT2(3)=GN_Hv*DFLOAT(IDH2)/PW_T2
      ENDIF

      IF ((PW_T1 .EQ. 0d0) .OR. (PW_T2 .EQ. 0d0)) THEN
        PRINT 985
        PRINT 999
        STOP
      ENDIF

      BRT1ac(0)=0d0
      DO i=1,3
        BRT1ac(i)=BRT1ac(i-1)+PRT1(i)
        WTT1(i)=1d0/PRT1(i)
        IF (PRT1(i) .EQ. 0d0) WTT1(i)=0d0
      ENDDO
      IF (ABS(BRT1ac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRT1ac(3)
      BRT1ac(3)=1d0

      BRT2ac(0)=0d0
      DO i=1,3
        BRT2ac(i)=BRT2ac(i-1)+PRT2(i)
        WTT2(i)=1d0/PRT2(i)
        IF (PRT2(i) .EQ. 0d0) WTT2(i)=0d0
      ENDDO
      IF (ABS(BRT2ac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRT2ac(3)
      BRT2ac(3)=1d0

      PRZ(1)=GZ_nu/GZ
      PRZ(2)=GZ_emt/GZ
      PRZ(3)=GZ_uc/GZ
      PRZ(4)=GZ_dsb/GZ

      BRZac(0)=0d0
      DO i=1,4
        BRZac(i)=BRZac(i-1)+PRZ(i)
        WTZ(i)=1d0/PRZ(i)
      ENDDO
      IF (ABS(BRZac(4)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRZac(4)
      BRZac(4)=1d0

      PRFL(1)=VlN(1)**2/Vsq
      PRFL(2)=VlN(2)**2/Vsq
      PRFL(3)=VlN(3)**2/Vsq

      BRFLac(0)=0d0
      DO i=1,3
        BRFLac(i)=BRFLac(i-1)+PRFL(i)
        WTFL(i)=1d0/PRFL(i)
        IF (PRFL(i) .EQ. 0d0) WTFL(i)=0d0
      ENDDO
      IF (ABS(BRFLac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRFLac(3)
      BRFLac(3)=1d0

!     Include here colour factors and sum over fermions for Z, H

      WTZ(1)=WTZ(1)*3d0
      WTZ(2)=WTZ(2)*3d0
      WTZ(3)=WTZ(3)*6d0
      WTZ(4)=WTZ(4)*9d0

c      print *,prt1
c      print *,BRT1ac
c      print *,WTT1
c      print 999
c      print *,prt2
c      print *,BRT2ac
c      print *,WTT2
c      print *,BRZac
c      print *,WTZ
c      stop

      NGE=m3/GE*0.95d0
      IF (IP .EQ. 1) THEN
        GT2=GE
        NGT2=m3/GE*0.95d0
      ELSE
        GT2=GN
        NGT2=m3/GN*0.95d0
      ENDIF
      NGW=MW/GW*0.95d0
      NGZ=MZ/GZ*0.95d0

!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO

      CALL SETPOL(istep,nhel8,ncomb8,8)
      DO i=1,MAXAMP
        DO j=1,ncomb8
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

      CALL SETPOL(istep,nhel6,ncomb6,6)
      CALL SETPOL(istep,nhel4,ncomb4,4)

c      DO i=1,ncomb8
c      PRINT *,(nhel8(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb8
c      DO i=1,ncomb6
c      PRINT *,(nhel6(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb6
c      DO i=1,ncomb4
c      PRINT *,(nhel4(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb4
c      STOP

      RETURN

500   FORMAT (' _____    _           _       ')
501   FORMAT ('|_   _|  (_)         | |      ')
502   FORMAT ('  | |_ __ _  __ _  __| | __ _ ')
503   FORMAT ('  | | \'__| |/ _` |/ _` |/ _` |')
504   FORMAT ('  | | |  | | (_| | (_| | (_| |')
505   FORMAT ('  \\_/_|  |_|\\__,_|\\__,_|\\__,_|')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
985   FORMAT ('Please open some decay channel for heavy leptons!')
999   FORMAT ('')
1000  FORMAT ('---------------------------------------')
1050  FORMAT ('Calculating E+ E- production for E = E1-')
1051  FORMAT ('Calculating E+ E- production for E = E2+')
1052  FORMAT ('Calculating E+- N production for E = E1-')
1053  FORMAT ('Calculating E+- N production for E = E2+')
1100  FORMAT ('M3 = ',F6.1,'   GE = ',F9.6,'   GN = ',F9.6)
1201  FORMAT ('BR(Z -> vv) = ',F5.3,'  BR(Z -> ll) = ',F5.3)
1202  FORMAT ('BR(Z -> uu) = ',F5.3,'  BR(Z -> dd) = ',F5.3)
1203  FORMAT ('BR(E -> Wv) = ',F5.3,
     &  '  BR(E -> Zl) = ',F5.3,'  BR(E -> Hl) = ',F5.3)
1204  FORMAT ('BR(N -> Wl) = ',F5.3,
     &  '  BR(N -> Zv) = ',F5.3,'  BR(N -> Hv) = ',F5.3)
1303  FORMAT ('Warning: in initialisation acc sum = ',D9.4)
      END


      SUBROUTINE SETPOL(nspin,hel,nhel,num)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel,num

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,num
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,num
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=num
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,num
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END

