!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Z couplings to SM fermions

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

!     Triplet-triplet-gauge boson

      IF (IE .EQ. 1) THEN                ! (E = E1-)

        GWEN(1)=-g            ! WEN
        GWEN(2)=-g

        GZEE(1)=g*cW          ! ZEE
        GZEE(2)=g*cW

        GAEE(1)=e             ! AEE
        GAEE(2)=e

      ELSE IF (IE .EQ. 2) THEN           ! (E = E2+)

        GWEN(1)=g             ! WEN
        GWEN(2)=g

        GZEE(1)=-g*cW         ! ZEE
        GZEE(2)=-g*cW

        GAEE(1)=-e            ! AEE
        GAEE(2)=-e

      ENDIF

!     E-light fermion-gauge boson and Higgs
!     Normalised to Vln = 1

      IF (IE .EQ. 1) THEN                ! (E = E1-)

        GWEv(1)=0d0                      ! WEv
        GWEv(2)=0d0

        GZEl(1)=gcw/2d0*SQRT(2d0)        ! ZEl
        GZEl(2)=0d0

        GHEl(1)=0d0                      ! HEl, E in l out
        GHEl(2)=GWF(1)*(m3/MW)

        GHlE(1)=GHEl(2)                  ! HEl, E out l in (actually, conjg)
        GHlE(2)=GHEl(1)

      ELSE IF (IE .EQ. 2) THEN           ! (E = E+)

        GWEv(1)=g                        ! WEv
        GWEv(2)=0d0

        GZEl(1)=0d0                      ! ZEl
        GZEl(2)=0d0

        GHEl(1)=0d0                      ! HEl, E in l out
        GHEl(2)=0d0

        GHlE(1)=0d0                      ! HEl, E out l in (actually, conjg)
        GHlE(2)=0d0

      ENDIF

!     N-light fermion-gauge boson and Higgs
!     Normalised to Vln = 1

      GWlN(1)=GWF(1)                   ! WlN
      GWlN(2)=0d0

      GZNv(1)=gcw/2d0                  ! ZNv, N in v out
      GZnv(2)=0d0
      
      GZvN(1)=GZNv(1)                  ! N out v in is the complex conjugate
      GZvN(2)=GZNv(2)
      
      GHNv(1)=0d0                      ! HNv, N in v out
      GHNv(2)=g/2d0*(m3/MW)
      
      GHvN(1)=GHNv(2)                  ! N out v in (actually, conjg)
      GHvN(2)=GHNv(1)

