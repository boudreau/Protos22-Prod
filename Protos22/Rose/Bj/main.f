      PROGRAM Protos
!
!     PROgram for TOp Simulations
!
!     By J. A. Aguilar-Saavedra, vintage June 2009
!
!     Includes adapted VEGAS for multigrid integration
!     Uses HELAS for matrix element evaluation
!
      IMPLICIT NONE
      
!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions used

      REAL*8 FXN
      INTEGER LONGITUD
      EXTERNAL FXN,LONGITUD
      
!     Flags

      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      INTEGER IMOD,IQ,IB1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IZF1
      INTEGER IDW1,IDZ1,IDH1
      COMMON /Qfstate/ IDW1,IDZ1,IDH1

!     Input heavy quark parameters

      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix

!     Other input data

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC1,QFAC2
      COMMON /PDFSCALE/ QFAC1,QFAC2
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     For VEGAS

      REAL*8 XL(MAXDIM),XU(MAXDIM),ACC,AVG1,SD1,CHI2A1
      INTEGER NCALL,ITMX,NPRN,ND
      COMMON/BVEG1/XL,XU,ACC,ND,NCALL,ITMX,NPRN

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC
      REAL*8 GRWGT(MAXGR)
      COMMON /multigr2/ GRWGT
      REAL*8 GRAPROB(0:MAXGR)
      COMMON /multigr3/ GRAPROB

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_t,SIG_tbar,ERR_t,ERR_tbar
      COMMON /FINALSTATS/ SIG_t,ERR_t,SIG_tbar,ERR_tbar
      REAL*8 SIG_tot,Rt
      COMMON /logextra/ SIG_tot,Rt

!     For BR check

      REAL*8 countB(3)
      COMMON /testdec/ countB

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     For ran2 calls

      INTEGER idum0
      COMMON /seed_ini/ idum0

!     For parameter scan

      INTEGER ISCAN
      REAL*8 SCANSTEP

!     Local

      INTEGER i
      REAL*8 ncount
      INTEGER ILOADGRID,ISAVEGRID,NCALL1,NCALL2,ITMX1,ITMX2
      INTEGER IEVTOUT,IPLOT,ILOG,IPRSUB,IPRAS,IVERB
      REAL*8 GRPROB(NPROC)
      REAL*8 ERR_tot,dRt

!     --------------------------------------------

      OPEN (32,FILE='input/run.dat',status='old')
      READ (32,*) PROCNAME
      l=LONGITUD(PROCNAME)
      READ (32,*) IMOD
      READ (32,*) ET
      READ (32,*) IPPBAR
      READ (32,*) NRUNS
      READ (32,*) ILOADGRID,ISAVEGRID
      READ (32,*) NCALL1,ITMX1
      READ (32,*) NCALL2,ITMX2
      READ (32,*) idum0
      READ (32,*) IDW1,IDZ1,IDH1
      READ (32,*) ICUT
      READ (32,*) IEVTOUT,IPLOT,ILOG
      READ (32,*) IVERB,IPRSUB,IPRAS
      READ (32,*) mQ,Vmix
      READ (32,*) ISCAN,SCANSTEP
      READ (32,*) IPDSET
      READ (32,*) QFAC1,QFAC2
      READ (32,*) IMATCH
      PTbmin=0d0
      IF (IMATCH .EQ. 2) READ (32,*) PTbmin
      CLOSE (32)

!     Check parameter consistency

      IF ((IMOD .LT. 1) .OR. (IMOD .GT. 4) .OR.
     &  (IDW1 .LT. 0) .OR. (IDW1 .GT. 1) .OR.
     &  (IDZ1 .LT. 0) .OR. (IDZ1 .GT. 1) .OR.
     &  (IDH1 .LT. 0) .OR. (IDH1 .GT. 1) ) THEN
        PRINT 985
        STOP
      ENDIF
      IF (IMOD .EQ. 3) THEN
        PRINT 988
        STOP
      ENDIF

      IF ((IMATCH .LT. 0) .OR. (IMATCH .GT. 2)) THEN
        PRINT *,'Wrong matching option IMATCH'
        STOP
      ENDIF

!     Override settings for special cases

      IF (NRUNS .GT. 1) THEN
        ISAVEGRID=0
      ENDIF

      IF ((IEVTOUT .NE. 0) .AND. (NRUNS .GT. 99)) THEN
        PRINT 986
        STOP
      ENDIF

!
!     BEGIN
!

      ND=NDIM
      CALL INITPAR


      IF (ILOG .NE. 0) CALL LOGINI(-1,0)

      DO IRUN=1,NRUNS                     !   Different runs

      IF (NRUNS .GT. 1) THEN
        PRINT 1110,IRUN
        PRINT 999
      ENDIF

      CALL REINITPAR
      CALL RAN2RESET(idum0)

      IF (IEVTOUT .EQ. 1) CALL EVTOUT(-1)
      CALL PLOTINI(-1)
      CALL ASINI(-1)

!     First integration: warmup

      DO i=1,ND
        XL(i)=0d0
        XU(i)=1d0
      ENDDO
      ACC=-1d0

      IF (ILOADGRID .EQ. 1) GOTO 20

      DO i=1,MAXGR
        SIG(i)=0d0
        SIG2(i)=0d0
      ENDDO

      IRECORD=0
      IWRITE=0
      IHISTO=0
      NCALL=NCALL1
      ITMX=ITMX1
      NPRN=0
      WT_ITMX=1d0/FLOAT(ITMX)

      INITGRID=1
      DO IPROC=1,NPROC
      PRINT 1100,IPROC
        CALL VEGAS(FXN,AVG1,SD1,CHI2A1)
        SIG(IPROC)=AVG1
      ENDDO

      SIG_tot=0d0
      DO IPROC=1,NPROC
        SIG_tot=SIG_tot+SIG(IPROC)
      ENDDO

      GRAPROB(0)=0d0
      DO i=1,MAXGR
        GRPROB(i)=SIG(i)/SIG_tot
        GRAPROB(i)=GRAPROB(i-1)+GRPROB(i)
        IF (GRPROB(i) .GT. 0d0) THEN
          GRWGT(i)=1d0/GRPROB(i)
        ELSE
          GRWGT(i)=0d0
        ENDIF
      ENDDO
      IF (ABS(GRAPROB(NPROC)-1d0) .GT. 1d-8) THEN
        PRINT 999
        PRINT 987,1d0-graprob(nproc)
      ENDIF
      GRAPROB(NPROC)=1d0


      IF (ISAVEGRID .EQ. 1) THEN
        OPEN (33,file='input/grid.ini',status='unknown')
        CALL SAVE(ND)
        CLOSE (33)
        OPEN (33,file='input/proc.ini',status='unknown')
        WRITE (33,2010) GRAPROB
        WRITE (33,2011) GRWGT
        CLOSE (33)
      ENDIF
      
20    CONTINUE


!     Second integration run

      DO i=1,MAXGR
        SIG(i)=0d0
        SIG2(i)=0d0
	ERR(i)=0d0
      ENDDO
      NIN=0
      NOUT=0

      IF (ILOADGRID .EQ. 1) THEN
        OPEN (33,file='input/grid.ini',status='old')
        CALL RESTR(ND)
        CLOSE (33)
        OPEN (33,file='input/proc.ini',status='old')
        READ (33,2010) GRAPROB
        READ (33,2011) GRWGT
        CLOSE (33)
      ENDIF

      IRECORD=1
      IF (IEVTOUT .EQ. 1) IWRITE=1
      IF ((IPLOT .NE. 0) .OR. (ILOG .NE. 0) .OR. (IPRAS .NE. 0))
     &   IHISTO=1
      NCALL=NCALL2
      ITMX=ITMX2
      NPRN=IVERB
      WT_ITMX=1d0/FLOAT(ITMX)
      INITGRID=0
      CALL VEGAS1(FXN,AVG1,SD1,CHI2A1)

      SIG_t=0d0
      ERR_t=0d0
      SIG_tbar=0d0
      ERR_tbar=0d0
      DO i=1,8
      SIG_t=SIG_t+SIG(i)+SIG(i+16)
      ERR_t=ERR_t+ERR(i)+ERR(i+16)
      SIG_tbar=SIG_tbar+SIG(i+8)+SIG(i+24)
      ERR_tbar=ERR_tbar+ERR(i+8)+ERR(i+24)
      ENDDO
     
      SIG_tot=SIG_t+SIG_tbar
      ERR_tot=ERR_t+ERR_tbar      
      Rt=SIG_tbar/SIG_t
      dRt=Rt*SQRT((ERR_t/SIG_t)**2+(ERR_tbar/SIG_tbar)**2)
      PRINT 990

      IF ((IPRAS .NE. 0) .OR. (ILOG .NE. 0)) CALL ASINI(1)
      IF (IPRAS .NE. 0) CALL ASINI(2)
      IF (ILOG .NE. 0) CALL LOGINI(0,ISCAN)
      IF (IPLOT .EQ. 1) CALL PLOTINI(1)
      IF (IEVTOUT .EQ. 1) CALL EVTOUT(1)

      ncount=0
      DO i=1,3
        ncount=ncount+countB(i)
      ENDDO
      PRINT 1300
      PRINT 999
      PRINT 1301
      PRINT 1302,(countB(i)/ncount,i=1,3)
      PRINT 999

      IF (IPRSUB .EQ. 1) THEN
        PRINT 1140,SIG(1)+SIG(17),ERR(1)+ERR(17)
        PRINT 1141,SIG(2)+SIG(18),ERR(2)+ERR(18)
        PRINT 1142,SIG(3)+SIG(19),ERR(3)+ERR(19)
        PRINT 1143,SIG(4)+SIG(20),ERR(4)+ERR(20)
        PRINT 1144,SIG(5)+SIG(21),ERR(5)+ERR(21)
        PRINT 1145,SIG(6)+SIG(22),ERR(6)+ERR(22)
        PRINT 1146,SIG(7)+SIG(23),ERR(7)+ERR(23)
        PRINT 1147,SIG(8)+SIG(24),ERR(8)+ERR(24)
        PRINT 1148,SIG(9)+SIG(25),ERR(9)+ERR(25)
        PRINT 1149,SIG(10)+SIG(26),ERR(10)+ERR(26)
        PRINT 1150,SIG(11)+SIG(27),ERR(11)+ERR(27)
        PRINT 1151,SIG(12)+SIG(28),ERR(12)+ERR(28)
        PRINT 1152,SIG(13)+SIG(29),ERR(13)+ERR(29)
        PRINT 1153,SIG(14)+SIG(30),ERR(14)+ERR(30)
        PRINT 1154,SIG(15)+SIG(31),ERR(15)+ERR(31)
        PRINT 1155,SIG(16)+SIG(32),ERR(16)+ERR(32)
        PRINT 999
      ENDIF
      PRINT 1160,SIG_t,ERR_t
      PRINT 1161,SIG_tbar,ERR_tbar
      PRINT 999
      PRINT 1130,SIG_tot,ERR_tot
      PRINt 1135,Rt,dRt
      PRINT 990

      IF (ISCAN .NE. 0) CALL CHGPAR(ISCAN,SCANSTEP)

      ENDDO                                             ! End of runs

      IF (ILOG .NE. 0) CALL LOGINI(1,0)

      STOP

984   FORMAT ('For exotic doublet T does not couple to b',
     & ' at first order')
985   FORMAT ('ERROR: incorrect ID')
986   FORMAT ('ERROR: NRUNS out of range')
987   FORMAT ('Warning: in initialisation acc sum = ',D9.4)
988   FORMAT ('This process does not take place in this model',
     & ' at first order')
990   FORMAT ('===================================================')
995   FORMAT ('---------------------------------------------------')
999   FORMAT ('')
1100  FORMAT ('Initialising grid ',I2,'...')
1110  FORMAT ('Run: ',I2)
1130  FORMAT ('Total xsec = ',D9.4,' +- ',D9.4,' fb')
1135  FORMAT ('R = ',F7.4,' +- ',F7.4)

1140  FORMAT ('xsec bu   = ',D10.4,' +- ',D9.4)
1141  FORMAT ('xsec bc   = ',D10.4,' +- ',D9.4)
1142  FORMAT ('xsec bu~  = ',D10.4,' +- ',D9.4)
1143  FORMAT ('xsec bc~  = ',D10.4,' +- ',D9.4)
1144  FORMAT ('xsec bd   = ',D10.4,' +- ',D9.4)
1145  FORMAT ('xsec bs   = ',D10.4,' +- ',D9.4)
1146  FORMAT ('xsec bd~  = ',D10.4,' +- ',D9.4)
1147  FORMAT ('xsec bs~  = ',D10.4,' +- ',D9.4)
1148  FORMAT ('xsec b~u  = ',D10.4,' +- ',D9.4)
1149  FORMAT ('xsec b~c  = ',D10.4,' +- ',D9.4)
1150  FORMAT ('xsec b~u~ = ',D10.4,' +- ',D9.4)
1151  FORMAT ('xsec b~c~ = ',D10.4,' +- ',D9.4)
1152  FORMAT ('xsec b~d  = ',D10.4,' +- ',D9.4)
1153  FORMAT ('xsec b~s  = ',D10.4,' +- ',D9.4)
1154  FORMAT ('xsec b~d~ = ',D10.4,' +- ',D9.4)
1155  FORMAT ('xsec b~s~ = ',D10.4,' +- ',D9.4)

1160  FORMAT ('xsec B = ',D10.4,' +- ',D9.4)
1161  FORMAT ('xsec Bbar = ',D10.4,' +- ',D9.4)
1300  FORMAT ('Fraction of generated boson decays',
     &  ' - this is not BR if channels off!')
1301  FORMAT ('          W       Z       H')
1302  FORMAT ('      ',F6.4,'  ',F6.4,'  ',F6.4)
2010  FORMAT (F8.6)
2011  FORMAT (D12.6)

      END
