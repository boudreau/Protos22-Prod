      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

10    CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      REAL*8 NGQ,NGt,NGW,NGZ
      COMMON /BWdata/ NGQ,NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 WTT1(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTZ,BRT1ac,BRZac
      INTEGER IDW1,IDZ1,IDH1
      COMMON /Qfstate/ IDW1,IDZ1,IDH1
      INTEGER IZL1
      COMMON /Qtweak/ IZL1

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      INTEGER IMOD,IQ,IB1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IZF1
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pb(0:3),Pj(0:3),Pf1(0:3),Pfb1(0:3),Pf3(0:3),Pfb3(0:3)
      COMMON /MOMEXT/ Pb,Pj,Pf1,Pfb1,Pf3,Pfb3
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR
      REAL*8 countB(3)
      COMMON /testdec/ countB

!     Local variables

      INTEGER i
      REAL*8 EPS,y1,y2,flux,s
      REAL*8 PCM_LAB(0:3),Pdum1(0:3)
      REAL*8 QQ1,QW1,Qt1,QB1,mf1
      REAL*8 WT1,WT2,WT3,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS
      REAL*8 RCH,ICH

!     --------------------------------------------

      WTPS=0d0

      IDIR=0
      IF (IPROC .GT. 16) IDIR=1

!     ------------------
!     Select final state
!     ------------------

      WTFS=1d0

!     Decay of Q1: Wb,Zt,Ht

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT1ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB1=ICH

      WTFS=WTFS*WTT1(IB1)
      countB(IB1)=countB(IB1)+1d0

!     Fermions from Z1,Z2 decay (if any)

      IZF1=-1
      IF (IB1 .EQ. 1) THEN
        WTFS=WTFS*81d0
      ELSE IF (IB1 .EQ. 2) THEN
        IF (IZL1 .EQ. 1) THEN
          IZF1=1                      ! only leptons
          WTFS=WTFS*3d0
        ELSE
          RCH=RAN2(idum)
          ICH=0
          DO WHILE (BRZac(ICH) .LT. RCH)
            ICH=ICH+1
          ENDDO
          IZF1=ICH-1
          WTFS=WTFS*WTZ(ICH)
        ENDIF
      ENDIF

!     Generation of the masses of the virtual particles

      CALL BREIT(mQ,GQ,NGQ,QQ1,WT1)
      WT_BREIT=WT1*(2d0*pi)**3

      IF (IB1 .EQ. 1) THEN                          ! Q1 -> Wt
        CALL BREIT(MW,GW,NGW,QB1,WT1)
        CALL BREIT(mt,Gt,NGt,Qt1,WT2)
        IF (QB1+Qt1 .GT. QQ1) RETURN
        CALL BREIT(MW,GW,NGW,QW1,WT3)               ! and t -> Wb
        IF (QW1+mb .GT. Qt1) RETURN
        mf1=Qt1
        WT_BREIT=WT_BREIT*WT1*WT2*WT3*(2d0*pi)**9
      ELSE IF (IB1 .EQ. 2) THEN                     ! Q1 -> Zb
        CALL BREIT(MZ,GZ,NGZ,QB1,WT1)
        IF (QB1+mb .GT. QQ1) RETURN
        mf1=mb
        WT_BREIT=WT_BREIT*WT1*(2d0*pi)**3
      ELSE                                          ! Q1 -> Hb
        QB1=MH
        mf1=mb
        IF (QB1+mb .GT. QQ1) RETURN
      ENDIF

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(QQ1)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system
      
      Q1(0)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(3)=ET*x1/2d0
              
      Q2(0)=ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(3)=-ET*x2/2d0

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)


!     Set to zero internal, not needed but good for plots

      Pt1(0)=0d0
      DO i=1,3
        Pt1(i)=0d0
      ENDDO
      CALL STORE(Pt1,PB1)
      CALL STORE(Pt1,PW1)
      CALL STORE(Pt1,Pf3)
      CALL STORE(Pt1,Pfb3)

!     Generation

      CALL PHASE2sym(SQRT(s),QQ1,0d0,PCM_LAB,PQ1,Pj,WT)
      WT_PROD=(2d0*pi)**4*WT

!     Q1 decay 

      CALL PHASE2(QQ1,QB1,mf1,PQ1,PB1,Pdum1,WT1)
      WT_DEC=WT1

!     B1 decay

      IF (IB1 .NE. 3) THEN
        CALL PHASE2(QB1,0d0,0d0,PB1,Pf1,Pfb1,WT1)
        WT_DEC=WT_DEC*WT1
      ENDIF

!     Decay of the quark if IB = 1 (top)

      IF (IB1 .EQ. 1) THEN                           ! t -> Wb
        CALL STORE(Pdum1,Pt1)
        CALL PHASE2(Qt1,QW1,mb,Pt1,PW1,Pb,WT1)
        CALL PHASE2(QW1,0d0,0d0,PW1,Pf3,Pfb3,WT2)
        WT_DEC=WT_DEC*WT1*WT2
      ELSE
        CALL STORE(Pdum1,Pb)
      ENDIF

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local variables

!     --------------------------------------------

      INOT=1

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 Bj,Bbarj,DOT

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pb(0:3),Pj(0:3),Pf1(0:3),Pfb1(0:3),Pf3(0:3),Pfb3(0:3)
      COMMON /MOMEXT/ Pb,Pj,Pf1,Pfb1,Pf3,Pfb3

      INTEGER IMOD,IQ,IB1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IZF1

      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &  nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,nhel4,GOODHEL,ncomb8,ncomb6,ncomb4,ntry

      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC1,QFAC2
      COMMON /PDFSCALE/ QFAC1,QFAC2
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     Local variables

      INTEGER i
      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      REAL*8 Ql1,Ql2,Qb1,Qb2
      INTEGER k,ISTAT,IAMP,ncomb
      REAL*8 M1,ME,STR(NPROC)
      REAL*8 fbsub
      

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Ql1=SQRT(2d0*DOT(Q2,Pj))*QFAC1
      Ql2=SQRT(2d0*DOT(Q1,Pj))*QFAC1
      Qb1=SQRT(Ql1**2+mQ**2)*QFAC2
      Qb2=SQRT(Ql2**2+mQ**2)*QFAC2

      Q=Qb1
      IF (IDIR .EQ. 1) Q=Qb2
     
      CALL GETPDF_tj(x1,Ql2,Qb1,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN
      IF (IMATCH .EQ. 1) THEN
        CALL CALC_BPDF(x1,Qb2,fbsub)
        fb1=fb1-fbsub
      ENDIF
      CALL GETPDF_tj(x2,Ql1,Qb2,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN
      IF (IMATCH .EQ. 1) THEN
        CALL CALC_BPDF(x2,Qb1,fbsub)
        fb2=fb2-fbsub
      ENDIF
      
c      fus1=fu1
c      fus2=fu2
c      fds1=fd1
c      fds2=fd2

!     b q

      IF (IPPBAR .EQ. 0) THEN
      STR(1)=fb1*fu2          ! b  u     9 b~ u
      STR(2)=fb1*fc2          ! b  c    10 b~ c
      STR(3)=fb1*fus2         ! b  u~   11 b~ u~
      STR(4)=fb1*fc2          ! b  c~   12 b~ c~
      STR(5)=fb1*fd2          ! b  d    13 b~ d
      STR(6)=fb1*fs2          ! b  s    14 b~ s
      STR(7)=fb1*fds2         ! b  d~   15 b~ d~
      STR(8)=fb1*fs2          ! b  s~   16 b~ s~
      ELSE
      STR(1)=fb1*fus2         ! b  u   
      STR(2)=fb1*fc2          ! b  c  
      STR(3)=fb1*fu2          ! b  u~ 
      STR(4)=fb1*fc2          ! b  c~ 
      STR(5)=fb1*fds2         ! b  d 
      STR(6)=fb1*fs2          ! b  s 
      STR(7)=fb1*fd2          ! b  d~  
      STR(8)=fb1*fs2          ! b  s~
      ENDIF
      DO i=9,16               ! the same but with initial b~ 
        STR(i)=STR(i-8)
      ENDDO

!     q b

      STR(17)=fu1*fb2          ! u  b   25  b~
      STR(18)=fc1*fb2          ! c  b   26
      STR(19)=fus1*fb2         ! u~ b   27
      STR(20)=fc1*fb2          ! c~ b   28
      STR(21)=fd1*fb2          ! d  b   29
      STR(22)=fs1*fb2          ! s  b   30
      STR(23)=fds1*fb2         ! d~ b   31
      STR(24)=fs1*fb2          ! s~ b   32
      DO i=25,32               ! the same but with initial b~ 
        STR(i)=STR(i-8)
      ENDDO

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IB1 .EQ. 2) IAMP=IAMP+NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 500) ntry(IAMP) = 500

      IF (IB1 .EQ. 1) THEN
        ncomb=ncomb8
      ELSE IF (IB1 .EQ. 2) THEN
        ncomb=ncomb6
      ELSE
        ncomb=ncomb4
      ENDIF


!     Select IQ

!      DO i=1,32
!      PRINT *,i,(2-MOD((i-1)/4,2))*(-1)**MOD((i-1)/2,2)
!      ENDDO

      IQ = (2-MOD((IPROC-1)/4,2))*(-1)**MOD((IPROC-1)/2,2)

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (MOD((IPROC-1)/8,2) .EQ. 1) GOTO 12

!     b -> B

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          IF (IB1 .EQ. 1) THEN
            M1=Bj(nhel8(1,k))
          ELSE IF (IB1 .EQ. 2) THEN
            M1=Bj(nhel6(1,k))
          ELSE
            M1=Bj(nhel4(1,k))
          ENDIF
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

12    CONTINUE

!     bbar -> Bbar

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          IF (IB1 .EQ. 1) THEN
            M1=Bbarj(nhel8(1,k))
          ELSE IF (IB1 .EQ. 2) THEN
            M1=Bbarj(nhel6(1,k))
          ELSE
            M1=Bbarj(nhel4(1,k))
          ENDIF
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      
20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      
      RETURN
      END

