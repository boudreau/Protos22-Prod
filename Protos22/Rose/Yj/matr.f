      REAL*8 FUNCTION Yj(NHEL)

!     FOR PROCESS : b d  -> Y u  / b u~ -> Y d~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 Wf1(6),Wfb1(6),WB1(6),Wb(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IQ
      COMMON /Qflags/ IQ

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),Xmix

!     Specific couplings


!     Colour data

      DATA EIGEN_VAL(1) /1d0/                  
      DATA EIGEN_VEC(1,1) /-1d0/                  

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

!     Code

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)              ! b

      IF (IQ .EQ. 1) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! d
        CALL OXXXXX(Pj,0d0,NHEL(6),1,W4)            ! u
      ELSE IF (IQ .EQ. -2) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! ubar
        CALL IXXXXX(Pj,0d0,NHEL(6),-1,W4)           ! dbar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Yj'
        STOP
      ENDIF

      CALL OXXXXX(Pf1,0d0,NHEL(3),1,Wf1)            ! e-
      CALL IXXXXX(Pfb1,0d0,NHEL(4),-1,Wfb1)         ! nu~
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)           ! W-
      CALL OXXXXX(Pb,mb,NHEL(5),1,Wb)               ! b
      CALL FVOXXX(Wb,WB1,GWQb,mQ,GQ,W3)             ! Y

      IF (IQ .EQ. 1) THEN
        CALL JIOXXX(W2,W4,GWF,MW,GW,W5)
      ELSE
        CALL JIOXXX(W4,W2,GWF,MW,GW,W5)
      ENDIF
      CALL IOVXXX(W1,W3,W5,GWQb,AMP(1))
      
      Yj = 0d0
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Yj = Yj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



      REAL*8 FUNCTION Ybarj(NHEL)

!     FOR PROCESS : b~ u -> Y~ d  /  b~ d~ -> Y~ u~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 Wf1(6),Wfb1(6),WB1(6),Wb(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IQ
      COMMON /Qflags/ IQ

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),Xmix

!     Specific couplings


!     Colour data

      DATA EIGEN_VAL(1) /1d0/                  
      DATA EIGEN_VEC(1,1) /-1d0/                  

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

!     Code

      CALL OXXXXX(P1,0d0,NHEL(1),-1,W1)            ! bbar
      IF (IQ .EQ. 2) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! u
        CALL OXXXXX(Pj,0d0,NHEL(6),1,W4)            ! d
      ELSE IF (IQ .EQ. -1) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! dbar
        CALL IXXXXX(Pj,0d0,NHEL(6),-1,W4)           ! ubar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Ybarj'
        STOP
      ENDIF


      CALL OXXXXX(Pf1,0d0,NHEL(3),1,Wf1)            ! nu
      CALL IXXXXX(Pfb1,0d0,NHEL(4),-1,Wfb1)         ! e+
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)           ! W+
      CALL IXXXXX(Pb,mb,NHEL(5),-1,Wb)              ! b~
      CALL FVIXXX(Wb,WB1,GWQb,mQ,GQ,W3)             ! Y~

      IF (IQ .EQ. 2) THEN
        CALL JIOXXX(W2,W4,GWF,MW,GW,W5)
      ELSE
        CALL JIOXXX(W4,W2,GWF,MW,GW,W5)
      ENDIF
      CALL IOVXXX(W3,W1,W5,GWQb,AMP(1))

      Ybarj = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Ybarj = Ybarj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

