      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     External functions used

      INTEGER COL
      REAL*8 RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PQ1(0:3),PQ2(0:3),Pt1(0:3),Pt2(0:3),PW1(0:3),PW2(0:3)
      COMMON /MOMINT/ PQ1,PQ2,Pt1,Pt2,PW1,PW2
      REAL*8 PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ PB1,PB2
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3),
     &  Pf3(0:3),Pfb3(0:3),Pf4(0:3),Pfb4(0:3)
      COMMON /MOMEXT/ Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,Pf3,Pfb3,Pf4,Pfb4

      INTEGER IMOD,IB1,IB2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IZF1,IZF2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables for colour structure

      REAL*8 CFAPROB(0:MAXFL)
      INTEGER NFLOWS

!     Local variables for flavour structure

      INTEGER IDTAB1(9),IDTAB2(9)

!     Local variables

      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2,SIGtot
      INTEGER i

      REAL*8 RCH
      INTEGER ICH
      INTEGER IDTABB1(3),IDTABB2(3)
      INTEGER IDTABW1(9),IDTABW2(9)
      INTEGER IDTABZN(3),IDTABZL(3),IDTABZU(2),IDTABZD(3)
      INTEGER IDB1,IDB2

      DATA IDTAB1 /21, 2, 1, 3, 4,-2,-1,-3,-4/
      DATA IDTAB2 /21,-2,-1,-3,-4, 2, 1, 3, 4/
      DATA IDTABB1 /24,23,25/
      DATA IDTABB2 /-24,23,25/
      DATA IDTABW1 / 12, 14, 16, 2, 2, 2, 4, 4, 4/          ! For W+ decays
      DATA IDTABW2 /-11,-13,-15,-1,-1,-1,-3,-3,-3/
      DATA IDTABZN /12,14,16/
      DATA IDTABZL /11,13,15/
      DATA IDTABZU /2,4/
      DATA IDTABZD /1,3,5/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0

      RETURN

10    IF (IMODE .NE. 0) GOTO 11

!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)/1000d0                          ! In pb
      IF (ABS(XWGTUP) .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,ABS(XWGTUP))
      NUMEVT=NUMEVT+1

      IDB1=IDTABB1(IB1)
      IDB2=IDTABB2(IB2)

!     Flavour

      ID(1)=IDTAB1(IPROC)
      ID(2)=IDTAB2(IPROC)
      ID(3)=5
      ID(4)=-5


      IF (IB1 .EQ. 1) THEN
        RCH=RAN2(idum)          ! W- from B decay (first fermion, second antifermion)
        ICH=INT(RCH*9d0)+1
        ID(5)=-IDTABW2(ICH)
        ID(6)=-IDTABW1(ICH)
        RCH=RAN2(idum)          ! W+ from t decay (first fermion, second antifermion)
        ICH=INT(RCH*9d0)+1
        ID(9)=IDTABW1(ICH)
        ID(10)=IDTABW2(ICH)
      ELSE IF (IB1 .EQ. 2) THEN
        ID(9)=0
        ID(10)=0
        RCH=RAN2(idum)
        IF (IZF1 .EQ. 0) THEN
          ICH=INT(RCH*3d0)+1
          ID(5)=IDTABZN(ICH)
          ID(6)=-IDTABZN(ICH)
        ELSE IF (IZF1 .EQ. 1) THEN
          ICH=INT(RCH*3d0)+1
          ID(5)=IDTABZL(ICH)
          ID(6)=-IDTABZL(ICH)
        ELSE IF (IZF1 .EQ. 2) THEN
          ICH=INT(RCH*2d0)+1
          ID(5)=IDTABZU(ICH)
          ID(6)=-IDTABZU(ICH)
        ELSE IF (IZF1 .EQ. 3) THEN
          ICH=INT(RCH*3d0)+1
          ID(5)=IDTABZD(ICH)
          ID(6)=-IDTABZD(ICH)
        ELSE
          PRINT *,'Wrong IZF1 = ',IZF1
          STOP
        ENDIF
      ELSE
        ID(5)=0
        ID(6)=0
        ID(9)=0
        ID(10)=0
      ENDIF

      IF (IB2 .EQ. 1) THEN
        RCH=RAN2(idum)          ! W+ from Bbar decay (first fermion, second antifermion)
        ICH=INT(RCH*9d0)+1
        ID(7)=IDTABW1(ICH)
        ID(8)=IDTABW2(ICH)
        RCH=RAN2(idum)          ! W- from tbar decay (first fermion, second antifermion)
        ICH=INT(RCH*9d0)+1
        ID(11)=-IDTABW2(ICH)
        ID(12)=-IDTABW1(ICH)
      ELSE IF (IB2 .EQ. 2) THEN
        ID(11)=0
        ID(12)=0
        RCH=RAN2(idum)
        IF (IZF2 .EQ. 0) THEN
          ICH=INT(RCH*3d0)+1
          ID(7)=IDTABZN(ICH)
          ID(8)=-IDTABZN(ICH)
        ELSE IF (IZF2 .EQ. 1) THEN
          ICH=INT(RCH*3d0)+1
          ID(7)=IDTABZL(ICH)
          ID(8)=-IDTABZL(ICH)
        ELSE IF (IZF2 .EQ. 2) THEN
          ICH=INT(RCH*2d0)+1
          ID(7)=IDTABZU(ICH)
          ID(8)=-IDTABZU(ICH)
        ELSE IF (IZF2 .EQ. 3) THEN
          ICH=INT(RCH*3d0)+1
          ID(7)=IDTABZD(ICH)
          ID(8)=-IDTABZD(ICH)
        ELSE
          PRINT *,'Wrong IZF2 = ',IZF2
          STOP
        ENDIF
      ELSE
        ID(7)=0
        ID(8)=0
        ID(11)=0
        ID(12)=0
      ENDIF

!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

!     Select colour structure: g g, q q~, q~ q

      IF (IPROC .EQ. 1) THEN
        ICSTR=1
      ELSE IF (IPROC .LE. 5) THEN
        ICSTR=2
      ELSE
        ICSTR=3
      ENDIF
      NFLOWS=CAMP2(ICSTR,0)

!     Calculate probabilities for each colour flow
      
      SIGtot=0d0
      DO i=1,NFLOWS
        SIGtot=SIGtot+ABS(CAMP2(ICSTR,i))
      ENDDO
      CFAPROB(0)=0d0
      DO i=1,NFLOWS
        CFAPROB(i)=CFAPROB(i-1)+ABS(CAMP2(ICSTR,i))/SIGtot
      ENDDO

!     Select colour flow

      RCH=RAN2(IDUM)
      IFL=0
      DO WHILE (CFAPROB(IFL) .LT. RCH)
        IFL=IFL+1
      END DO
      IF (IFL .GT. NFLOWS) THEN
        PRINT *,'IFL > NFLOWS error'
        PRINT *,IFL,nflows
        STOP
      ENDIF

!     Store all colours

      ICOL1(1)=COL(1,1)
      ICOL2(1)=COL(1,2)
      ICOL1(2)=COL(2,1)
      ICOL2(2)=COL(2,2)
      ICOL1(3)=COL(3,1)
      ICOL2(3)=COL(3,2)
      ICOL1(4)=COL(4,1)
      ICOL2(4)=COL(4,2)

      DO i=5,6
        IF ((ABS(ID(i)) .GT. 0) .AND. (ABS(ID(i)) .LT. 10)) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=11
          ELSE
            ICOL2(i)=11
          ENDIF
        ENDIF
      ENDDO

      DO i=7,8
        IF ((ABS(ID(i)) .GT. 0) .AND. (ABS(ID(i)) .LT. 10)) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=12
          ELSE
            ICOL2(i)=12
          ENDIF
        ENDIF
      ENDDO

      DO i=9,10
        IF ((ABS(ID(i)) .GT. 0) .AND. (ABS(ID(i)) .LT. 10)) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=13
          ELSE
            ICOL2(i)=13
          ENDIF
        ENDIF
      ENDDO

      DO i=11,12
        IF ((ABS(ID(i)) .GT. 0) .AND. (ABS(ID(i)) .LT. 10)) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=14
          ELSE
            ICOL2(i)=14
          ENDIF
        ENDIF
      ENDDO

      Pz1=Q1(3)
      Pz2=Q2(3)

      WRITE (35,3010) NUMEVT,XWGTUP,2d0*Q
      WRITE (35,3015) IDB1,IDB2
      WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),Pb(1),Pb(2),Pb(3)

      IF (IB1 .EQ. 1) THEN
      WRITE (35,3030) ID(9),ICOL1(9),ICOL2(9),Pf3(1),Pf3(2),Pf3(3)
      WRITE (35,3030) ID(10),ICOL1(10),ICOL2(10),Pfb3(1),Pfb3(2),
     &  Pfb3(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pf1(1),Pf1(2),Pf1(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),Pfb1(1),Pfb1(2),Pfb1(3)
      ELSE IF (IB1 .EQ. 2) THEN
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pf1(1),Pf1(2),Pf1(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),Pfb1(1),Pfb1(2),Pfb1(3)
      ELSE IF (IB1 .EQ. 3) THEN
      WRITE (35,3030) 25,0,0,PB1(1),PB1(2),PB1(3)
      ENDIF

      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),Pbb(1),Pbb(2),Pbb(3)

      IF (IB2 .EQ. 1) THEN
      WRITE (35,3030) ID(11),ICOL1(11),ICOL2(11),Pf4(1),Pf4(2),Pf4(3)
      WRITE (35,3030) ID(12),ICOL1(12),ICOL2(12),Pfb4(1),Pfb4(2),
     &  Pfb4(3)
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),Pf2(1),Pf2(2),Pf2(3)
      WRITE (35,3030) ID(8),ICOL1(8),ICOL2(8),Pfb2(1),Pfb2(2),Pfb2(3)
      ELSE IF (IB2 .EQ. 2) THEN
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),Pf2(1),Pf2(2),Pf2(3)
      WRITE (35,3030) ID(8),ICOL1(8),ICOL2(8),Pfb2(1),Pfb2(2),Pfb2(3)
      ELSE IF (IB2 .EQ. 3) THEN
      WRITE (35,3030) 25,0,0,PB2(1),PB2(2),PB2(3)
      ENDIF

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 52
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4006) mQ,GQ,Vmix
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_tot,ERR_tot
      CLOSE (36)
      RETURN

12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3015  FORMAT (I3,' ',I3)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mB, GB, Vmix')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! BB cross section and error')
      END


      INTEGER FUNCTION COL(IPART,I)
      IMPLICIT NONE
      INTEGER IPART,I
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      INTEGER IGFLOW(2,2,8,10)
      DATA (IGFLOW(1,1,i,  1),i=1,  4)/502,501,501,  0/
      DATA (IGFLOW(1,2,i,  1),i=1,  4)/503,502,  0,503/
      DATA (IGFLOW(1,1,i,  2),i=1,  4)/501,502,501,  0/
      DATA (IGFLOW(1,2,i,  2),i=1,  4)/502,503,  0,503/
      DATA (IGFLOW(2,1,i,  1),i=1,  4)/501,  0,501,  0/
      DATA (IGFLOW(2,2,i,  1),i=1,  4)/  0,502,  0,502/

      IF (ICSTR .LT. 3) THEN
        COL=IGFLOW(ICSTR,I,IPART,IFL)
      ELSE IF (IPART .GT. 2) THEN
        COL=IGFLOW(2,I,IPART,IFL)
      ELSE
        COL=IGFLOW(2,I,3-IPART,IFL)
      ENDIF
      IF (COL .NE.0) COL=COL-500
      RETURN
      END
