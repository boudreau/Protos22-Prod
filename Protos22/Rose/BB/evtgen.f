      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      REAL*8 NGQ,NGt,NGW,NGZ
      COMMON /BWdata/ NGQ,NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 WTT1(3),WTT2(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRT2ac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTT2,WTZ,BRT1ac,BRT2ac,BRZac
      INTEGER IDW1,IDZ1,IDH1,IDW2,IDZ2,IDH2
      COMMON /Qfstate/ IDW1,IDZ1,IDH1,IDW2,IDZ2,IDH2
      INTEGER IZL1,IZL2
      COMMON /Qtweak/ IZL1,IZL2

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      INTEGER IMOD,IB1,IB2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IZF1,IZF2
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PQ1(0:3),PQ2(0:3),Pt1(0:3),Pt2(0:3),PW1(0:3),PW2(0:3)
      COMMON /MOMINT/ PQ1,PQ2,Pt1,Pt2,PW1,PW2
      REAL*8 PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ PB1,PB2
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3),
     &  Pf3(0:3),Pfb3(0:3),Pf4(0:3),Pfb4(0:3)
      COMMON /MOMEXT/ Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,Pf3,Pfb3,Pf4,Pfb4
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 countB(3,3)
      COMMON /testdec/ countB

!     Local variables

      INTEGER i
      REAL*8 EPS,y1,y2,flux
      REAL*8 PCM_LAB(0:3)
      REAL*8 QQ1,QQ2,QW1,QW2,Qt1,Qt2,QB1,QB2,mf1,mf2,m1,m2
      REAL*8 WT1,WT2,WT3,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS
      REAL*8 RCH,ICH
      REAL*8 Pdum1(0:3),Pdum2(0:3)


!     --------------------------------------------

      WTPS=0d0

      IDIR=0
      IF (IPROC .GT. 5) IDIR=1

!     ------------------
!     Select final state
!     ------------------

      WTFS=1d0

!     Decay of Q1, Q2: Wt,Zb,Hb

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT1ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB1=ICH

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT2ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB2=ICH

      WTFS=WTFS*WTT1(IB1)*WTT2(IB2)
      countB(IB1,IB2)=countB(IB1,IB2)+1d0

!     Fermions from Z1,Z2 decay (if any)

      IZF1=-1
      IF (IB1 .EQ. 1) THEN
        WTFS=WTFS*81d0
      ELSE IF (IB1 .EQ. 2) THEN
        IF (IZL1 .EQ. 1) THEN
          IZF1=1                      ! only leptons
          WTFS=WTFS*3d0
        ELSE
          RCH=RAN2(idum)
          ICH=0
          DO WHILE (BRZac(ICH) .LT. RCH)
            ICH=ICH+1
          ENDDO
          IZF1=ICH-1
          WTFS=WTFS*WTZ(ICH)
        ENDIF
      ENDIF

      IZF2=-1
      IF (IB2 .EQ. 1) THEN
        WTFS=WTFS*81d0
      ELSE IF (IB2 .EQ. 2) THEN
        IF (IZL2 .EQ. 1) THEN
          IZF2=1                      ! only leptons
          WTFS=WTFS*3d0
        ELSE
          RCH=RAN2(idum)
          ICH=0
          DO WHILE (BRZac(ICH) .LT. RCH)
            ICH=ICH+1
          ENDDO
          IZF2=ICH-1
          WTFS=WTFS*WTZ(ICH)
        ENDIF
      ENDIF

!     Generation of the masses of the virtual particles

      CALL BREIT(mQ,GQ,NGQ,QQ1,WT1)
      CALL BREIT(mQ,GQ,NGQ,QQ2,WT2)
      WT_BREIT=WT1*WT2*(2d0*pi)**6

      IF (IB1 .EQ. 1) THEN                          ! Q1 -> Wt
        CALL BREIT(MW,GW,NGW,QB1,WT1)
        CALL BREIT(mt,Gt,NGt,Qt1,WT2)
        IF (QB1+Qt1 .GT. QQ1) RETURN
        CALL BREIT(MW,GW,NGW,QW1,WT3)               ! and t -> Wb
        IF (QW1+mb .GT. Qt1) RETURN
        mf1=Qt1
        WT_BREIT=WT_BREIT*WT1*WT2*WT3*(2d0*pi)**9
      ELSE IF (IB1 .EQ. 2) THEN                     ! Q1 -> Zb
        CALL BREIT(MZ,GZ,NGZ,QB1,WT1)
        IF (QB1+mb .GT. QQ1) RETURN
        mf1=mb
        WT_BREIT=WT_BREIT*WT1*(2d0*pi)**3
      ELSE                                          ! Q1 -> Hb
        QB1=MH
        mf1=mb
        IF (QB1+mb .GT. QQ1) RETURN
      ENDIF

      IF (IB2 .EQ. 1) THEN                          ! Q2 -> Wt
        CALL BREIT(MW,GW,NGW,QB2,WT1)
        CALL BREIT(mt,Gt,NGt,Qt2,WT2)
        IF (QB2+Qt2 .GT. QQ2) RETURN
        CALL BREIT(MW,GW,NGW,QW2,WT3)               ! and t -> Wb
        IF (QW2+mb .GT. Qt2) RETURN
        mf2=Qt2
        WT_BREIT=WT_BREIT*WT1*WT2*WT3*(2d0*pi)**9
      ELSE IF (IB2 .EQ. 2) THEN                     ! Q2 -> Zb
        CALL BREIT(MZ,GZ,NGZ,QB2,WT1)
        IF (QB2+mb .GT. QQ2) RETURN
        mf2=mb
        WT_BREIT=WT_BREIT*WT1*(2d0*pi)**3
      ELSE                                          ! Q2 -> Hb
        QB2=MH
        mf2=mb
        IF (QB2+mb .GT. QQ2) RETURN
      ENDIF

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(QQ1+QQ2)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system

      m1=0d0
      m2=0d0
      
      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Set to zero internal, not needed but better for plots

      Pt1(0)=0d0
      DO i=1,3
        Pt1(i)=0d0
      ENDDO
      CALL STORE(Pt1,Pt2)
      CALL STORE(Pt1,PW1)
      CALL STORE(Pt1,PW2)
      CALL STORE(Pt1,Pf1)
      CALL STORE(Pt1,Pfb1)
      CALL STORE(Pt1,Pf2)
      CALL STORE(Pt1,Pfb2)
      CALL STORE(Pt1,Pf3)
      CALL STORE(Pt1,Pfb3)
      CALL STORE(Pt1,Pf4)
      CALL STORE(Pt1,Pfb4)

!     Generation

      CALL PHASE2sym(SQRT(s),QQ1,QQ2,PCM_LAB,PQ1,PQ2,WT1)
      WT_PROD=(2d0*pi)**4*WT1

!     Q1,Q2 decay 

      CALL PHASE2(QQ1,QB1,mf1,PQ1,PB1,Pdum1,WT1)
      CALL PHASE2(QQ2,QB2,mf2,PQ2,PB2,Pdum2,WT2)
      WT_DEC=WT1*WT2

!     B1,B2 decay

      IF (IB1 .NE. 3) THEN
        CALL PHASE2(QB1,0d0,0d0,PB1,Pf1,Pfb1,WT1)
        WT_DEC=WT_DEC*WT1
      ENDIF
      IF (IB2 .NE. 3) THEN
        CALL PHASE2(QB2,0d0,0d0,PB2,Pf2,Pfb2,WT2)
        WT_DEC=WT_DEC*WT2
      ENDIF

!     Decay of the quarks if IB = 1 (top)

      IF (IB1 .EQ. 1) THEN                           ! t -> Wb
        CALL STORE(Pdum1,Pt1)
        CALL PHASE2(Qt1,QW1,mb,Pt1,PW1,Pb,WT1)
        CALL PHASE2(QW1,0d0,0d0,PW1,Pf3,Pfb3,WT2)
        WT_DEC=WT_DEC*WT1*WT2
      ELSE
        CALL STORE(Pdum1,Pb)
      ENDIF
        
      IF (IB2 .EQ. 1) THEN                           ! t -> Wb
        CALL STORE(Pdum2,Pt2)
        CALL PHASE2(Qt2,QW2,mb,Pt2,PW2,Pbb,WT1)
        CALL PHASE2(QW2,0d0,0d0,PW2,Pf4,Pfb4,WT2)
        WT_DEC=WT_DEC*WT1*WT2
      ELSE
        CALL STORE(Pdum2,Pbb)
      ENDIF

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux

      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local

!     --------------------------------------------

      INOT=1 

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 GG_BB,UU_BB

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      INTEGER IMOD,IB1,IB2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IZF1,IZF2
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2

      INTEGER nhel12(NPART,3**NPART),nhel10(NPART,3**NPART),
     &        nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &        nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb12,ncomb10,ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel12,nhel10,nhel8,nhel6,nhel4,GOODHEL,
     &  ncomb12,ncomb10,ncomb8,ncomb6,ncomb4,ntry

      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER i,j,k,ISTAT,IAMP,ncomb
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=mQ*QFAC

      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .LT. 0) RETURN

      STR(1)=fg1*fg2          ! g  g  -  No factor here nor in global fact() 
      IF (IPPBAR .EQ. 0) THEN
      STR(2)=fu1*fus2
      STR(3)=fd1*fds2
      STR(4)=fs1*fs2
      STR(5)=fc1*fc2
      STR(6)=fus1*fu2
      STR(7)=fds1*fd2
      STR(8)=fs1*fs2
      STR(9)=fc1*fc2
      ELSE
      STR(2)=fu1*fu2
      STR(3)=fd1*fd2
      STR(4)=fs1*fs2
      STR(5)=fc1*fc2
      STR(6)=fus1*fus2
      STR(7)=fds1*fds2
      STR(8)=fs1*fs2
      STR(9)=fc1*fc2
      ENDIF

!     Set "coloured" squared amplitudes to zero

      DO i=1,3
        DO j=1,MAXFL
          CAMP2(i,j)=0d0
        ENDDO
      ENDDO

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IB1 .EQ. 2) IAMP=IAMP+NPROC
      IF (IB2 .EQ. 2) IAMP=IAMP+2*NPROC
      IF (IB1 .EQ. 3) IAMP=IAMP+4*NPROC
      IF (IB2 .EQ. 3) IAMP=IAMP+8*NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 500) ntry(IAMP)=500
      
      IF        ((IB1 .EQ. 1) .AND. (IB2 .EQ. 1)) THEN
        ncomb=ncomb12
      ELSE IF ( ((IB1 .EQ. 1) .AND. (IB2 .EQ. 2))
     &  .OR.    ((IB1 .EQ. 2) .AND. (IB2 .EQ. 1)) ) THEN
        ncomb=ncomb10
      ELSE IF ( ((IB1 .EQ. 1) .AND. (IB2 .EQ. 3))
     &  .OR.    ((IB1 .EQ. 3) .AND. (IB2 .EQ. 1)) 
     &  .OR.    ((IB1 .EQ. 2) .AND. (IB2 .EQ. 2)) ) THEN
        ncomb=ncomb8
      ELSE IF ( ((IB1 .EQ. 2) .AND. (IB2 .EQ. 3)) 
     &  .OR.    ((IB1 .EQ. 3) .AND. (IB2 .EQ. 2)) ) THEN
        ncomb=ncomb6
      ELSE IF   ((IB1 .EQ. 3) .AND. (IB2 .EQ. 3)) THEN
        ncomb=ncomb4
      ELSE
        PRINT *,'You messed something'
        STOP
      ENDIF

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (IPROC .NE. 1) GOTO 12

!     g g -> t t~

      ME=0d0
      ICSTR=1
      CAMP2(ICSTR,0)=2
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN

          IF        ((IB1 .EQ. 1) .AND. (IB2 .EQ. 1)) THEN
            M1=GG_BB(nhel12(1,k))
          ELSE IF ( ((IB1 .EQ. 1) .AND. (IB2 .EQ. 2))
     &      .OR.    ((IB1 .EQ. 2) .AND. (IB2 .EQ. 1)) ) THEN
            M1=GG_BB(nhel10(1,k))
          ELSE IF ( ((IB1 .EQ. 1) .AND. (IB2 .EQ. 3))
     &      .OR.    ((IB1 .EQ. 3) .AND. (IB2 .EQ. 1)) 
     &      .OR.    ((IB1 .EQ. 2) .AND. (IB2 .EQ. 2)) ) THEN
            M1=GG_BB(nhel8(1,k))
          ELSE IF ( ((IB1 .EQ. 2) .AND. (IB2 .EQ. 3)) 
     &      .OR.    ((IB1 .EQ. 3) .AND. (IB2 .EQ. 2)) ) THEN
            M1=GG_BB(nhel6(1,k))
          ELSE IF   ((IB1 .EQ. 3) .AND. (IB2 .EQ. 3)) THEN
            M1=GG_BB(nhel4(1,k))
          ENDIF

          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

      GOTO 20

12    CONTINUE

!     q q~

      ME=0d0
      ICSTR=2
      IF (IDIR .EQ. 1) ICSTR=3
      CAMP2(ICSTR,0)=1
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN

          IF        ((IB1 .EQ. 1) .AND. (IB2 .EQ. 1)) THEN
            M1=UU_BB(nhel12(1,k))
          ELSE IF ( ((IB1 .EQ. 1) .AND. (IB2 .EQ. 2))
     &      .OR.    ((IB1 .EQ. 2) .AND. (IB2 .EQ. 1)) ) THEN
            M1=UU_BB(nhel10(1,k))
          ELSE IF ( ((IB1 .EQ. 1) .AND. (IB2 .EQ. 3))
     &      .OR.    ((IB1 .EQ. 3) .AND. (IB2 .EQ. 1)) 
     &      .OR.    ((IB1 .EQ. 2) .AND. (IB2 .EQ. 2)) ) THEN
            M1=UU_BB(nhel8(1,k))
          ELSE IF ( ((IB1 .EQ. 2) .AND. (IB2 .EQ. 3)) 
     &      .OR.    ((IB1 .EQ. 3) .AND. (IB2 .EQ. 2)) ) THEN
            M1=UU_BB(nhel6(1,k))
          ELSE IF   ((IB1 .EQ. 3) .AND. (IB2 .EQ. 3)) THEN
            M1=UU_BB(nhel4(1,k))
          ENDIF

          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
     
      RETURN
      END




