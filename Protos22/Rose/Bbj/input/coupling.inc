!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Z couplings to SM fermions

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

!     Gluon coupling

      GG(1)=-gs
      GG(2)=-gs

!     Heavy quark couplings
      
      Xmix=Vmix

      IF (IMOD .EQ. 1) THEN
        GWtQ(1)=GWF(1)*Vmix
        GWtQ(2)=0d0
        GZQb(1)=gcw/2d0*Xmix
        GZQb(2)=0d0
        GHQb(1)=-g*mb/(2d0*MW)*Xmix                   ! B in b out
        GHQb(2)=-g*mQ/(2d0*MW)*Xmix
      ELSE IF (IMOD .EQ. 2) THEN                      ! (T B) doublet equal mixing
        GWtQ(1)=0d0
        GWtQ(2)=GWF(1)*Vmix
        GZQb(1)=0d0
        GZQb(2)=gcw/2d0*Xmix
        GHQb(1)=g*mQ/(2d0*MW)*Xmix                    ! B in b out
        GHQb(2)=g*mb/(2d0*MW)*Xmix
      ELSE IF (IMOD .EQ. 3) THEN                      ! (T B) doublet up mixing
        GWtQ(1)=0d0
        GWtQ(2)=GWF(1)*Vmix
        GZQb(1)=0d0
        GZQb(2)=0d0
        GHQb(1)=0d0                                   ! B in b out
        GHQb(2)=0d0
      ELSE IF (IMOD .EQ. 4) THEN                      ! (B Y) doublet
        GWtQ(1)=0d0
        GWtQ(2)=0d0
        GZQb(1)=0d0
        GZQb(2)=-gcw/2d0*Xmix
        GHQb(1)=g*mQ/(2d0*MW)*Xmix                    ! B in b out
        GHQb(2)=g*mb/(2d0*MW)*Xmix
      ENDIF

      GHbQ(1)=CONJG(GHQb(2))                          ! b in B out
      GHbQ(2)=CONJG(GHQb(1))


