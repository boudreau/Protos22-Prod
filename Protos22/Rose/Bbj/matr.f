      REAL*8 FUNCTION Bbj(NHEL)

!     FOR PROCESS : g q  -> B b~ q  /  g q~  ->  B b~ q~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=9)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf1(0:3),Pfb1(0:3),Pf3(0:3),Pfb3(0:3)
      COMMON /MOMEXT/ Pb,Pj,PbX,Pf1,Pfb1,Pf3,Pfb3
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf3(6),Wfb3(6)
      COMPLEX*16 WB1(6),WW1(6),Wb(6),Wt(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IQ,IB1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IZF1

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWtQ(2),GZQb(2),Xmix
      COMPLEX*16 GHQb(2),GHbQ(2)

!     Specific couplings

      REAL*8 GB1ff(2),GZqq(2)

!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (ABS(IQ) .EQ. 2) THEN
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
      ELSE IF (ABS(IQ) .EQ. 1) THEN
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Bj'
        STOP
      ENDIF
      
      IF (IB1 .NE. 2) GOTO 4

      IF (IZF1 .EQ. 0) THEN
        GB1ff(1)=GZvv(1)
        GB1ff(2)=GZvv(2)
      ELSE IF (IZF1 .EQ. 1) THEN
        GB1ff(1)=GZll(1)
        GB1ff(2)=GZll(2)
      ELSE IF (IZF1 .EQ. 2) THEN
        GB1ff(1)=GZuu(1)
        GB1ff(2)=GZuu(2)
      ELSE IF (IZF1 .EQ. 3) THEN
        GB1ff(1)=GZdd(1)
        GB1ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF1 = ',IZF1
        STOP
      ENDIF

4     CONTINUE

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL IXXXXX(PbX,mb,NHEL(5),-1,W5)             ! bbar from gluon splitting

      IF (IQ .GT. 0) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! q initial
        CALL OXXXXX(Pj,0d0,NHEL(4),1,W4)            ! q final
      ELSE
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! qbar initial
        CALL IXXXXX(Pj,0d0,NHEL(4),-1,W4)           ! qbar final
      ENDIF

!     B

      CALL OXXXXX(Pb,mb,NHEL(3),1,Wb)                ! b final
      IF (IB1 .EQ. 1) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(6),1,Wf1)           ! f1     - W- from B decay
        CALL IXXXXX(Pfb1,0d0,NHEL(7),-1,Wfb1)        ! f1bar
        CALL OXXXXX(Pf3,0d0,NHEL(8),1,Wf3)           ! f3     - W+ from t decay
        CALL IXXXXX(Pfb3,0d0,NHEL(9),-1,Wfb3)        ! f3bar

        CALL JIOXXX(Wfb3,Wf3,GWF,MW,GW,WW1)          ! W+
        CALL FVOXXX(Wb,WW1,GWF,mt,Gt,Wt)             ! t
        CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)          ! W-
        CALL FVOXXX(Wt,WB1,GWtQ,mQ,GQ,W3)            ! B
      ELSE IF (IB1 .EQ. 2) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(6),1,Wf1)           ! f1     - Z from B decay
        CALL IXXXXX(Pfb1,0d0,NHEL(7),-1,Wfb1)        ! f1bar

        CALL JIOXXX(Wfb1,Wf1,GB1ff,MZ,GZ,WB1)        ! Z
        CALL FVOXXX(Wb,WB1,GZQb,mQ,GQ,W3)            ! B
      ELSE IF (IB1 .EQ. 3) THEN
        CALL SXXXXX(PB1,1,WB1)                       ! H
        CALL FSOXXX(Wb,WB1,GHQb,mQ,GQ,W3)            ! B
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in Bj'
        STOP
      ENDIF

      IF (IQ .GT. 0) THEN
        CALL JIOXXX(W2,W4,GZqq,MZ,GZ,W7)
      ELSE
        CALL JIOXXX(W4,W2,GZqq,MZ,GZ,W7)
      ENDIF

      CALL FVIXXX(W5,W1,GG,mb,0d0,W6)
      CALL IOVXXX(W6,W3,W7,GZQb,AMP(1))
 
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W5,W8,W7,GZQb,AMP(2))


      Bbj = 0D0
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Bbj = Bbj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


      REAL*8 FUNCTION Bbarbj(NHEL)

!     FOR PROCESS : g q  -> B~ b q  /  g q~  ->  B~ b q~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=9)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf1(0:3),Pfb1(0:3),Pf3(0:3),Pfb3(0:3)
      COMMON /MOMEXT/ Pb,Pj,PbX,Pf1,Pfb1,Pf3,Pfb3
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf3(6),Wfb3(6)
      COMPLEX*16 WB1(6),WW1(6),Wb(6),Wt(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IQ,IB1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IZF1

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWtQ(2),GZQb(2),Xmix
      COMPLEX*16 GHQb(2),GHbQ(2)

!     Specific couplings

      REAL*8 GB1ff(2),GZqq(2)

!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (ABS(IQ) .EQ. 2) THEN
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
      ELSE IF (ABS(IQ) .EQ. 1) THEN
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Bj'
        STOP
      ENDIF

      IF (IB1 .NE. 2) GOTO 4

      IF (IZF1 .EQ. 0) THEN
        GB1ff(1)=GZvv(1)
        GB1ff(2)=GZvv(2)
      ELSE IF (IZF1 .EQ. 1) THEN
        GB1ff(1)=GZll(1)
        GB1ff(2)=GZll(2)
      ELSE IF (IZF1 .EQ. 2) THEN
        GB1ff(1)=GZuu(1)
        GB1ff(2)=GZuu(2)
      ELSE IF (IZF1 .EQ. 3) THEN
        GB1ff(1)=GZdd(1)
        GB1ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF1 = ',IZF1
        STOP
      ENDIF

4     CONTINUE

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL OXXXXX(PbX,mb,NHEL(5),1,W5)              ! b from gluon splitting

      IF (IQ .GT. 0) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! q initial
        CALL OXXXXX(Pj,0d0,NHEL(4),1,W4)            ! q final
      ELSE
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! qbar initial
        CALL IXXXXX(Pj,0d0,NHEL(4),-1,W4)           ! qbar final
      ENDIF

!     Bbar

      CALL IXXXXX(Pb,mb,NHEL(3),-1,Wb)               ! bbar final
      IF (IB1 .EQ. 1) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(6),1,Wf1)           ! f1     - W+ from Bbar decay
        CALL IXXXXX(Pfb1,0d0,NHEL(7),-1,Wfb1)        ! f1bar
        CALL OXXXXX(Pf3,0d0,NHEL(8),1,Wf3)           ! f3     - W- from tbar decay
        CALL IXXXXX(Pfb3,0d0,NHEL(9),-1,Wfb3)        ! f3bar

        CALL JIOXXX(Wfb3,Wf3,GWF,MW,GW,WW1)          ! W-
        CALL FVIXXX(Wb,WW1,GWF,mt,Gt,Wt)             ! tbar
        CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)          ! W+
        CALL FVIXXX(Wt,WB1,GWtQ,mQ,GQ,W3)            ! Bbar
      ELSE IF (IB1 .EQ. 2) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(6),1,Wf1)           ! f1     - Z from Bbar decay
        CALL IXXXXX(Pfb1,0d0,NHEL(7),-1,Wfb1)        ! f1bar

        CALL JIOXXX(Wfb1,Wf1,GB1ff,MZ,GZ,WB1)        ! Z
        CALL FVIXXX(Wb,WB1,GZQb,mQ,GQ,W3)            ! B
      ELSE IF (IB1 .EQ. 3) THEN
        CALL SXXXXX(PB1,1,WB1)                       ! H
        CALL FSIXXX(Wb,WB1,GHbQ,mQ,GQ,W3)            ! B
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in Bbarj'
        STOP
      ENDIF


      IF (IQ .GT. 0) THEN
        CALL JIOXXX(W2,W4,GZqq,MZ,GZ,W7)
      ELSE
        CALL JIOXXX(W4,W2,GZqq,MZ,GZ,W7)
      ENDIF

      CALL FVOXXX(W5,W1,GG,mb,0d0,W6)
      CALL IOVXXX(W3,W6,W7,GZQb,AMP(1))

      CALL FVIXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W8,W5,W7,GZQb,AMP(2))

      Bbarbj = 0D0
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Bbarbj = Bbarbj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



