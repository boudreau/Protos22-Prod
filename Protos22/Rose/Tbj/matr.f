      REAL*8 FUNCTION Tbj(NHEL)

!     FOR PROCESS : g u  -> T b~ d

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=9)

!     ARGUMENTS

      INTEGER NHEL(NEXTERNAL)    

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX,Pf2,Pfb2
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1

!     LOCAL VARIABLES

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6)
      COMPLEX*16 WW1(6),WB1(6),Wb(6),Wt(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),GZQt(2),Xmix
      COMPLEX*16 GHQt(2),GHtQ(2)

!     Specific couplings

      REAL*8 GB1ff(2)

!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (IZF1 .EQ. 0) THEN
        GB1ff(1)=GZvv(1)
        GB1ff(2)=GZvv(2)
      ELSE IF (IZF1 .EQ. 1) THEN
        GB1ff(1)=GZll(1)
        GB1ff(2)=GZll(2)
      ELSE IF (IZF1 .EQ. 2) THEN
        GB1ff(1)=GZuu(1)
        GB1ff(2)=GZuu(2)
      ELSE IF (IZF1 .EQ. 3) THEN
        GB1ff(1)=GZdd(1)
        GB1ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF1 = ',IZF1
        STOP
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL IXXXXX(PbX,mb,NHEL(6),-1,W4)             ! bbar

      IF (IQ .EQ. 2) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! u
        CALL OXXXXX(Pj,0d0,NHEL(7),1,W5)            ! d
      ELSE IF (IQ .EQ. -1) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! dbar
        CALL IXXXXX(Pj,0d0,NHEL(7),-1,W5)           ! ubar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Tbj'
        STOP
      ENDIF

      CALL OXXXXX(Pf1,0d0,NHEL(3),1,Wf1)            ! nu
      CALL IXXXXX(Pfb1,0d0,NHEL(4),-1,Wfb1)         ! e+
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WW1)           ! W+
      CALL OXXXXX(Pb,mb,NHEL(5),1,Wb)               ! b

      IF (IB1 .EQ. 1) THEN
        CALL FVOXXX(Wb,WW1,GWQb,mQ,GQ,W3)           ! T
      ELSE IF (IB1 .EQ. 2) THEN
        CALL FVOXXX(Wb,WW1,GWF,mt,Gt,Wt)            ! t
        CALL OXXXXX(Pf2,0d0,NHEL(8),1,Wf2)          ! f2: q, l-, nu
        CALL IXXXXX(Pfb2,0d0,NHEL(9),-1,Wfb2)       ! f2bar: q~, l+, nu~ 
        CALL JIOXXX(Wfb2,Wf2,GB1ff,MZ,GZ,WB1)       ! Z
        CALL FVOXXX(Wt,WB1,GZQt,mQ,GQ,W3)           ! T
      ELSE IF (IB1 .EQ. 3) THEN
        CALL FVOXXX(Wb,WW1,GWF,mt,Gt,Wt)            ! t
        CALL SXXXXX(PB1,1,WB1)                      ! H
        CALL FSOXXX(Wt,WB1,GHQt,mQ,GQ,W3)           ! T
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in GU_Tbj'
        STOP
      ENDIF

      IF (IQ .EQ. 2) THEN
        CALL JIOXXX(W2,W5,GWF,MW,GW,W7)
      ELSE
        CALL JIOXXX(W5,W2,GWF,MW,GW,W7)
      ENDIF

      CALL FVIXXX(W4,W1,GG,mb,0d0,W6)
      CALL IOVXXX(W6,W3,W7,GWQb,AMP(1))
 
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W4,W8,W7,GWQb,AMP(2))

      TBj = 0D0 
      DO I = 1, NEIGEN
        ZTEMP = (0d0,0d0)
        DO J = 1, NGRAPHS
          ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
        ENDDO
        TBj = TBj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



      REAL*8 FUNCTION Tbarbj(NHEL)

!     FOR PROCESS : g u~  -> T~ b d~  

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=9)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX,Pf2,Pfb2
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6)
      COMPLEX*16 WW1(6),WB1(6),Wb(6),Wt(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),GZQt(2),Xmix
      COMPLEX*16 GHQt(2),GHtQ(2)

!     Specific couplings

      REAL*8 GB1ff(2)

!     Colour data

      DATA EIGEN_VAL(1) /0.5d0/                  
      DATA EIGEN_VEC(1,1) /-1d0/                  
      DATA EIGEN_VEC(2,1) /-1d0/                  

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (IZF1 .EQ. 0) THEN
        GB1ff(1)=GZvv(1)
        GB1ff(2)=GZvv(2)
      ELSE IF (IZF1 .EQ. 1) THEN
        GB1ff(1)=GZll(1)
        GB1ff(2)=GZll(2)
      ELSE IF (IZF1 .EQ. 2) THEN
        GB1ff(1)=GZuu(1)
        GB1ff(2)=GZuu(2)
      ELSE IF (IZF1 .EQ. 3) THEN
        GB1ff(1)=GZdd(1)
        GB1ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF1 = ',IZF1
        STOP
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL OXXXXX(PbX,mb,NHEL(6),1,W4)              ! b

      IF (IQ .EQ. -2) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! ubar
        CALL IXXXXX(Pj,0d0,NHEL(7),-1,W5)           ! dbar
      ELSE IF (IQ .EQ. 1) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! d
        CALL OXXXXX(Pj,0d0,NHEL(7),1,W5)            ! u
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Tbarbj'
        STOP
      ENDIF

      CALL OXXXXX(Pf1,0d0,NHEL(3),1,Wf1)            ! e-
      CALL IXXXXX(Pfb1,0d0,NHEL(4),-1,Wfb1)         ! nu~
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WW1)           ! W-
      CALL IXXXXX(Pb,mb,NHEL(5),-1,Wb)              ! b~

      IF (IB1 .EQ. 1) THEN
        CALL FVIXXX(Wb,WW1,GWQb,mQ,GQ,W3)           ! T~
      ELSE IF (IB1 .EQ. 2) THEN
        CALL FVIXXX(Wb,WW1,GWF,mt,Gt,Wt)            ! t~
        CALL OXXXXX(Pf2,0d0,NHEL(8),1,Wf2)          ! f2: q, l-, nu
        CALL IXXXXX(Pfb2,0d0,NHEL(9),-1,Wfb2)       ! f2bar: q~, l+, nu~ 
        CALL JIOXXX(Wfb2,Wf2,GB1ff,MZ,GZ,WB1)       ! Z
        CALL FVIXXX(Wt,WB1,GZQt,mQ,GQ,W3)           ! t~
      ELSE IF (IB1 .EQ. 3) THEN
        CALL FVIXXX(Wb,WW1,GWF,mt,Gt,Wt)            ! t~
        CALL SXXXXX(PB1,1,WB1)                      ! H
        CALL FSIXXX(Wt,WB1,GHtQ,mQ,GQ,W3)           ! T~
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in GUB_Tbj'
        STOP
      ENDIF

      IF (IQ .EQ. -2) THEN
        CALL JIOXXX(W5,W2,GWF,MW,GW,W7)
      ELSE
        CALL JIOXXX(W2,W5,GWF,MW,GW,W7)
      ENDIF

      CALL FVOXXX(W4,W1,GG,mb,0d0,W6)
      CALL IOVXXX(W3,W6,W7,GWQb,AMP(1))

      CALL FVIXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W8,W4,W7,GWQb,AMP(2))

      Tbarbj = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Tbarbj = Tbarbj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


