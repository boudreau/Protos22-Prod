      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

10    CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      REAL*8 NGQ,NGt,NGW,NGZ
      COMMON /BWdata/ NGQ,NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 WTT1(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTZ,BRT1ac,BRZac
      INTEGER IDW1,IDZ1,IDH1
      COMMON /Qfstate/ IDW1,IDZ1,IDH1
      INTEGER IZL1
      COMMON /Qtweak/ IZL1

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX,Pf2,Pfb2
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR
      REAL*8 countB(3)
      COMMON /testdec/ countB

!     Local variables

      INTEGER i
      REAL*8 EPS,y1,y2,flux,s
      REAL*8 PCM_LAB(0:3)
      REAL*8 QQ1,QW1,Qt1,QB1
      REAL*8 WT1,WT2,WT3,WT4,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS
      REAL*8 RCH,ICH

!     --------------------------------------------

      WTPS=0d0

      IDIR=0
      IF (IPROC .GT. 8) IDIR=1

!     ------------------
!     Select final state
!     ------------------

      WTFS=1d0

!     Decay of Q1: Wb,Zt,Ht

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT1ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB1=ICH

      WTFS=WTFS*WTT1(IB1)
      countB(IB1)=countB(IB1)+1d0

!     Fermions from W1 decay and Z1 (if any)

      IWF1=1                        ! Kept for future use, perhaps
      WTFS=WTFS*9d0

      IF (IB1 .EQ. 2) THEN
        IF (IZL1 .EQ. 1) THEN
          IZF1=1                      ! only leptons
          WTFS=WTFS*3d0
        ELSE
          RCH=RAN2(idum)
          ICH=0
          DO WHILE (BRZac(ICH) .LT. RCH)
            ICH=ICH+1
          ENDDO
          IZF1=ICH-1
          WTFS=WTFS*WTZ(ICH)
        ENDIF
      ENDIF

!     Generation of the masses of the virtual particles

      CALL BREIT(mQ,GQ,NGQ,QQ1,WT1)
      CALL BREIT(MW,GW,NGW,QW1,WT2)
      WT_BREIT=WT1*WT2*(2d0*pi)**6

      IF (IB1 .EQ. 1) THEN                       ! Q1 -> Wb
        IF (QW1+mb .GT. QQ1) RETURN
      ELSE IF (IB1 .EQ. 2) THEN                  ! Q1 -> Zt
        CALL BREIT(mt,Gt,NGt,Qt1,WT3)
        CALL BREIT(MZ,GZ,NGZ,QB1,WT4)
        WT_BREIT=WT_BREIT*WT3*WT4*(2d0*pi)**6
        IF (QB1+Qt1 .GT. QQ1) RETURN
        IF (QW1+mb .GT. Qt1) RETURN
      ELSE                                       ! Q1 -> Ht
        CALL BREIT(mt,Gt,NGt,Qt1,WT3)
        QB1=MH
        WT_BREIT=WT_BREIT*WT3*(2d0*pi)**3
        IF (QB1+Qt1 .GT. QQ1) RETURN
        IF (QW1+mb .GT. Qt1) RETURN
      ENDIF

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(QQ1+mb)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system
      
      Q1(0)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(3)=ET*x1/2d0
              
      Q2(0)=ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(3)=-ET*x2/2d0

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)


!     Set to zero internal, not needed but good for plots

      Pt1(0)=0d0
      DO i=1,3
        Pt1(i)=0d0
      ENDDO
      CALL STORE(Pt1,PB1)

!     Generation

      CALL PHASE3tsym(SQRT(s),QQ1,mb,0d0,PCM_LAB,PQ1,PbX,Pj,WT,IDIR)
      WT_PROD=(2d0*pi)**4*WT
      WT_DEC=1d0

!     Q1 decay 

      IF (IB1 .EQ. 1) THEN                           ! T -> Wb
        WT1=1d0
        CALL PHASE2(QQ1,QW1,mb,PQ1,PW1,Pb,WT2)
      ELSE                                           ! T -> Zt, Ht
        CALL PHASE2(QQ1,QB1,Qt1,PQ1,PB1,Pt1,WT1)
        CALL PHASE2(Qt1,QW1,mb,Pt1,PW1,Pb,WT2)       ! and t -> Wb
      ENDIF
      WT_DEC=WT_DEC*WT1*WT2

!     W1 decay and B1 decay, if Z

      WT2=1d0
      CALL PHASE2(QW1,0d0,0d0,PW1,Pf1,Pfb1,WT1)
      IF (IB1 .EQ. 2) CALL PHASE2(QB1,0d0,0d0,PB1,Pf2,Pfb2,WT2)
      WT_DEC=WT_DEC*WT1*WT2

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX,Pf2,Pfb2
      REAL*8 PQ1(0:3),PB1(0:3),Pt(0:3),PW(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt,PW
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local variables

      REAL*8 PTbX

!     --------------------------------------------

      INOT=1
      
      PTbX=SQRT(PbX(1)**2+PbX(2)**2)
      IF ((IMATCH .EQ. 2) .AND. (PTbX .LT. PTbmin)) RETURN

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 TBj,Tbarbj,DOT

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3),
     &  Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX,Pf2,Pfb2
      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1

      INTEGER nhel9(NPART,3**NPART),nhel7(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb9,ncomb7,ntry(MAXAMP)
      COMMON /HELI/ nhel9,nhel7,GOODHEL,ncomb9,ncomb7,ntry

      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC1,QFAC2
      COMMON /PDFSCALE/ QFAC1,QFAC2

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      REAL*8 Ql1,Ql2,Qg
      INTEGER k,ISTAT,IAMP,ncomb
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Qg=SQRT(PbX(1)**2+PbX(2)**2+mb**2)
      CALL SETALPHAS(Qg)
      Qg=Qg*QFAC2

      Ql1=SQRT(2d0*DOT(Q2,Pj))*QFAC1
      Ql2=SQRT(2d0*DOT(Q1,Pj))*QFAC1
      Q=Ql1
      IF (IDIR .EQ. 1) Q=Ql2
      
      CALL GETPDF_tbj(x1,Ql1,Qg,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF_tbj(x2,Ql2,Qg,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN

!     g q

      IF (IPPBAR .EQ. 0) THEN
      STR(1)=fg1*fu2         ! g  u
      STR(2)=fg1*fc2         ! g  c
      STR(3)=fg1*fds2        ! g  d~
      STR(4)=fg1*fs2         ! g  s~
      STR(5)=fg1*fus2        ! g  u~
      STR(6)=fg1*fc2         ! g  c~
      STR(7)=fg1*fd2         ! g  d
      STR(8)=fg1*fs2         ! g  s
      ELSE
      STR(1)=fg1*fus2        ! g  u
      STR(2)=fg1*fc2         ! g  c
      STR(3)=fg1*fd2         ! g  d~
      STR(4)=fg1*fs2         ! g  s~
      STR(5)=fg1*fu2         ! g  u~
      STR(6)=fg1*fc2         ! g  c~
      STR(7)=fg1*fds2        ! g  d
      STR(8)=fg1*fs2         ! g  s
      ENDIF

!     q g

      STR(9)=fu1*fg2          ! u  g 
      STR(10)=fc1*fg2         ! c  g
      STR(11)=fds1*fg2        ! d~ g
      STR(12)=fs1*fg2         ! s~ g
      STR(13)=fus1*fg2        ! u~ g
      STR(14)=fc1*fg2         ! c~ g
      STR(15)=fd1*fg2         ! d  g
      STR(16)=fs1*fg2         ! s  g

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IB1 .EQ. 2) IAMP=IAMP+NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 500) ntry(IAMP) = 500

      IF (IB1 .NE. 2) THEN
        ncomb=ncomb7
      ELSE
        ncomb=ncomb9
      ENDIF

c      DO k=1,NPROC
c      IQ=(2-MOD((k-1)/2,2))*(-1)**(MOD((k-1)/2,2)+MOD((k-1)/4,2))
c      PRINT *,k,IQ,MOD((k-1)/4,2)
c      ENDDO
c      STOP

      IQ=(2-MOD((IPROC-1)/2,2))*
     &  (-1)**(MOD((IPROC-1)/2,2)+MOD((IPROC-1)/4,2))

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (MOD((IPROC-1)/4,2) .EQ. 1) GOTO 14

!     T

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          IF (IB1 .NE. 2) THEN
            M1=TBj(nhel7(1,k))
          ELSE
            M1=TBj(nhel9(1,k))
          ENDIF
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

14    CONTINUE

!     Tbar

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          IF (IB1 .NE. 2) THEN
            M1=Tbarbj(nhel7(1,k))
          ELSE
            M1=Tbarbj(nhel9(1,k))
          ENDIF
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END

