      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file
!     Arguments

      INTEGER IMODE

!     External functions used

      REAL*8 RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_t,ERR_t,SIG_tbar,ERR_tbar
      COMMON /FINALSTATS/ SIG_t,ERR_t,SIG_tbar,ERR_tbar
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables

      INTEGER IDTAB2(8),IDTAB3(8),IDTAB5(8)
      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2
      INTEGER i

      REAL*8 RCH
      INTEGER ICH
      INTEGER IDTABB1(3),IDTABB2(3)
      INTEGER IDTABW1(9),IDTABW2(9)
      INTEGER IDB1

      DATA IDTAB2 /1,3,-2,-4,-1,-3,2,4/
      DATA IDTAB3 /5,5,5,5,-5,-5,-5,-5/
      DATA IDTAB5 /2,4,-1,-3,-2,-4,1,3/

      DATA IDTABB1 /24,23,25/
      DATA IDTABB2 /-24,23,25/
      DATA IDTABW1 / 12, 14, 16, 2, 2, 2, 4, 4, 4/          ! For W+ decays
      DATA IDTABW2 /-11,-13,-15,-1,-1,-1,-3,-3,-3/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0
      
      RETURN

10    IF (IMODE .NE. 0) GOTO 11
      
!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)/1000d0                          ! In pb
      IF (XWGTUP .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,XWGTUP)
      NUMEVT=NUMEVT+1

!     Flavour

      ID(1)=21                                       ! gluon
      ID(2)=IDTAB2(MOD(IPROC,9)+IPROC/9)             ! initial q / qbar
      ID(3)=IDTAB3(MOD(IPROC,9)+IPROC/9)             ! final b / bbar from top
      ID(4)=-ID(3)                                   ! bbar / b from gluon splitting
      ID(5)=IDTAB5(MOD(IPROC,9)+IPROC/9)             ! final q / qbar

      RCH=RAN2(idum)
      ICH=INT(RCH*9d0)+1
      IF (ID(3) .LT. 0) THEN
        IDB1=IDTABB1(1)
        ID(6)=IDTABW1(ICH)      ! For W+: first fermion, second antifermion
        ID(7)=IDTABW2(ICH)
      ELSE
        IDB1=IDTABB2(1)
        ID(6)=-IDTABW2(ICH)     ! For W-: first fermion, second antifermion
        ID(7)=-IDTABW1(ICH)
      ENDIF

!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

      ICOL1(1)=2                     ! Initial g
      ICOL2(1)=3

      IF (ID(2) .GT. 0) THEN         ! Initial q / qbar
         ICOL1(2)=1
      ELSE
         ICOL2(2)=1
      ENDIF

      ICOL1(5)=ICOL1(2)              ! Final q / qbar
      ICOL2(5)=ICOL2(2)

      DO i=3,4                       ! Final b and bbar
        IF (ID(i) .GT. 0) THEN
          ICOL1(i)=ICOL1(1)
        ELSE
          ICOL2(i)=ICOL2(1)
        ENDIF
      ENDDO

      DO i=6,7
        IF ((ABS(ID(i)) .GT. 0) .AND. (ABS(ID(i)) .LT. 10)) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=11
          ELSE
            ICOL2(i)=11
          ENDIF
        ENDIF
      ENDDO

      IF (IDIR .EQ. 0) THEN
        Pz1=Q1(3)
        Pz2=Q2(3)
      ELSE
        Pz1=Q2(3)
        Pz2=Q1(3)
      ENDIF
      
      WRITE (35,3010) NUMEVT,XWGTUP,Q
      WRITE (35,3016) IDB1
      IF (Pz1 .GT. 0d0) THEN
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      ELSE
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      ENDIF
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),Pb(1),Pb(2),Pb(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),Pf1(1),Pf1(2),Pf1(3)
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),Pfb1(1),Pfb1(2),Pfb1(3)
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),PbX(1),PbX(2),PbX(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pj(1),Pj(2),Pj(3)
      RETURN

      
      
11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 66
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4006) mQ,GQ,Vmix
      WRITE (36,4007) IMATCH,PTbmin
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_t,ERR_t
      WRITE (36,4050) SIG_tbar,ERR_tbar
      CLOSE (36)
      RETURN



12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3016  FORMAT (I3)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)
   
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mY, GY, Vmix')
4007  FORMAT (I1,'  ',F5.1,'                         ! Matching')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! Y cross section and error')
4050  FORMAT (D12.6,'   ',D12.6,
     & '      ! Ybar cross section and error')
      END

