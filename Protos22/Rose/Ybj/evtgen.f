      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

10    CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      REAL*8 NGQ,NGt,NGW,NGZ
      COMMON /BWdata/ NGQ,NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET

!     External functions used

      REAL*8 XRN ! ,RAN2
c      INTEGER idum
c      COMMON /ranno/ idum

!     Outputs

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX
      REAL*8 PQ1(0:3),PB1(0:3)
      COMMON /MOMINT/ PQ1,PB1
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR

!     Local variables

      REAL*8 EPS,y1,y2,flux,s
      REAL*8 PCM_LAB(0:3)
      REAL*8 QQ1,QB1
      REAL*8 WT1,WT2,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS

!     --------------------------------------------

      WTPS=0d0

      IDIR=0
      IF (IPROC .GT. 8) IDIR=1

!     ------------------
!     Select final state
!     ------------------

      WTFS=9d0

!     Generation of the masses of the virtual particles

      CALL BREIT(mQ,GQ,NGQ,QQ1,WT1)
      CALL BREIT(MW,GW,NGW,QB1,WT2)
      IF (QB1+mb .GT. QQ1) RETURN
      WT_BREIT=WT1*WT2*(2d0*pi)**6

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(QQ1+mb)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system
      
      Q1(0)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(3)=ET*x1/2d0
              
      Q2(0)=ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(3)=-ET*x2/2d0

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Generation

      CALL PHASE3tsym(SQRT(s),QQ1,mb,0d0,PCM_LAB,PQ1,PbX,Pj,WT,IDIR)
      WT_PROD=(2d0*pi)**4*WT

!     Q1 and W decay

      CALL PHASE2(QQ1,QB1,mb,PQ1,PB1,Pb,WT1)
      CALL PHASE2(QB1,0d0,0d0,PB1,Pf1,Pfb1,WT2)
      WT_DEC=WT1*WT2

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local variables

      REAL*8 PTbX

!     --------------------------------------------

      INOT=1

      PTbX=SQRT(PbX(1)**2+PbX(2)**2)
      IF ((IMATCH .EQ. 2) .AND. (PTbX .LT. PTbmin)) RETURN

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 Ybj,Ybarbj,DOT

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX
      INTEGER IQ
      COMMON /Qflags/ IQ
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb,ntry(MAXAMP)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC1,QFAC2
      COMMON /PDFSCALE/ QFAC1,QFAC2

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      REAL*8 Ql1,Ql2,Qg
      INTEGER k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

      WTME=0d0

!     Scale for structure functions

      Qg=SQRT(PbX(1)**2+PbX(2)**2+mb**2)
      CALL SETALPHAS(Qg)
      Qg=Qg*QFAC2

      Ql1=SQRT(2d0*DOT(Q2,Pj))*QFAC1
      Ql2=SQRT(2d0*DOT(Q1,Pj))*QFAC1
      Q=Ql1
      IF (IDIR .EQ. 1) Q=Ql2
      
      CALL GETPDF_tbj(x1,Ql1,Qg,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF_tbj(x2,Ql2,Qg,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN

!     g q

      IF (IPPBAR .EQ. 0) THEN
      STR(1)=fg1*fd2          ! b  d   
      STR(2)=fg1*fs2          ! b  s  
      STR(3)=fg1*fus2         ! b  u~ 
      STR(4)=fg1*fc2          ! b  c~ 
      STR(5)=fg1*fds2         ! b~ d~ 
      STR(6)=fg1*fs2          ! b~ s~ 
      STR(7)=fg1*fu2          ! b~ u
      STR(8)=fg1*fc2          ! b~ c
      ELSE
      STR(1)=fg1*fds2         ! b  d
      STR(2)=fg1*fs2          ! b  c  
      STR(3)=fg1*fu2          ! b  u~
      STR(4)=fg1*fc2          ! b  c~ 
      STR(5)=fg1*fd2          ! b~ u~ 
      STR(6)=fg1*fs2          ! b~ s~ 
      STR(7)=fg1*fus2         ! b~ u
      STR(8)=fg1*fc2          ! b~ c
      ENDIF

!     q g

      STR(9)=fd1*fg2           ! d  b 
      STR(10)=fs1*fg2          ! s  b
      STR(11)=fus1*fg2         ! u~ b
      STR(12)=fc1*fg2          ! c~ b
      STR(13)=fds1*fg2         ! d~ b~
      STR(14)=fs1*fg2          ! s~ b~
      STR(15)=fu1*fg2          ! u  b~
      STR(16)=fc1*fg2          ! c  b~

!     Select helicity amplitude and process

      IAMP=IPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 500) ntry(IAMP) = 500

c      DO k=1,NPROC
c      IQ=(1+MOD((k-1)/2,2))*(-1)**(MOD((k-1)/2,2)+MOD((k-1)/4,2))
c      PRINT *,k,IQ,MOD((k-1)/4,2)
c      ENDDO
c      STOP

      IQ=(1+MOD((IPROC-1)/2,2))
     &  *(-1)**(MOD((IPROC-1)/2,2)+MOD((IPROC-1)/4,2))

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (MOD((IPROC-1)/4,2) .EQ. 1) GOTO 14

!     b -> Y 

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=Ybj(nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

14    CONTINUE

!     bbar -> Ybar

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=Ybarbj(nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END

