      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MQ1  MQ2  MB1  MB2  -------------------  MQQ
      DATA varmin  /250.,250.,  0.,  0.,  0.,  0.,  0.,  0.,   0.,-1.,
     &  -1./
      DATA varmax  /750.,750.,200.,200.,200.,750.,200.,750.,4000., 1.,
     &   1./
      DATA nbintot /100,  100, 100, 100, 100, 150, 100, 150,  200, 40,
     &   40/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO
      
      
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      LOGICAL GOODASYM(n_a)
      REAL*8 beta,Ap,Am,FL,F0,FR

      DATA asym_cent /0.,-0.5874,0.5874,0.,-0.5874,0.5874,
     &  0.,-0.5874,0.5874,0.,-0.5874,0.5874/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      beta=2**(1d0/3d0)-1d0

      PRINT 1150,asym(1),err_asym(1)
      PRINT 1151,asym(2),err_asym(2)
      PRINT 1152,asym(3),err_asym(3)
      Ap=asym(2)
      Am=asym(3)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1200,FL,F0,FR

      PRINT 1160,asym(4),err_asym(4)
      PRINT 1161,asym(5),err_asym(5)
      PRINT 1162,asym(6),err_asym(6)
      Ap=asym(5)
      Am=asym(6)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1200,FL,F0,FR
      PRINT 999
      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB t  ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('A+  t  ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('A-  t  ',F8.5,' +- ',F8.5,' (MC)')
1160  FORMAT ('AFB tb ',F8.5,' +- ',F8.5,' (MC)')
1161  FORMAT ('A+  tb ',F8.5,' +- ',F8.5,' (MC)')
1162  FORMAT ('A-  tb ',F8.5,' +- ',F8.5,' (MC)')
1200  FORMAT ('--->  FL ~ ',F8.5,'   F0 ~ ',F8.5,'   FR ~ ',F8.5)
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 DOT,COSVEC

!     External momenta

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 PQ1(0:3),PQ2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT/ PQ1,PQ2,PB1,PB2
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2

      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     Local kinematical quantities

      REAL*8 Pall(0:3),Pboo(0:3)
      REAL*8 cos_lW1,cos_lW2

!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3),pdum2(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

      CALL SUMVEC(Pf1,Pfb1,Pdum1)
      var(3)=SQRT(DOT(Pdum1,Pdum1))
      CALL SUMVEC(Pdum1,Pb,Pdum2)
      var(1)=SQRT(DOT(Pdum2,Pdum2))
      CALL SUMVEC(Pf2,Pfb2,Pdum1)
      var(4)=SQRT(DOT(Pdum1,Pdum1))
      CALL SUMVEC(Pdum1,Pbb,Pdum2)
      var(2)=SQRT(DOT(Pdum2,Pdum2))

      CALL SUMVEC(PQ1,PQ2,Pall)
      var(9)=SQRT(DOT(Pall,Pall))

!     lW distribution for Q

      Pboo(0)=PB1(0)                                ! LAB to W- rest frame
      DO i=1,3
        Pboo(i)=-PB1(i)
      ENDDO
      CALL BOOST(Pboo,Pf1,pdum1)
      CALL BOOST(Pboo,Pb,pdum2)
      cos_lW1=-COSVEC(pdum1,pdum2)

!     lW distribution for Qbar

      Pboo(0)=PB2(0)                                ! LAB to W+ rest frame
      DO i=1,3
        Pboo(i)=-PB2(i)
      ENDDO
      CALL BOOST(Pboo,Pfb2,pdum1)
      CALL BOOST(Pboo,Pbb,pdum2)
      cos_lW2=-COSVEC(pdum1,pdum2)

      var(10)=cos_lW1
      var(11)=cos_lW2

      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      quant(1)=cos_lW1                 ! A_FB
      quant(2)=cos_lW1                 ! A+
      quant(3)=cos_lW1                 ! A-
      quant(4)=cos_lW2                 ! A_FB
      quant(5)=cos_lW2                 ! A+
      quant(6)=cos_lW2                 ! A-

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







