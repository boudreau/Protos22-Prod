      PROGRAM Protos
!
!     PROgram for TOp Simulations
!
!     By J. A. Aguilar-Saavedra, vintage May 2009
!
!     Includes adapted VEGAS for multigrid integration
!     Uses HELAS for matrix element evaluation
!
      IMPLICIT NONE
      
!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions used

      REAL*8 FXN
      INTEGER LONGITUD
      EXTERNAL FXN,LONGITUD

!     Flags

      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO

!     Input heavy quark parameters

      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix

!     Other input data

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     For VEGAS

      REAL*8 XL(MAXDIM),XU(MAXDIM),ACC,AVG1,SD1,CHI2A1
      INTEGER NCALL,ITMX,NPRN,ND
      COMMON/BVEG1/XL,XU,ACC,ND,NCALL,ITMX,NPRN

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC
      REAL*8 GRWGT(MAXGR)
      COMMON /multigr2/ GRWGT
      REAL*8 GRAPROB(0:MAXGR)
      COMMON /multigr3/ GRAPROB

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     For ran2 calls

      INTEGER idum0
      COMMON /seed_ini/ idum0

!     For parameter scan

      INTEGER ISCAN
      REAL*8 SCANSTEP

!     Local

      INTEGER i
      INTEGER ILOADGRID,ISAVEGRID,NCALL1,NCALL2,ITMX1,ITMX2
      INTEGER IEVTOUT,IPLOT,ILOG,IPRSUB,IPRAS,IVERB
      REAL*8 GRPROB(NPROC)

!     --------------------------------------------

      OPEN (32,FILE='input/run.dat',status='old')
      READ (32,*) PROCNAME
      l=LONGITUD(PROCNAME)
      READ (32,*) ET
      READ (32,*) IPPBAR
      READ (32,*) NRUNS
      READ (32,*) ILOADGRID,ISAVEGRID
      READ (32,*) NCALL1,ITMX1
      READ (32,*) NCALL2,ITMX2
      READ (32,*) idum0
      READ (32,*) ICUT
      READ (32,*) IEVTOUT,IPLOT,ILOG
      READ (32,*) IVERB,IPRSUB,IPRAS
      READ (32,*) mQ,Vmix
      READ (32,*) ISCAN,SCANSTEP
      READ (32,*) IPDSET
      READ (32,*) QFAC
      CLOSE (32)

!     Check parameter consistency

!     Override settings for special cases

      IF (NRUNS .GT. 1) THEN
        ISAVEGRID=0
      ENDIF

      IF ((IEVTOUT .NE. 0) .AND. (NRUNS .GT. 99)) THEN
        PRINT 986
        STOP
      ENDIF

!
!     BEGIN
!

      ND=NDIM
      CALL INITPAR

      IF (ILOG .NE. 0) CALL LOGINI(-1,0)

      DO IRUN=1,NRUNS                     !   Different runs

      IF (NRUNS .GT. 1) THEN
        PRINT 1110,IRUN
        PRINT 999
      ENDIF

      CALL REINITPAR
      CALL RAN2RESET(idum0)

      IF (IEVTOUT .EQ. 1) CALL EVTOUT(-1)
      CALL PLOTINI(-1)
      CALL ASINI(-1)

!     First integration: warmup

      DO i=1,ND
        XL(i)=0d0
        XU(i)=1d0
      ENDDO
      ACC=-1d0

      IF (ILOADGRID .EQ. 1) GOTO 20

      DO i=1,MAXGR
        SIG(i)=0d0
        SIG2(i)=0d0
      ENDDO

      IRECORD=0
      IWRITE=0
      IHISTO=0
      NCALL=NCALL1
      ITMX=ITMX1
      NPRN=0
      WT_ITMX=1d0/FLOAT(ITMX)

      INITGRID=1
      DO IPROC=1,NPROC
      PRINT 1100,IPROC
        CALL VEGAS(FXN,AVG1,SD1,CHI2A1)
        SIG(IPROC)=AVG1
      ENDDO

      SIG_tot=0d0
      DO IPROC=1,NPROC
        SIG_tot=SIG_tot+SIG(IPROC)
      ENDDO
      

      GRAPROB(0)=0d0
      DO i=1,MAXGR
        GRPROB(i)=SIG(i)/SIG_tot
        GRAPROB(i)=GRAPROB(i-1)+GRPROB(i)
        IF (GRPROB(i) .GT. 0d0) THEN
          GRWGT(i)=1d0/GRPROB(i)
        ELSE
          GRWGT(i)=0d0
        ENDIF
      ENDDO
      IF (ABS(GRAPROB(NPROC)-1d0) .GT. 1d-8) THEN
        PRINT 999
        PRINT 987,1d0-graprob(nproc)
      ENDIF
      GRAPROB(NPROC)=1d0


      IF (ISAVEGRID .EQ. 1) THEN
        OPEN (33,file='input/grid.ini',status='unknown')
        CALL SAVE(ND)
        CLOSE (33)
        OPEN (33,file='input/proc.ini',status='unknown')
        WRITE (33,2010) GRAPROB
        WRITE (33,2011) GRWGT
        CLOSE (33)
      ENDIF
      
20    CONTINUE

!     Second integration run

      DO i=1,MAXGR
        SIG(i)=0d0
        SIG2(i)=0d0
	ERR(i)=0d0
      ENDDO
      NIN=0
      NOUT=0

      IF (ILOADGRID .EQ. 1) THEN
        OPEN (33,file='input/grid.ini',status='old')
        CALL RESTR(ND)
        CLOSE (33)
        OPEN (33,file='input/proc.ini',status='old')
        READ (33,2010) GRAPROB
        READ (33,2011) GRWGT
        CLOSE (33)
      ENDIF

      IRECORD=1
      IF (IEVTOUT .EQ. 1) IWRITE=1
      IF ((IPLOT .NE. 0) .OR. (ILOG .NE. 0) .OR. (IPRAS .NE. 0))
     &   IHISTO=1
      NCALL=NCALL2
      ITMX=ITMX2
      NPRN=IVERB
      WT_ITMX=1d0/FLOAT(ITMX)
      INITGRID=0
      CALL VEGAS1(FXN,AVG1,SD1,CHI2A1)

      SIG_tot=0d0
      ERR_tot=0d0
      DO i=1,9
      SIG_tot=SIG_tot+SIG(i)
      ERR_tot=ERR_tot+ERR(i)
      ENDDO
      PRINT 990

      IF ((IPRAS .NE. 0) .OR. (ILOG .NE. 0)) CALL ASINI(1)
      IF (IPRAS .NE. 0) CALL ASINI(2)
      IF (ILOG .NE. 0) CALL LOGINI(0,ISCAN)
      IF (IPLOT .EQ. 1) CALL PLOTINI(1)
      IF (IEVTOUT .EQ. 1) CALL EVTOUT(1)

      IF (IPRSUB .EQ. 1) THEN
        PRINT 1140,SIG(1),ERR(1)
        PRINT 1141,SIG(2)+SIG(6),ERR(2)+ERR(6)
        PRINT 1142,SIG(3)+SIG(7),ERR(3)+ERR(7)
        PRINT 1143,SIG(4)+SIG(8),ERR(4)+ERR(8)
        PRINT 1144,SIG(5)+SIG(9),ERR(5)+ERR(9)
        PRINT 1146,SIG_tot-SIG(1),ERR_tot-ERR(1)
        PRINT 999
      ENDIF
      PRINT 1130,SIG_tot,ERR_tot
      PRINT 990
      
      IF (ISCAN .NE. 0) CALL CHGPAR(ISCAN,SCANSTEP)

      ENDDO                                             ! End of runs

      IF (ILOG .NE. 0) CALL LOGINI(1,0)

      STOP

985   FORMAT ('ERROR: incorrect ID')
986   FORMAT ('ERROR: NRUNS out of range')
990   FORMAT ('===================================================')
995   FORMAT ('---------------------------------------------------')
999   FORMAT ('')
1100  FORMAT ('Initialising grid ',I2,'...')
1110  FORMAT ('Run: ',I2)
1130  FORMAT ('Total xsec = ',D9.4,' +- ',D9.4,' fb')

1140  FORMAT ('c.s. gg~ = ',D9.4,' +- ',D9.4)
1141  FORMAT ('c.s. uu~ = ',D9.4,' +- ',D9.4)
1142  FORMAT ('c.s. dd~ = ',D9.4,' +- ',D9.4)
1143  FORMAT ('c.s. ss~ = ',D9.4,' +- ',D9.4)
1144  FORMAT ('c.s. cc~ = ',D9.4,' +- ',D9.4)
1146  FORMAT ('c.s. qq~ = ',D9.4,' +- ',D9.4)
987   FORMAT ('Warning: in initialisation acc sum = ',D9.4)
2010  FORMAT (F8.6)
2011  FORMAT (D12.6)

      END
