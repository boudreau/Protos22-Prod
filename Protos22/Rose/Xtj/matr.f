      REAL*8 FUNCTION Xtj(NHEL)

!     FOR PROCESS : g u  ->  X t~ d   /   g d~  ->  X t~ u~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=11)

!     ARGUMENTS

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pf3(0:3),Pfb3(0:3),Pb(0:3),
     &  Pf4(0:3),Pfb4(0:3),PbX(0:3),Pj(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pf3,Pfb3,Pb,Pf4,Pfb4,PbX,Pj

!     LOCAL VARIABLES

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf3(6),Wfb3(6),Wf4(6),Wfb4(6)
      COMPLEX*16 Wb(6),WW1(6),WB1(6),Wt(6)
      COMPLEX*16 WbX(6),WW2(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IQ
      COMMON /Qflags/ IQ
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWtQ(2),Xmix

!     Specific couplings


!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL OXXXXX(Pb,mb,NHEL(3),1,Wb)               ! b
      CALL IXXXXX(PbX,mb,NHEL(4),-1,WbX)            ! bbar

      IF (IQ .EQ. 2) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! u
        CALL OXXXXX(Pj,0d0,NHEL(5),1,W5)            ! d
      ELSE IF (IQ .EQ. -1) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! dbar
        CALL IXXXXX(Pj,0d0,NHEL(5),-1,W5)           ! ubar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Xtj'
        STOP
      ENDIF

!     Heavy quark

      CALL OXXXXX(Pf1,0d0,NHEL(6),1,Wf1)            ! f1     - W+ from Q decay
      CALL IXXXXX(Pfb1,0d0,NHEL(7),-1,Wfb1)         ! f1bar
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)           ! W+ from Q

      CALL OXXXXX(Pf3,0d0,NHEL(8),1,Wf3)            ! f3     - W+ from t decay
      CALL IXXXXX(Pfb3,0d0,NHEL(9),-1,Wfb3)         ! f3bar
      CALL JIOXXX(Wfb3,Wf3,GWF,MW,GW,WW1)           ! W+ from t

      CALL FVOXXX(Wb,WW1,GWF,mt,Gt,Wt)              ! t
      CALL FVOXXX(Wt,WB1,GWtQ,mQ,GQ,W3)             ! Q

!     tbar

      CALL OXXXXX(Pf4,0d0,NHEL(10),1,Wf4)           ! f4     - W- from tbar
      CALL IXXXXX(Pfb4,0d0,NHEL(11),-1,Wfb4)        ! f4bar
      CALL JIOXXX(Wfb4,Wf4,GWF,MW,GW,WW2)           ! W-

      CALL FVIXXX(WbX,WW2,GWF,mt,Gt,W4)             ! tbar

!     Diagrams

      IF (IQ .EQ. 2) THEN
        CALL JIOXXX(W2,W5,GWF,MW,GW,W7)
      ELSE
        CALL JIOXXX(W5,W2,GWF,MW,GW,W7)
      ENDIF

      CALL FVIXXX(W4,W1,GG,mt,Gt,W6)
      CALL IOVXXX(W6,W3,W7,GWtQ,AMP(1))
 
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W4,W8,W7,GWtQ,AMP(2))

      Xtj = 0d0
      DO I = 1, NEIGEN
        ZTEMP = (0d0,0d0)
        DO J = 1, NGRAPHS
          ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
        ENDDO
        Xtj = Xtj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


      REAL*8 FUNCTION Xbartj(NHEL)

!     FOR PROCESS : g u  ->  X t~ d   /   g d~  ->  X t~ u~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=11)

!     ARGUMENTS

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pf3(0:3),Pfb3(0:3),Pb(0:3),
     &  Pf4(0:3),Pfb4(0:3),PbX(0:3),Pj(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pf3,Pfb3,Pb,Pf4,Pfb4,PbX,Pj

!     LOCAL VARIABLES

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf3(6),Wfb3(6),Wf4(6),Wfb4(6)
      COMPLEX*16 Wb(6),WW1(6),WB1(6),Wt(6)
      COMPLEX*16 WbX(6),WW2(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IQ
      COMMON /Qflags/ IQ
      REAL*8 x1,x2,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,Q,IDIR

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWtQ(2),Xmix

!     Specific couplings


!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL IXXXXX(Pb,mb,NHEL(3),-1,Wb)              ! bbar
      CALL OXXXXX(PbX,mb,NHEL(4),1,WbX)             ! b

      IF (IQ .EQ. 1) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! d
        CALL OXXXXX(Pj,0d0,NHEL(5),1,W5)            ! u
      ELSE IF (IQ .EQ. -2) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! ubar
        CALL IXXXXX(Pj,0d0,NHEL(5),-1,W5)           ! dbar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Xbartj'
        STOP
      ENDIF

!     Heavy quark

      CALL OXXXXX(Pf1,0d0,NHEL(6),1,Wf1)            ! f1     - W- from Qbar decay
      CALL IXXXXX(Pfb1,0d0,NHEL(7),-1,Wfb1)         ! f1bar
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)           ! W- from Qbar

      CALL OXXXXX(Pf3,0d0,NHEL(8),1,Wf3)            ! f3     - W- from t decay
      CALL IXXXXX(Pfb3,0d0,NHEL(9),-1,Wfb3)         ! f3bar
      CALL JIOXXX(Wfb3,Wf3,GWF,MW,GW,WW1)           ! W- from tbar

      CALL FVIXXX(Wb,WW1,GWF,mt,Gt,Wt)              ! t
      CALL FVIXXX(Wt,WB1,GWtQ,mQ,GQ,W3)             ! Q

!     t

      CALL OXXXXX(Pf4,0d0,NHEL(10),1,Wf4)           ! f4     - W+ from t
      CALL IXXXXX(Pfb4,0d0,NHEL(11),-1,Wfb4)        ! f4bar
      CALL JIOXXX(Wfb4,Wf4,GWF,MW,GW,WW2)           ! W+

      CALL FVOXXX(WbX,WW2,GWF,mt,Gt,W4)             ! t

!     Diagrams

      IF (IQ .EQ. 1) THEN
        CALL JIOXXX(W2,W5,GWF,MW,GW,W7)
      ELSE
        CALL JIOXXX(W5,W2,GWF,MW,GW,W7)
      ENDIF

      CALL FVOXXX(W4,W1,GG,mt,Gt,W6)
      CALL IOVXXX(W3,W6,W7,GWtQ,AMP(1))

      CALL FVIXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W8,W4,W7,GWtQ,AMP(2))

      Xbartj = 0d0
      DO I = 1, NEIGEN
        ZTEMP = (0d0,0d0)
        DO J = 1, NGRAPHS
          ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
        ENDDO
        Xbartj = Xbartj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



