      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Input parameters 

      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1
      INTEGER IDW1,IDZ1,IDH1
      COMMON /Qfstate/ IDW1,IDZ1,IDH1
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      REAL*8 WTT1(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTZ,BRT1ac,BRZac
      INTEGER IZL1
      COMMON /Qtweak/ IZL1

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGQ,NGt,NGW,NGZ
      COMMON /BWdata/ NGQ,NGt,NGW,NGZ

      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,GOODHEL,ncomb8,ncomb6,ntry

!     Variables for weighting of final states

      REAL*8 PW_T1
      REAL*8 PRT1(3),PRZ(4)

!     Local variables

      INTEGER i,j
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 GQ_Wb,GQ_Zt,GQ_Ht
      REAL*8 xW,xb,EW,qW
      REAL*8 r_W,r_Z,r_H,r_t,E_t,p_t,Xmix
      REAL*8 gevpb
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999
!
!     tweaks
!
      OPEN (31,FILE='input/tweak.dat',status='old')
      READ (31,*) IZL1
      CLOSE (31)

      IF ((IZL1 .NE. 0) .AND. (IZL1 .NE. 1)) IZL1=0

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb*1000d0                   ! Cross section in fb
      ENDDO

      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!

      CALL SETALPHA(mQ)
      CALL SETALPHAS(mQ)

!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)

!     Z boson

      cw=SQRT(1d0-sw2)
      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)

!     Higgs  -- not used

      GH=1d0

!     Heavy Q

      r_Z = (MZ/mQ)**2
      r_W = (MW/mQ)**2
      r_H = (MH/mQ)**2
      r_t = (mt/mQ)**2
      Xmix=Vmix

      xW=MW/mQ
      xb=mb/mQ
      EW=(mQ**2+MW**2-mb**2)/(2d0*mQ)
      qW=SQRT(EW**2-MW**2)
      GQ_Wb=g**2/(32d0*Pi)*qW*(mQ/MW)**2*Vmix**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)
      IF ((IMOD .EQ. 3) .OR. (IMOD .EQ. 4)) GQ_Wb=0d0

      E_t=(mQ**2+mt**2-MZ**2)/(2d0*mQ)
      p_t=SQRT(E_t**2-mt**2)
      GQ_Zt=g**2/(64d0*Pi*cW**2) * Xmix**2 * p_t * (mQ**2/MZ**2) *
     &      (1d0+r_Z-2d0*r_Z**2-2d0*r_t+r_t**2+r_Z*r_t)
      IF (MZ+mt .GT. mQ) GQ_Zt=0d0

      E_t=(mQ**2+mt**2-MH**2)/(2d0*mQ)
      p_t=SQRT(E_t**2-mt**2)
      GQ_Ht=g**2/(64d0*Pi) * Xmix**2 * p_t * (mQ**2/MW**2) *
     &      (1d0 + 6d0*r_t - r_H + r_t**2 - r_t*r_H)
      IF (MH+mt .GT. mQ) GQ_Ht=0d0
      
      GQ=GQ_Wb+GQ_Zt+GQ_Ht

      IF (IMOD .EQ. 1) PRINT 1111
      IF (IMOD .EQ. 2) PRINT 1112
      IF (IMOD .EQ. 3) PRINT 1113
      IF (IMOD .EQ. 4) PRINT 1114
      PRINT 1100,mQ,GQ
      PRINT 1203,GQ_Wb/GQ,GQ_Zt/GQ,GQ_Ht/GQ
      PRINT 999
      PRINT 1201,GZ_nu/GZ,GZ_emt/GZ
      PRINT 1202,GZ_uc/GZ,GZ_dsb/GZ
      PRINT 999
      IF (IMATCH .EQ. 1) THEN
        PRINT 981
        PRINT 999
      ENDIF
      IF (IMATCH .EQ. 2) THEN
        PRINT 982,PTbmin
        PRINT 999
      ENDIF

!     Probabilities for each decay channel

      PW_T1=GQ_Wb*DFLOAT(IDW1)+GQ_Zt*DFLOAT(IDZ1)+GQ_Ht*DFLOAT(IDH1)

      IF (PW_T1 .EQ. 0d0) THEN
        PRINT 985
        PRINT 999
        STOP
      ENDIF
      
      PRT1(1)=GQ_Wb*DFLOAT(IDW1)/PW_T1
      PRT1(2)=GQ_Zt*DFLOAT(IDZ1)/PW_T1
      PRT1(3)=GQ_Ht*DFLOAT(IDH1)/PW_T1
      
      BRT1ac(0)=0d0
      DO i=1,3
        BRT1ac(i)=BRT1ac(i-1)+PRT1(i)
        WTT1(i)=1d0/PRT1(i)
        IF (PRT1(i) .EQ. 0d0) WTT1(i)=0d0
      ENDDO
      IF (ABS(BRT1ac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRT1ac(3)
      BRT1ac(3)=1d0

      PRZ(1)=GZ_nu/GZ
      PRZ(2)=GZ_emt/GZ
      PRZ(3)=GZ_uc/GZ
      PRZ(4)=GZ_dsb/GZ

      BRZac(0)=0d0
      DO i=1,4
        BRZac(i)=BRZac(i-1)+PRZ(i)
        WTZ(i)=1d0/PRZ(i)
      ENDDO
      IF (ABS(BRZac(4)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRZac(4)
      BRZac(4)=1d0

!     Include here colour factors and sum over fermions for Z, H

      WTZ(1)=WTZ(1)*3d0
      WTZ(2)=WTZ(2)*3d0
      WTZ(3)=WTZ(3)*6d0
      WTZ(4)=WTZ(4)*9d0

      NGW=MW/GW*0.95d0
      NGZ=MZ/GZ*0.95d0
      NGt=0.95*mt/Gt
      NGQ=0.95*mQ/GQ

!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO

      CALL SETPOL(istep,nhel8,ncomb8,8)
      DO i=1,MAXAMP
        DO j=1,ncomb8
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

      CALL SETPOL(istep,nhel6,ncomb6,6)

c      DO j=1,ncomb8
c      print *,(nhel8(i,j),i=1,8)
c      ENDDO
c      print *,ncomb8
c      DO j=1,ncomb6
c      print *,(nhel6(i,j),i=1,8)
c      ENDDO
c      print *,ncomb6
c      STOP

      RETURN

500   FORMAT ('______          _            ')
501   FORMAT ('| ___ \\        | |           ')           
502   FORMAT ('| |_/ / __ ___ | |_ ___  ___ ')
503   FORMAT ('|  __/ ''__/ _ \\| __/ _ \\/ __|')
504   FORMAT ('| |  | | | (_) | || (_) \\__ \\')
505   FORMAT ('\\_|  |_|  \\___/ \\__\\___/|___/')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
981   FORMAT ('Matching: subtraction ')
982   FORMAT ('Matching at PTb = ',F5.1)
985   FORMAT ('Please open some decay channel for heavy quarks!')
999   FORMAT ('')
1000  FORMAT ('---------------------------------------------------')
1100  FORMAT ('mT = ',F6.1,'   GT = ',F9.6)
1111  FORMAT ('Model: T_{L,R} singlet')
1112  FORMAT ('Model: (T B)_{L,R} doublet equal mixing')
1113  FORMAT ('Model: (T B)_{L,R} doublet up mixing')
1114  FORMAT ('Model: (X T)_{L,R} doublet')
1201  FORMAT ('BR(Z -> vv) = ',F5.3,'  BR(Z -> ll) = ',F5.3)
1202  FORMAT ('BR(Z -> uu) = ',F5.3,'  BR(Z -> dd) = ',F5.3)
1203  FORMAT ('BR(T -> Wb) = ',F5.3,
     &  '  BR(T -> Zt) = ',F5.3,'  BR(T -> Ht) = ',F5.3)
1303  FORMAT ('Warning: in initialisation acc sum = ',D9.4)
      END



      SUBROUTINE SETPOL(nspin,hel,nhel,num)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel,num

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,num
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,num
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=num
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,num
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END


