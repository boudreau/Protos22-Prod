      SUBROUTINE GET_BPDF(x,fb)
      IMPLICIT NONE

!     Arguments

      REAL*8 x,fb

!     PDF tables

      REAL*8 xtab(105),fbtab(105)
      COMMON /bpdfdat/ xtab,fbtab

!     Local

      INTEGER i
      REAL*8 logx1,logx2,logy1,logy2

      IF (x .LT. 1d-7) THEN
        fb=0d0
        PRINT *,'x too small'
        RETURN
      ENDIF
      
      i=1
      DO WHILE (x .GT. xtab(i))
        i=i+1
      ENDDO
      IF ((fbtab(i-1) .EQ. 0) .OR. (fbtab(i) .EQ. 0)) THEN
      fb=0d0
      RETURN
      ENDIF
      logy1=LOG(fbtab(i-1))
      logy2=LOG(fbtab(i))
      logx1=LOG(xtab(i-1))
      logx2=LOG(xtab(i))
      fb=EXP(logy1+(logy2-logy1)/(logx2-logx1)*(LOG(x)-logx1))
      RETURN
      END




      SUBROUTINE INIT_BPDF(Q)
      IMPLICIT NONE

!     Arguments

      REAL*8 Q

!     PDF tables

      REAL*8 xtab(105),fbtab(105)
      COMMON /bpdfdat/ xtab,fbtab

!     Local

      INTEGER i

      DATA xtab /1d-7,1.2589d-7,1.5849d-7,1.9952d-7,2.5119d-7,
     &  3.1623d-7,3.9811d-7,5.0119d-7,6.3096d-7,7.9533d-7,
     &  1d-6,1.28121d-6,1.64152d-6,2.10317d-6,2.69463d-6,
     &  3.45242d-6,4.42329d-6,5.66715d-6,7.26076d-6,9.30241d-6,
     &  1.19180d-5,1.52689d-5,1.95617d-5,2.50609d-5,3.21053d-5,
     &  4.11287d-5,5.26863d-5,6.74889d-5,8.64459d-5,1.10720d-4,
     &  1.41800d-4,1.81585d-4,2.32503d-4,2.97652d-4,3.80981d-4,
     &  4.87518d-4,6.26039d-4,8.00452d-4,1.02297d-3,1.30657d-3,
     &  1.66759d-3,2.12729d-3,2.71054d-3,3.44865d-3,4.37927d-3,
     &  5.54908d-3,7.01192d-3,8.83064d-3,1.10763d-2,1.38266d-2,
     &  1.71641d-2,2.11717d-2,2.59364d-2,3.15062d-2,3.79623d-2,
     &  4.53425d-2,5.36750d-2,6.29705d-2,7.32221d-2,8.44039d-2,
     &  9.64793d-2,1.09332d-1,1.23067d-1,1.37507d-1,1.52639d-1,
     &  1.68416d-1,1.84794d-1,2.01731d-1,2.19016d-1,2.36948d-1,
     &  2.55242d-1,2.73927d-1,2.92954d-1,3.12340d-1,3.32036d-1,
     &  3.52019d-1,3.72282d-1,3.92772d-1,4.13533d-1,4.34326d-1,
     &  4.55495d-1,4.76836d-1,4.98342d-1,5.20006d-1,5.41818d-1,
     &  5.63773d-1,5.85861d-1,6.08077d-1,6.30459d-1,6.52800d-1,
     &  6.75387d-1,6.98063d-1,7.20830d-1,7.43683d-1,7.66623d-1,
     &  7.89636d-1,8.12791d-1,8.35940d-1,8.59175d-1,8.82485d-1,
     &  9.05866d-1,9.29311d-1,9.52817d-1,9.76387d-1,1d0/

      DO 1 i=1,105
      CALL CALC_BPDF(xtab(i),Q,fbtab(i))
c      print *,xtab(i),fbtab(i)
1     CONTINUE
c      stop
      RETURN
      END






      SUBROUTINE CALC_BPDF(x,Q,fb)
      IMPLICIT NONE

!     Arguments

      REAL*8 x,Q,fb

!     Functions

      REAL*8 F
      EXTERNAL F

!     Data passed to F

      REAL*8 x0,Q0
      COMMON /Fdata/ x0,Q0

!     Local

      x0=x
      Q0=Q
      
      CALL QSIMP(F,x,1d0,fb)

      RETURN
      END




      REAL*8 FUNCTION F(z)
      IMPLICIT NONE

!     Arguments

      REAL*8 z

!     Parameters

      REAL*8 pi
      PARAMETER (pi=3.1415926535)

!     Inputs

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs

!     Data for integration

      REAL*8 x,Q
      COMMON /Fdata/ x,Q

!     Local

      REAL*8 fu,fd,fus,fds,fs,fc,fb,fg
      INTEGER ISTAT

      F=(z**2+(1d0-z)**2)/(2d0*z)
      CALL GETPDF(x/z,Q,fu,fd,fus,fds,fs,fc,fb,fg,ISTAT)
c      print *,x/z,fg
      F=F*fg*alpha_s/(2d0*pi)*LOG(Q**2/mb**2)
      RETURN
      END


