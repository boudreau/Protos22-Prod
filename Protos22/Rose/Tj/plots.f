      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MQ1  MB1  MW1  MW1b  angles
      DATA varmin  /250.,  0.,  0.,  0.,-1.,-1.,-1.,-1.,-1.,-1./
      DATA varmax  /750.,200.,200.,750., 1., 1., 1., 1., 1., 1./
      DATA nbintot / 100, 100, 100, 150, 40, 40, 40, 40, 40, 40/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO
      
      
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local to save

      LOGICAL GOODASYM(n_a)
      SAVE GOODASYM

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      REAL*8 rbl,rnl,rnb,drbl,drnl,drnb
      REAL*8 beta,Ap,Am,FL,F0,FR

      DATA asym_cent /0.,-0.5874,0.5874,0.,-0.5874,0.5874,0.,0.,0.,0.,
     &  0.,0./

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      beta=2**(1d0/3d0)-1d0

      IF (GOODASYM(1)) THEN
      PRINT 1150,asym(1),err_asym(1)
      PRINT 1151,asym(2),err_asym(2)
      PRINT 1152,asym(3),err_asym(3)
      Ap=asym(2)
      Am=asym(3)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1180,FL,F0,FR
      PRINT 999
      ENDIF

      IF (GOODASYM(4)) THEN
      PRINT 1170,asym(4),err_asym(4)
      PRINT 1171,asym(5),err_asym(5)
      PRINT 1172,asym(6),err_asym(6)
      Ap=asym(5)
      Am=asym(6)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1180,FL,F0,FR
      PRINT 999
      ENDIF

      IF (GOODASYM(7)) THEN
      PRINT 1153,asym(7),err_asym(7)
      PRINT 1154,asym(8),err_asym(8)
      PRINT 1155,asym(9),err_asym(9)
      rbl=asym(9)/asym(7)
      drbl=rbl*SQRT((err_asym(9)/asym(9))**2+(err_asym(7)/asym(7))**2)
      rnl=asym(8)/asym(7)
      drnl=rnl*SQRT((err_asym(8)/asym(8))**2+(err_asym(7)/asym(7))**2)
      rnb=asym(8)/asym(9)
      drnb=rnb*SQRT((err_asym(8)/asym(8))**2+(err_asym(9)/asym(9))**2)
      PRINT 1160,rbl,drbl
      PRINT 1161,rnl,drnl
      PRINT 1162,rnb,drnb
      PRINT 999
      ENDIF

      IF (GOODASYM(10)) THEN
      PRINT 1173,asym(10),err_asym(10)
      PRINT 1174,asym(11),err_asym(11)
      PRINT 1175,asym(12),err_asym(12)
      rbl=asym(12)/asym(10)
      drbl=rbl*SQRT((err_asym(12)/asym(12))**2
     &  +(err_asym(10)/asym(10))**2)
      rnl=asym(11)/asym(10)
      drnl=rnl*SQRT((err_asym(11)/asym(11))**2
     &  +(err_asym(10)/asym(10))**2)
      rnb=asym(11)/asym(12)
      drnb=rnb*SQRT((err_asym(11)/asym(11))**2
     &  +(err_asym(12)/asym(12))**2)
      PRINT 1160,rbl,drbl
      PRINT 1161,rnl,drnl
      PRINT 1162,rnb,drnb
      PRINT 999
      ENDIF


      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB t  ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('A+  t  ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('A-  t  ',F8.5,' +- ',F8.5,' (MC)')
1170  FORMAT ('AFB T  ',F8.5,' +- ',F8.5,' (MC)')
1171  FORMAT ('A+  T  ',F8.5,' +- ',F8.5,' (MC)')
1172  FORMAT ('A-  T  ',F8.5,' +- ',F8.5,' (MC)')
1153  FORMAT ('Al  t  ',F8.5,' +- ',F8.5,' (MC)')
1154  FORMAT ('An  t  ',F8.5,' +- ',F8.5,' (MC)')
1155  FORMAT ('Ab  t  ',F8.5,' +- ',F8.5,' (MC)')
1173  FORMAT ('Al  T  ',F8.5,' +- ',F8.5,' (MC)')
1174  FORMAT ('An  T  ',F8.5,' +- ',F8.5,' (MC)')
1175  FORMAT ('Ab  T  ',F8.5,' +- ',F8.5,' (MC)')
1160  FORMAT ('rbl ',F8.5,' +- ',F8.5,' (MC)')
1161  FORMAT ('rnl ',F8.5,' +- ',F8.5,' (MC)')
1162  FORMAT ('rnb ',F8.5,' +- ',F8.5,' (MC)')
1180  FORMAT ('--->  FL ~ ',F8.5,'   F0 ~ ',F8.5,'   FR ~ ',F8.5)
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 COSVEC,DOT

!     External momenta

      INTEGER IMOD,IQ,IB1,IWF1,IZF1
      COMMON /Qflags/ IMOD,IQ,IB1,IWF1,IZF1
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,Pf2,Pfb2
      REAL*8 PQ1(0:3),PB1(0:3),Pt1(0:3),PW1(0:3)
      COMMON /MOMINT/ PQ1,PB1,Pt1,PW1
      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     Local kinematical quantities

      REAL*8 PWb(0:3),Pl(0:3),Pn(0:3),PjR(0:3)
      REAL*8 cos_lW,cos_lj,cos_nj,cos_bj
      
!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3),pdum2(0:3)
      REAL*8 Pboo(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

      var(1)=SQRT(DOT(PQ1,PQ1))
      var(2)=SQRT(DOT(PB1,PB1))
      CALL SUMVEC(Pf1,Pfb1,Pdum1)
      var(3)=SQRT(DOT(Pdum1,Pdum1))
      CALL SUMVEC(Pdum1,Pb,PWb)
      var(4)=SQRT(DOT(PWb,PWb))

!     Select charged lepton

      IF ( ((IPROC .GE. 1) .AND. (IPROC .LE. 4))
     &   .OR. ((IPROC .GE. 9) .AND. (IPROC .LE. 12)) ) THEN
        CALL STORE(Pfb1,Pl)
        CALL STORE(Pf1,Pn)
      ELSE
        CALL STORE(Pf1,Pl)
        CALL STORE(Pfb1,Pn)
      ENDIF 

!     lW distribution

      Pboo(0)=PW1(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PW1(i)
      ENDDO

      CALL BOOST(Pboo,Pl,pdum1)
      CALL BOOST(Pboo,Pb,pdum2)
      cos_lW=-COSVEC(pdum1,pdum2)
      IF (IB1 .NE. 1) THEN
        quant(1)=cos_lW                 ! A_FB
        quant(2)=cos_lW                 ! A+
        quant(3)=cos_lW                 ! A-
      ELSE
        quant(4)=cos_lW                 ! A_FB
        quant(5)=cos_lW                 ! A+
        quant(6)=cos_lW                 ! A-
      ENDIF

!     t rest frame distributions

      Pboo(0)=PWb(0)                ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-PWb(i)
      ENDDO

      CALL BOOST(Pboo,Pj,PjR)       ! jet direction in "t" rest frame

      CALL BOOST(Pboo,Pl,pdum1)
      cos_lj=COSVEC(PjR,pdum1)      ! A_l
      CALL BOOST(Pboo,Pn,pdum1)
      cos_nj=COSVEC(PjR,pdum1)      ! A_n
      CALL BOOST(Pboo,Pb,pdum1)
      cos_bj=COSVEC(PjR,pdum1)      ! A_b
      IF (IB1 .NE. 1) THEN
      quant(7)=cos_lj
      quant(8)=cos_nj
      quant(9)=cos_bj
      ELSE
      quant(10)=cos_lj
      quant(11)=cos_nj
      quant(12)=cos_bj
      ENDIF

      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







