      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     External functions used

c      REAL*8 RAN2
c      INTEGER idum
c      COMMON /ranno/ idum

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3),P5(0:3),P6(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4,P5,P6
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      INTEGER IQFLAV1,IQFLAV2
      COMMON /QFLAV/ IQFLAV1,IQFLAV2

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 Cqq,Cqup,Cqu,Cuu,Cqup2,Cqu2
      COMMON /COUP4F/ Cqq,Cqup,Cqu,Cuu,Cqup2,Cqu2
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables for flavour structure

      INTEGER IDTAB1(12),IDTAB2(12)

!     Local variables

      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2
      INTEGER i

      DATA IDTAB1 /2,-2,2,-2,2,-2,4,-4,4,-4,4,-4/
      DATA IDTAB2 /2,-2,2,-2,4,-4,2,-2,4,-4,4,-4/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0

      RETURN

10    IF (IMODE .NE. 0) GOTO 11

!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)                          ! Already in pb
      IF (ABS(XWGTUP) .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,ABS(XWGTUP))
      NUMEVT=NUMEVT+1

!     Flavour

      ID(1)=IDTAB1(IPROC+4*(IQFLAV1+IQFLAV2-2))
      ID(2)=IDTAB2(IPROC+4*(IQFLAV1+IQFLAV2-2))
      ID(5)=5
      ID(8)=5

      IF (IF1 .EQ. 1) THEN
        ID(3)=12
        ID(4)=-11
      ELSE IF (IF1 .EQ. 2) THEN
        ID(3)=14
        ID(4)=-13
      ELSE IF (IF1 .EQ. 3) THEN
        ID(3)=16
        ID(4)=-15
      ELSE IF (IF1 .EQ. 4) THEN
        ID(3)=2
        ID(4)=-1
      ELSE IF (IF1 .EQ. 5) THEN
        ID(3)=4
        ID(4)=-3
      ELSE
        PRINT *,'Wrong IF1'
        STOP
      ENDIF

      IF (IF2 .EQ. 1) THEN
        ID(6)=12
        ID(7)=-11
      ELSE IF (IF2 .EQ. 2) THEN
        ID(6)=14
        ID(7)=-13
      ELSE IF (IF2 .EQ. 3) THEN
        ID(6)=16
        ID(7)=-15
      ELSE IF (IF2 .EQ. 4) THEN
        ID(6)=2
        ID(7)=-1
      ELSE IF (IF2 .EQ. 5) THEN
        ID(6)=4
        ID(7)=-3
      ELSE
        PRINT *,'Wrong IF1'
        STOP
      ENDIF

!     Change if tbar tbar

      IF (MOD(IPROC,2) .EQ. 0) THEN
      ID(3)=-ID(3)
      ID(4)=-ID(4)
      ID(5)=-ID(5)
      ID(6)=-ID(6)
      ID(7)=-ID(7)
      ID(8)=-ID(8)
      ENDIF


!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

!     Store all colours

      IF (ID(1) .GT. 0) THEN
        ICOL1(1)=1
      ELSE
        ICOL2(1)=1
      ENDIF
      IF (ID(2) .GT. 0) THEN
        ICOL1(2)=2
      ELSE
        ICOL2(2)=2
      ENDIF
      ICOL1(5)=ICOL1(1)
      ICOL2(5)=ICOL2(1)
      ICOL1(8)=ICOL1(2)
      ICOL2(8)=ICOL2(2)

      DO i=3,4
        IF (ABS(ID(i)) .LT. 10) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=11
          ELSE
            ICOL2(i)=11
          ENDIF
        ENDIF
      ENDDO

      DO i=6,7
        IF (ABS(ID(i)) .LT. 10) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=12
          ELSE
            ICOL2(i)=12
          ENDIF
        ENDIF
      ENDDO

      Pz1=Q1(3)
      Pz2=Q2(3)

      WRITE (35,3010) NUMEVT,XWGTUP,2d0*Q
      WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),P1(1),P1(2),P1(3)
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),P2(1),P2(2),P2(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),P3(1),P3(2),P3(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),P4(1),P4(2),P4(3)
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),P5(1),P5(2),P5(3)
      WRITE (35,3030) ID(8),ICOL1(8),ICOL2(8),P6(1),P6(2),P6(3)
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 18
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4006) IQFLAV1,IQFLAV2
      WRITE (36,4010) Cqq,Cqup,Cqu,Cuu
      WRITE (36,4011) Cqup2,Cqu2
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_tot,ERR_tot
      CLOSE (36)
      RETURN

12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (I1,' ',I1,'        ',
     &  '                      ! Initial flavours')
4010  FORMAT (F6.1,'  ',F6.1,'  ',F6.1,'  ',F6.1,
     &  '   ! Cqq, Cqup, Cqu, Cuu')
4011  FORMAT (F6.1,'  ',F6.1,'                   ',
     &  '! Cqup2, Cqu2')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! tt cross section and error')
      END

