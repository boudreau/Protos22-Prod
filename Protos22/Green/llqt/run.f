      SUBROUTINE CHGPAR(id,step)
      IMPLICIT NONE

!     Arguments

      INTEGER id
      REAL*8 step

!     Parameters that can be changed

      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2
      COMMON /COUP4F/ Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2

      IF (id .EQ. 1) THEN
        Clq=Clq+step
      ELSE IF (id .EQ. 2) THEN
        Clu=Clu+step
      ELSE IF (id .EQ. 3) THEN
        Cqe=Cqe+step
      ELSE IF (id .EQ. 4) THEN
        Ceu=Ceu+step
      ELSE IF (id .EQ. 5) THEN
        Cqle1=Cqle1+step
      ELSE IF (id .EQ. 6) THEN
        Clqe1=Clqe1+step
      ELSE IF (id .EQ. 7) THEN
        Cqle2=Cqle2+step
      ELSE IF (id .EQ. 8) THEN
        Clqe2=Clqe2+step
      ELSE IF (id .EQ. -1) THEN
        idum0=idum0+1
      ELSE
        print *,id
        PRINT 100
        STOP
      ENDIF

      RETURN

100   FORMAT ('Unsupported parameter scan')
      END


      SUBROUTINE LOGINI(IMODE,id)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE,id

!     Parameters

      REAL*8 Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2
      COMMON /COUP4F/ Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2

!     For logging

      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot
      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l

!     Local

      REAL*8 scpar
      INTEGER i

      IF (IMODE .EQ. -1) THEN 
        OPEN (40,FILE='output/'//PROCNAME(1:l)//'.log',
     &    status='unknown')
        RETURN
      ELSE IF (IMODE .EQ. 1) THEN
        CLOSE (40)
        RETURN
      ELSE IF (IMODE .NE. 0) THEN
        PRINT 99
        STOP
      ENDIF

!     Write results

      IF ((id .EQ. 0) .OR. (id .EQ. -1)) THEN
        scpar=0d0
      ELSE IF (id .EQ. 1) THEN
        scpar=Clq
      ELSE IF (id .EQ. 2) THEN
        scpar=Clu
      ELSE IF (id .EQ. 3) THEN
        scpar=Cqe
      ELSE IF (id .EQ. 4) THEN
        scpar=Ceu
      ELSE IF (id .EQ. 5) THEN
        scpar=Cqle1
      ELSE IF (id .EQ. 6) THEN
        scpar=Clqe1
      ELSE IF (id .EQ. 7) THEN
        scpar=Cqle2
      ELSE IF (id .EQ. 8) THEN
        scpar=Clqe2
      ELSE
        PRINT 100
        STOP
      ENDIF

      WRITE (40,200) scpar,SIG_tot,(asym(i),i=1,6)

      RETURN
99    FORMAT ('Error in WRITELOG call')
100   FORMAT ('Unsupported parameter scan')
200   FORMAT (F7.4,' ',D12.6,' ',D12.6,' ',D12.6,' ',D12.6,' ',
     &  D12.6,' ',D12.6,' ',D12.6)
      END


