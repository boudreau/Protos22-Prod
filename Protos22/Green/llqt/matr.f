      REAL*8 FUNCTION GG_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     PROCESS : g g  -> t t~  with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                            

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      INTEGER IFCN_T,IFCN_TB
      COMMON /tflagF/ IFCN_T,IFCN_TB
      REAL*8 Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2
      COMMON /COUP4F/ Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWF(2),GG(2)
      COMPLEX*16 Wdum(6)
      REAL*8 PL(2),PR(2)
      REAL*8 mq1,mq2

!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

!     Four-fermion projectors

      PL(1) = 1d0
      PL(2) = 0d0
      PR(1) = 0d0
      PR(2) = 1d0

      IF (IFCN_T .EQ. 1) THEN
        mq1=0d0
      ELSE
        mq1=mb
      ENDIF
      IF (IFCN_TB .EQ. 1) THEN
        mq2=0d0
      ELSE
        mq2=mb
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                            
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)                            

!     -----------------------------------------------------

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)           ! l out: nu / e_j^-
      CALL IXXXXX(P3b,0d0,NHEL(4),-1,W3b)          ! l in:  e+ / e_i^+
      CALL OXXXXX(P3c,mq1,NHEL(5),1,W3c)           !         b / u_k

      IF (IFCN_T .EQ. 1) THEN

      CALL FVOIOX(W3c,W3b,W3a,PL,PL,mt,Gt,W3)
      W3(1)=W3(1)*Clq/1d6
      W3(2)=W3(2)*Clq/1d6
      W3(3)=W3(3)*Clq/1d6
      W3(4)=W3(4)*Clq/1d6
      IF (Clu .NE. 0d0) THEN
        CALL FVOIOX(W3c,W3b,W3a,PR,PL,mt,Gt,Wdum)
        W3(1)=W3(1)-Wdum(1)*Clu/2d6                 ! Extra -1/2
        W3(2)=W3(2)-Wdum(2)*Clu/2d6
        W3(3)=W3(3)-Wdum(3)*Clu/2d6
        W3(4)=W3(4)-Wdum(4)*Clu/2d6
      ENDIF
      IF (Cqe .NE. 0d0) THEN
        CALL FVOIOX(W3c,W3b,W3a,PL,PR,mt,Gt,Wdum)
        W3(1)=W3(1)-Wdum(1)*Cqe/2d6                 ! Extra -1/2
        W3(2)=W3(2)-Wdum(2)*Cqe/2d6
        W3(3)=W3(3)-Wdum(3)*Cqe/2d6
        W3(4)=W3(4)-Wdum(4)*Cqe/2d6
      ENDIF
      IF (Ceu .NE. 0d0) THEN
        CALL FVOIOX(W3c,W3b,W3a,PR,PR,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Ceu/1d6
        W3(2)=W3(2)+Wdum(2)*Ceu/1d6
        W3(3)=W3(3)+Wdum(3)*Ceu/1d6
        W3(4)=W3(4)+Wdum(4)*Ceu/1d6
      ENDIF
      IF (Cqle1 .NE. 0d0) THEN
        CALL FSOIOX(W3a,W3b,W3c,PR,PR,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Cqle1/1d6               ! -1 from fermion interchange
        W3(2)=W3(2)+Wdum(2)*Cqle1/1d6               ! and -1 from coefficient 
        W3(3)=W3(3)+Wdum(3)*Cqle1/1d6
        W3(4)=W3(4)+Wdum(4)*Cqle1/1d6
      ENDIF
      IF (Clqe1 .NE. 0d0) THEN
        CALL FSOIOX(W3c,W3b,W3a,PR,PR,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Clqe1/1d6
        W3(2)=W3(2)+Wdum(2)*Clqe1/1d6
        W3(3)=W3(3)+Wdum(3)*Clqe1/1d6
        W3(4)=W3(4)+Wdum(4)*Clqe1/1d6
      ENDIF
      IF (Cqle2 .NE. 0d0) THEN
        CALL FSOIOX(W3a,W3b,W3c,PL,PL,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Cqle2/1d6               ! -1 from fermion interchange
        W3(2)=W3(2)+Wdum(2)*Cqle2/1d6               ! and -1 from coefficient 
        W3(3)=W3(3)+Wdum(3)*Cqle2/1d6
        W3(4)=W3(4)+Wdum(4)*Cqle2/1d6
      ENDIF
      IF (Clqe2 .NE. 0d0) THEN
        CALL FSOIOX(W3c,W3b,W3a,PL,PL,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Clqe2/1d6
        W3(2)=W3(2)+Wdum(2)*Clqe2/1d6
        W3(3)=W3(3)+Wdum(3)*Clqe2/1d6
        W3(4)=W3(4)+Wdum(4)*Clqe2/1d6
      ENDIF

      ELSE

      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)               ! t

      ENDIF

!     -----------------------------------------------------

      CALL IXXXXX(P4a,0d0,NHEL(6),-1,W4a)              ! l in:  nu~ / e_j^+
      CALL OXXXXX(P4b,0d0,NHEL(7),1,W4b)               ! l out: e-  / e_i^-
      CALL IXXXXX(P4c,mq2,NHEL(8),-1,W4c)              !        b~  / u~_k

      IF (IFCN_TB .EQ. 1) THEN

      CALL FVIIOX(W4c,W4a,W4b,PL,PL,mt,Gt,W4)
      W4(1)=W4(1)*Clq/1d6
      W4(2)=W4(2)*Clq/1d6
      W4(3)=W4(3)*Clq/1d6
      W4(4)=W4(4)*Clq/1d6
      IF (Clu .NE. 0d0) THEN
        CALL FVIIOX(W4c,W4a,W4b,PR,PL,mt,Gt,Wdum)
        W4(1)=W4(1)-Wdum(1)*Clu/2d6                 ! Extra -1/2
        W4(2)=W4(2)-Wdum(2)*Clu/2d6
        W4(3)=W4(3)-Wdum(3)*Clu/2d6
        W4(4)=W4(4)-Wdum(4)*Clu/2d6
      ENDIF
      IF (Cqe .NE. 0d0) THEN
        CALL FVIIOX(W4c,W4a,W4b,PL,PR,mt,Gt,Wdum)
        W4(1)=W4(1)-Wdum(1)*Cqe/2d6                 ! Extra -1/2
        W4(2)=W4(2)-Wdum(2)*Cqe/2d6
        W4(3)=W4(3)-Wdum(3)*Cqe/2d6
        W4(4)=W4(4)-Wdum(4)*Cqe/2d6
      ENDIF
      IF (Ceu .NE. 0d0) THEN
        CALL FVIIOX(W4c,W4a,W4b,PR,PR,mt,Gt,Wdum)
        W4(1)=W4(1)+Wdum(1)*Ceu/1d6
        W4(2)=W4(2)+Wdum(2)*Ceu/1d6
        W4(3)=W4(3)+Wdum(3)*Ceu/1d6
        W4(4)=W4(4)+Wdum(4)*Ceu/1d6
      ENDIF
      IF (Cqle1 .NE. 0d0) THEN
        CALL FSIIOX(W4a,W4c,W4b,PL,PL,mt,Gt,Wdum)   ! H.C. of LR is RL
        W4(1)=W4(1)+Wdum(1)*Cqle1/1d6               ! -1 from fermion interchange
        W4(2)=W4(2)+Wdum(2)*Cqle1/1d6               ! and -1 from coefficient 
        W4(3)=W4(3)+Wdum(3)*Cqle1/1d6
        W4(4)=W4(4)+Wdum(4)*Cqle1/1d6
      ENDIF
      IF (Clqe1 .NE. 0d0) THEN
        CALL FSIIOX(W4c,W4a,W4b,PL,PL,mt,Gt,Wdum)   ! H.C. of LR is RL
        W4(1)=W4(1)+Wdum(1)*Clqe1/1d6
        W4(2)=W4(2)+Wdum(2)*Clqe1/1d6
        W4(3)=W4(3)+Wdum(3)*Clqe1/1d6
        W4(4)=W4(4)+Wdum(4)*Clqe1/1d6
      ENDIF
      IF (Cqle2 .NE. 0d0) THEN
        CALL FSIIOX(W4a,W4c,W4b,PR,PR,mt,Gt,Wdum)   ! H.C. of RL is LR
        W4(1)=W4(1)+Wdum(1)*Cqle2/1d6               ! -1 from fermion interchange
        W4(2)=W4(2)+Wdum(2)*Cqle2/1d6               ! and -1 from coefficient 
        W4(3)=W4(3)+Wdum(3)*Cqle2/1d6
        W4(4)=W4(4)+Wdum(4)*Cqle2/1d6
      ENDIF
      IF (Clqe2 .NE. 0d0) THEN
        CALL FSIIOX(W4c,W4a,W4b,PR,PR,mt,Gt,Wdum)   ! H.C. of RL is LR
        W4(1)=W4(1)+Wdum(1)*Clqe2/1d6
        W4(2)=W4(2)+Wdum(2)*Clqe2/1d6
        W4(3)=W4(3)+Wdum(3)*Clqe2/1d6
        W4(4)=W4(4)+Wdum(4)*Clqe2/1d6
      ENDIF

      ELSE

      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4d)                  ! W-
      CALL FVIXXX(W4c,W4d,GWF,mt,Gt,W4)                   ! t~

      ENDIF

!     -----------------------------------------------------

      CALL FVOXXX(W3,W2,GG,mt,Gt,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mt,Gt,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_TT = GG_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_TT = GG_TT/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END




      REAL*8 FUNCTION UU_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> t t~    with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      INTEGER IFCN_T,IFCN_TB
      COMMON /tflagF/ IFCN_T,IFCN_TB
      REAL*8 Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2
      COMMON /COUP4F/ Clq,Clu,Cqe,Ceu,Cqle1,Clqe1,Cqle2,Clqe2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWF(2),GG(2)
      COMPLEX*16 Wdum(6)
      REAL*8 PL(2),PR(2)
      REAL*8 mq1,mq2

!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

!     Four-fermion projectors

      PL(1) = 1d0
      PL(2) = 0d0
      PR(1) = 0d0
      PR(2) = 1d0

      IF (IFCN_T .EQ. 1) THEN
        mq1=0d0
      ELSE
        mq1=mb
      ENDIF
      IF (IFCN_TB .EQ. 1) THEN
        mq2=0d0
      ELSE
        mq2=mb
      ENDIF
      
!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)                       
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                       

!     -----------------------------------------------------

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)           ! l out: nu / e-
      CALL IXXXXX(P3b,0d0,NHEL(4),-1,W3b)          ! l in:  e+ / e+
      CALL OXXXXX(P3c,mq1,NHEL(5),1,W3c)           ! b / u_k

      IF (IFCN_T .EQ. 1) THEN

      CALL FVOIOX(W3c,W3b,W3a,PL,PL,mt,Gt,W3)
      W3(1)=W3(1)*Clq/1d6
      W3(2)=W3(2)*Clq/1d6
      W3(3)=W3(3)*Clq/1d6
      W3(4)=W3(4)*Clq/1d6
      IF (Clu .NE. 0d0) THEN
        CALL FVOIOX(W3c,W3b,W3a,PR,PL,mt,Gt,Wdum)
        W3(1)=W3(1)-Wdum(1)*Clu/2d6                 ! Extra -1/2
        W3(2)=W3(2)-Wdum(2)*Clu/2d6
        W3(3)=W3(3)-Wdum(3)*Clu/2d6
        W3(4)=W3(4)-Wdum(4)*Clu/2d6
      ENDIF
      IF (Cqe .NE. 0d0) THEN
        CALL FVOIOX(W3c,W3b,W3a,PL,PR,mt,Gt,Wdum)
        W3(1)=W3(1)-Wdum(1)*Cqe/2d6                 ! Extra -1/2
        W3(2)=W3(2)-Wdum(2)*Cqe/2d6
        W3(3)=W3(3)-Wdum(3)*Cqe/2d6
        W3(4)=W3(4)-Wdum(4)*Cqe/2d6
      ENDIF
      IF (Ceu .NE. 0d0) THEN
        CALL FVOIOX(W3c,W3b,W3a,PR,PR,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Ceu/1d6
        W3(2)=W3(2)+Wdum(2)*Ceu/1d6
        W3(3)=W3(3)+Wdum(3)*Ceu/1d6
        W3(4)=W3(4)+Wdum(4)*Ceu/1d6
      ENDIF
      IF (Cqle1 .NE. 0d0) THEN
        CALL FSOIOX(W3a,W3b,W3c,PR,PR,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Cqle1/1d6               ! -1 from fermion interchange
        W3(2)=W3(2)+Wdum(2)*Cqle1/1d6               ! and -1 from coefficient 
        W3(3)=W3(3)+Wdum(3)*Cqle1/1d6
        W3(4)=W3(4)+Wdum(4)*Cqle1/1d6
      ENDIF
      IF (Clqe1 .NE. 0d0) THEN
        CALL FSOIOX(W3c,W3b,W3a,PR,PR,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Clqe1/1d6
        W3(2)=W3(2)+Wdum(2)*Clqe1/1d6
        W3(3)=W3(3)+Wdum(3)*Clqe1/1d6
        W3(4)=W3(4)+Wdum(4)*Clqe1/1d6
      ENDIF
      IF (Cqle2 .NE. 0d0) THEN
        CALL FSOIOX(W3a,W3b,W3c,PL,PL,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Cqle2/1d6               ! -1 from fermion interchange
        W3(2)=W3(2)+Wdum(2)*Cqle2/1d6               ! and -1 from coefficient 
        W3(3)=W3(3)+Wdum(3)*Cqle2/1d6
        W3(4)=W3(4)+Wdum(4)*Cqle2/1d6
      ENDIF
      IF (Clqe2 .NE. 0d0) THEN
        CALL FSOIOX(W3c,W3b,W3a,PL,PL,mt,Gt,Wdum)
        W3(1)=W3(1)+Wdum(1)*Clqe2/1d6
        W3(2)=W3(2)+Wdum(2)*Clqe2/1d6
        W3(3)=W3(3)+Wdum(3)*Clqe2/1d6
        W3(4)=W3(4)+Wdum(4)*Clqe2/1d6
      ENDIF

      ELSE

      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)               ! t

      ENDIF

!     -----------------------------------------------------

      CALL IXXXXX(P4a,0d0,NHEL(6),-1,W4a)              ! l in:  nu~ / e+
      CALL OXXXXX(P4b,0d0,NHEL(7),1,W4b)               ! l out: e-  / e-
      CALL IXXXXX(P4c,mq2,NHEL(8),-1,W4c)              ! b~ / u~_k

      IF (IFCN_TB .EQ. 1) THEN

      CALL FVIIOX(W4c,W4a,W4b,PL,PL,mt,Gt,W4)
      W4(1)=W4(1)*Clq/1d6
      W4(2)=W4(2)*Clq/1d6
      W4(3)=W4(3)*Clq/1d6
      W4(4)=W4(4)*Clq/1d6
      IF (Clu .NE. 0d0) THEN
        CALL FVIIOX(W4c,W4a,W4b,PR,PL,mt,Gt,Wdum)
        W4(1)=W4(1)-Wdum(1)*Clu/2d6                 ! Extra -1/2
        W4(2)=W4(2)-Wdum(2)*Clu/2d6
        W4(3)=W4(3)-Wdum(3)*Clu/2d6
        W4(4)=W4(4)-Wdum(4)*Clu/2d6
      ENDIF
      IF (Cqe .NE. 0d0) THEN
        CALL FVIIOX(W4c,W4a,W4b,PL,PR,mt,Gt,Wdum)
        W4(1)=W4(1)-Wdum(1)*Cqe/2d6                 ! Extra -1/2
        W4(2)=W4(2)-Wdum(2)*Cqe/2d6
        W4(3)=W4(3)-Wdum(3)*Cqe/2d6
        W4(4)=W4(4)-Wdum(4)*Cqe/2d6
      ENDIF
      IF (Ceu .NE. 0d0) THEN
        CALL FVIIOX(W4c,W4a,W4b,PR,PR,mt,Gt,Wdum)
        W4(1)=W4(1)+Wdum(1)*Ceu/1d6
        W4(2)=W4(2)+Wdum(2)*Ceu/1d6
        W4(3)=W4(3)+Wdum(3)*Ceu/1d6
        W4(4)=W4(4)+Wdum(4)*Ceu/1d6
      ENDIF
      IF (Cqle1 .NE. 0d0) THEN
        CALL FSIIOX(W4a,W4c,W4b,PL,PL,mt,Gt,Wdum)   ! H.C. of LR is RL
        W4(1)=W4(1)+Wdum(1)*Cqle1/1d6               ! -1 from fermion interchange
        W4(2)=W4(2)+Wdum(2)*Cqle1/1d6               ! and -1 from coefficient 
        W4(3)=W4(3)+Wdum(3)*Cqle1/1d6
        W4(4)=W4(4)+Wdum(4)*Cqle1/1d6
      ENDIF
      IF (Clqe1 .NE. 0d0) THEN
        CALL FSIIOX(W4c,W4a,W4b,PL,PL,mt,Gt,Wdum)   ! H.C. of LR is RL
        W4(1)=W4(1)+Wdum(1)*Clqe1/1d6
        W4(2)=W4(2)+Wdum(2)*Clqe1/1d6
        W4(3)=W4(3)+Wdum(3)*Clqe1/1d6
        W4(4)=W4(4)+Wdum(4)*Clqe1/1d6
      ENDIF
      IF (Cqle2 .NE. 0d0) THEN
        CALL FSIIOX(W4a,W4c,W4b,PR,PR,mt,Gt,Wdum)   ! H.C. of RL is LR
        W4(1)=W4(1)+Wdum(1)*Cqle2/1d6               ! -1 from fermion interchange
        W4(2)=W4(2)+Wdum(2)*Cqle2/1d6               ! and -1 from coefficient 
        W4(3)=W4(3)+Wdum(3)*Cqle2/1d6
        W4(4)=W4(4)+Wdum(4)*Cqle2/1d6
      ENDIF
      IF (Clqe2 .NE. 0d0) THEN
        CALL FSIIOX(W4c,W4a,W4b,PR,PR,mt,Gt,Wdum)   ! H.C. of RL is LR
        W4(1)=W4(1)+Wdum(1)*Clqe2/1d6
        W4(2)=W4(2)+Wdum(2)*Clqe2/1d6
        W4(3)=W4(3)+Wdum(3)*Clqe2/1d6
        W4(4)=W4(4)+Wdum(4)*Clqe2/1d6
      ENDIF

      ELSE

      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4d)                  ! W-
      CALL FVIXXX(W4c,W4d,GWF,mt,Gt,W4)                   ! t~

      ENDIF

!     -----------------------------------------------------

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))

      CAMP(1) = -AMP(1)

      UU_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_TT = UU_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_TT = UU_TT/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END

