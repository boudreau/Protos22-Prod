      REAL*8 FUNCTION ran2(idum)
      IMPLICIT NONE
      
!     Arguments

      INTEGER idum
      
!     Parameters

      INTEGER IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,IR2,NTAB,NDIV
      REAL*8 AM,EPS,RNMX

      PARAMETER (IM1=2147483563,IM2=2147483399,AM=1d0/IM1,IMM1=IM1-1,
     *    IA1=40014,IA2=40692,IQ1=53668,IQ2=52774,IR1=12211,
     *    IR2=3791,NTAB=32,NDIV=1+IMM1/NTAB,EPS=1.2d-14,RNMX=1d0-EPS)

      INTEGER idum2,iv(NTAB),iy
      COMMON /ran2vars/ idum2,iv,iy
      
      INTEGER j,k
c      SAVE iv,iy,idum2
      DATA idum2/123456789/, iv/NTAB*0/, iy/0/
      if (idum.le.0) then
          idum=max(-idum,1)
          idum2=idum
          do j=NTAB+8,1,-1
              k=idum/IQ1
              idum=IA1*(idum-k*IQ1)-k*IR1
              if (idum.lt.0) idum=idum+IM1
              if (j.le.NTAB) iv(j)=idum
          enddo
          iy=iv(1)
      endif
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if (idum.lt.0) idum=idum+IM1
      k=idum2/IQ2
      idum2=IA2*(idum2-k*IQ2)-k*IR2
      if (idum2.lt.0) idum2=idum2+IM2
      j=1+iy/NDIV
      iy=iv(j)-idum2
      iv(j)=idum
      if (iy.lt.1) iy=iy+IMM1
      ran2=min(AM*iy,RNMX)
c      print *,'idum = ',idum,'  ran2 = ',ran2
c      print *,idum,idum2,iv,iy
      return
      END


      SUBROUTINE RAN2RESET(idum0)
      IMPLICIT NONE

!     Arguments

      INTEGER idum0
      
!     Commons

      INTEGER idum
      COMMON /ranno/ idum
      INTEGER idum2,iv(32),iy
      COMMON /ran2vars/ idum2,iv,iy

!     Local

      INTEGER i

      idum=idum0
      idum2=123456789
      DO i=1,32
        iv(i)=0
      ENDDO
      iy=0
      RETURN
      END
      
      
      
      DOUBLE PRECISION FUNCTION gauss(pull)
      REAL*8 pull,prob,prob2,ran2
      INTEGER idum
      COMMON /ranno/ idum
1     gauss=pull*(2*ran2(idum)-1)
      prob=exp(-gauss**2/2.d0)
      prob2=ran2(idum)
      IF (prob .lt. prob2) GOTO 1
      RETURN
      END



      SUBROUTINE BREIT(M,G,delta,Q,WT)
      IMPLICIT NONE
      REAL*8 M,G,delta,Q,WT,ran2,prop
      REAL*8 Qmin,Qmax,ymin,ymax,y
      INTEGER idum
      COMMON /ranno/ idum
      Qmin=M-delta*G
      Qmax=M+delta*G
      ymin=(atan((Qmin**2-M**2)/(M*G))/(M*G))
      ymax=(atan((Qmax**2-M**2)/(M*G))/(M*G))
      y=ymin+(ymax-ymin)*ran2(idum)
      Q=sqrt(M**2+M*G*tan(M*G*y))
      WT=ymax-ymin
      prop=(Q**2-M**2)**2+(M*G)**2
      WT=WT*prop
      RETURN
      END



      SUBROUTINE BOOST(P,Kp,K)

C     P is the momentum of a particle in a reference system O
C     Kp is a momentum in the particle's CM system O'
C     K is this momentum in the reference system O
C     In particular, if Kp=(m,0,0,0), K=P

      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 P(0:3),Kp(0:3),K(0:3)
      rmass=SQRT(DOT(P,P))
      K(0)=(P(0)*Kp(0)+P(1)*Kp(1)+P(2)*Kp(2)+P(3)*Kp(3))/rmass
      fact=(Kp(0)+K(0))/(P(0)+rmass)
      DO i=1,3
        K(i)=Kp(i)+fact*P(i)
      ENDDO
      RETURN
      END


      DOUBLE PRECISION FUNCTION DOT(p,q)
      REAL*8 p(0:3),q(0:3)
      DOT=p(0)*q(0)-p(1)*q(1)-p(2)*q(2)-p(3)*q(3)
      RETURN
      END

      DOUBLE PRECISION FUNCTION DOT3(p,q)
      REAL*8 p(0:3),q(0:3)
      DOT3=p(1)*q(1)+p(2)*q(2)+p(3)*q(3)
      RETURN
      END
      
      SUBROUTINE CROSSVEC(p,q,r)
      REAL*8 p(0:3),q(0:3),r(0:3)
      r(0)=0d0
      r(1)=p(2)*q(3)-p(3)*q(2)
      r(2)=-p(1)*q(3)+p(3)*q(1)
      r(3)=p(1)*q(2)-p(2)*q(1)
      RETURN
      END





      SUBROUTINE EXCHANGE(p,q)
      REAL*8 P(0:3),Q(0:3),TEMP(0:3)
      DO i=0,3
        TEMP(i)=P(i)
        P(i)=Q(i)
        Q(i)=TEMP(i)
      ENDDO
      RETURN
      END

      
      SUBROUTINE SUMVEC(P,Q,R)
      REAL*8 P(0:3),Q(0:3),R(0:3)
      DO i=0,3
        R(i)=P(i)+Q(i)
      ENDDO
      RETURN
      END


      SUBROUTINE SUMVEC3(P,Q,R,S)
      REAL*8 P(0:3),Q(0:3),R(0:3),S(0:3)
      DO i=0,3
        S(i)=P(i)+Q(i)+R(i)
      ENDDO
      RETURN
      END

      SUBROUTINE SUMVEC4(P,Q,R,S,T)
      REAL*8 P(0:3),Q(0:3),R(0:3),S(0:3),T(0:3)
      DO i=0,3
        T(i)=P(i)+Q(i)+R(i)+S(i)
      ENDDO
      RETURN
      END


      SUBROUTINE DIFVEC(P,Q,R)
      REAL*8 P(0:3),Q(0:3),R(0:3)
      DO i=0,3
        R(i)=P(i)-Q(i)
      ENDDO
      RETURN
      END

      SUBROUTINE STORE(P,Q)
      REAL*8 P(0:3),Q(0:3)
      DO i=0,3
        Q(i)=P(i)
      ENDDO
      RETURN
      END

      SUBROUTINE PARITY(P,Q)
      REAL*8 P(0:3),Q(0:3)
      Q(0)=P(0)
      DO i=1,3
        Q(i)=-P(i)
      ENDDO
      RETURN
      END

      DOUBLE PRECISION FUNCTION COSVEC(P,Q)
      REAL*8 P(0:3),Q(0:3)
      REAL*8 PQ,PP,QQ
      PQ=P(1)*Q(1)+P(2)*Q(2)+P(3)*Q(3)
      PP=P(1)**2+P(2)**2+P(3)**2
      QQ=Q(1)**2+Q(2)**2+Q(3)**2
      COSVEC=PQ/SQRT(PP*QQ)
      RETURN
      END

      DOUBLE PRECISION FUNCTION RAP(P)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION P(0:3)
      pt=sqrt(P(1)**2+P(2)**2)
      th=ATN(P(3),pt)
      RAP=-log(tan(th/2d0))
      RETURN
      END

      DOUBLE PRECISION FUNCTION RLEGO(P1,P2)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION P1(0:3),P2(0:3)
      pi=2d0*dasin(1d0)
      phi1=ATN(P1(1),P1(2))
      phi2=ATN(P2(1),P2(2))
      delta=phi2-phi1
      IF (delta .le. 0d0) delta=-delta
      IF (delta .ge. pi) delta=2d0*pi-delta
      eta1=RAP(P1)
      eta2=RAP(P2)
      RLEGO=SQRT((eta1-eta2)**2+delta**2)
      RETURN
      END

      DOUBLE PRECISION FUNCTION ATN(x,y)
      REAL*8 pi,x,y
      pi=2d0*dasin(1d0)
      ATN=atan(y/x)
      IF (x .lt. 0d0) ATN=ATN+pi
      IF (ATN .lt. 0d0) ATN=ATN+2.d0*pi
      RETURN
      END

      INTEGER FUNCTION LONGITUD(name)
      CHARACTER*100 name
      INTEGER i
      i = LEN(name)
      DO WHILE (name(i:i) .EQ. ' ')
        i = i - 1
      ENDDO
      LONGITUD = i
      RETURN
      END


      SUBROUTINE GETPDF(x,Q1,fu,fd,fus,fds,fs,fc,fb,fg,ISTAT)
      IMPLICIT REAL*8 (A-H,O-Z)

      CALL STRUCTM(x,Q1,fu,fd,fus,fds,fs,fc,fb,ft,fg)
      fu=fu/x
      fd=fd/x
      fus=fus/x
      fds=fds/x
      fs=fs/x
      fc=fc/x
      fb=fb/x
      fg=fg/x
      fu=fu+fus
      fd=fd+fds
      ISTAT=1
      IF ((fg .LT. 0) .OR. (fu .LT. 0) .OR. (fd .LT. 0) .OR.
     &  (fus .LT. 0) .OR. (fds .LT. 0) .OR. (fs .LT. 0) .OR. 
     &  (fc .LT. 0) .OR. (fb .LT. 0)) ISTAT=0 
      RETURN
      END


      SUBROUTINE GETPDF_tbj(x,Q1,Q2,fu,fd,fus,fds,fs,fc,fb,fg,ISTAT)
      IMPLICIT REAL*8 (A-H,O-Z)

      CALL STRUCTM(x,Q1,fu,fd,fus,fds,fs,fc,fb,ft,fg2)
      CALL STRUCTM(x,Q2,fu2,fd2,fus2,fds2,fs2,fc2,fb2,ft2,fg)
      fu=fu/x
      fd=fd/x
      fus=fus/x
      fds=fds/x
      fs=fs/x
      fc=fc/x
      fb=fb/x
      fg=fg/x
      fu=fu+fus
      fd=fd+fds
      ISTAT=1
      IF ((fg .LT. 0) .OR. (fu .LT. 0) .OR. (fd .LT. 0) .OR.
     &  (fus .LT. 0) .OR. (fds .LT. 0) .OR. (fs .LT. 0) .OR. 
     &  (fc .LT. 0) .OR. (fb .LT. 0)) ISTAT=0 
      RETURN
      END


      SUBROUTINE GETPDF_tj(x,Q1,Q2,fu,fd,fus,fds,fs,fc,fb,fg,ISTAT)
      IMPLICIT REAL*8 (A-H,O-Z)

      CALL STRUCTM(x,Q1,fu,fd,fus,fds,fs,fc,fb2,ft,fg)
      CALL STRUCTM(x,Q2,fu2,fd2,fus2,fds2,fs2,fc2,fb,ft2,fg2)
      fu=fu/x
      fd=fd/x
      fus=fus/x
      fds=fds/x
      fs=fs/x
      fc=fc/x
      fb=fb/x
      fg=fg/x
      fu=fu+fus
      fd=fd+fds
      ISTAT=1
      IF ((fg .LT. 0) .OR. (fu .LT. 0) .OR. (fd .LT. 0) .OR.
     &  (fus .LT. 0) .OR. (fds .LT. 0) .OR. (fs .LT. 0) .OR. 
     &  (fc .LT. 0) .OR. (fb .LT. 0)) ISTAT=0 
      RETURN
      END

