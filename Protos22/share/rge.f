      SUBROUTINE SETALPHA(Q)
      IMPLICIT NONE

!     Arguments

      REAL*8 Q

!     Required data

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau

      REAL*8 pi

!     Output

      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs

!     Local

      REAL*8 aY1,aL1,aY2,aL2
      REAL*8 sw

      pi=2d0*dasin(1d0)

      IF (Q .LE. mt) THEN
        aY1=(1d0-sw2_MZ)/alpha_MZ-103d0/30d0/(2d0*pi)*LOG(Q/MZ)
        aL1=sw2_MZ/alpha_MZ+13d0/3d0/(2d0*pi)*LOG(Q/MZ)
        alpha=1d0/(aY1+aL1)
        sw2=aL1/(aL1+aY1)
      ELSE
        aY1=(1d0-sw2_MZ)/alpha_MZ-103d0/30d0/(2d0*pi)*LOG(mt/MZ)
        aL1=sw2_MZ/alpha_MZ+13d0/3d0/(2d0*pi)*LOG(mt/MZ)
        aY2=aY1-4d0/(2d0*pi)*LOG(Q/mt)
        aL2=aL1+10d0/3d0/(2d0*pi)*LOG(Q/mt)
        alpha=1d0/(aY2+aL2)
        sw2=aL2/(aL2+aY2)
      ENDIF

      sw=sqrt(sw2)
      e=sqrt(4d0*pi*alpha)
      g=e/sw

      RETURN
      END

      SUBROUTINE SETALPHAS(Q)
      IMPLICIT NONE

!     Arguments

      REAL*8 Q

!     Required data

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau

      REAL*8 pi

!     Output

      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs

!     Local

      REAL*8 as1,as2

      pi=2d0*dasin(1d0)

      IF (Q .LE. mt) THEN
        as1=1d0/alphas_MZ+23d0/3d0/(2d0*pi)*LOG(Q/MZ)
        alpha_s=1d0/as1
      ELSE
        as1=1d0/alphas_MZ+23d0/3d0/(2d0*pi)*LOG(mt/MZ)
        as2=as1+7d0/(2d0*pi)*LOG(Q/mt)
        alpha_s=1d0/as2
      ENDIF

      gs=sqrt(4d0*pi*alpha_s)

      RETURN
      END

