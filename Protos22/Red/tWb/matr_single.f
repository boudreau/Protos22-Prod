      REAL*8 FUNCTION GG_Tsin(P1,P2,P3a,P3b,P3c,P4a,P4b,P5,NHEL)

!     FOR PROCESS : g g  -> t w- b~  

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=8,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),
     &  P4a(0:3),P4b(0:3),P5(0:3)                             
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W6(6),W7(6),W8(6),W9(6),W10(6)
      COMPLEX*16 W11(6),W12(6),W13(6),W15(6)
      COMPLEX*16 W16(6),W17(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR),CAMPT(NCOLOR)

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Aon,Wdum(6)
      REAL*8 F1MASS,F2MASS,TT

!     Flags

      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC

!     Colour data

      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GWtb_p(1)=GWF(1)*CONJG(VL)     ! t production: b in t out
      GWtb_p(2)=GWF(1)*CONJG(VR)
      GWtb_d(1)=GWF(1)*VL            ! t decay: t in b out
      GWtb_d(2)=GWF(1)*VR

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)              ! nu
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)               ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOCXX(W3c,W3d,GWtb_d,mt,Gt,W3)            ! t
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Wtb,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL IXXXXX(P4a,0d0,NHEL(6),-1,W4a)               ! nu~ / u~
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)             ! e-  / d
      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4)                 ! W-

      CALL IXXXXX(P5,mb,NHEL(8),-1,W5)

      CALL FVOXXX(W3,W2,GG,mt,Gt,W6)
      CALL FVIXXX(W5,W1,GG,mb,0d0,W7)
      CALL IOVCXX(W7,W6,W4,GWtb_p,AMP(1))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W7,W6,W4,G2Wbt,-1,Aon)
        AMP(1)=AMP(1)+Aon
      ENDIF
      
      CALL FVOXXX(W6,W1,GG,mt,Gt,W8)
      CALL IOVCXX(W5,W8,W4,GWtb_p,AMP(2))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W5,W8,W4,G2Wbt,-1,Aon)
        AMP(2)=AMP(2)+Aon
      ENDIF

      CALL FVOXXX(W3,W1,GG,mt,Gt,W9)
      CALL FVIXXX(W5,W2,GG,mb,0d0,W10)
      CALL IOVCXX(W10,W9,W4,GWtb_p,AMP(3))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W10,W9,W4,G2Wbt,-1,Aon)
        AMP(3)=AMP(3)+Aon
      ENDIF

      CALL FVOXXX(W9,W2,GG,mt,Gt,W11)
      CALL IOVCXX(W5,W11,W4,GWtb_p,AMP(4))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W5,W11,W4,G2Wbt,-1,Aon)
        AMP(4)=AMP(4)+Aon
      ENDIF

      CALL FVIXXX(W7,W2,GG,mb,0d0,W12)
      CALL IOVCXX(W12,W3,W4,GWtb_p,AMP(5))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W12,W3,W4,G2Wbt,-1,Aon)
        AMP(5)=AMP(5)+Aon
      ENDIF

      CALL FVIXXX(W10,W1,GG,mb,0d0,W13)
      CALL IOVCXX(W13,W3,W4,GWtb_p,AMP(6))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W13,W3,W4,G2Wbt,-1,Aon)
        AMP(6)=AMP(6)+Aon
      ENDIF
      
      CALL JGGXXX(W1,W2,gs,W15)
      CALL FVOXXX(W3,W15,GG,mt,Gt,W16)
      CALL IOVCXX(W5,W16,W4,GWtb_p,AMP(7))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W5,W16,W4,G2Wbt,-1,Aon)
        AMP(7)=AMP(7)+Aon
      ENDIF

      CALL FVIXXX(W5,W15,GG,mb,0d0,W17)
      CALL IOVCXX(W17,W3,W4,GWtb_p,AMP(8))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W17,W3,W4,G2Wbt,-1,Aon)
        AMP(8)=AMP(8)+Aon
      ENDIF

!     Amp 1 tt -> 1, 2, 5
!     Amp 2 tt -> 3, 4, 6
!     Amp 3 tt -> 7, 8

      CAMP(1) = -(AMP(1)+AMP(2)+AMP(5))+(AMP(7)+AMP(8))
      CAMP(2) = -(AMP(3)+AMP(4)+AMP(6))-(AMP(7)+AMP(8))

      GG_Tsin = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_Tsin = GG_Tsin + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_Tsin = GG_Tsin/64d0

      CAMPT(1) = -(AMP(2))+(AMP(7))
      CAMPT(2) = -(AMP(4))-(AMP(7))

      TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMPT(J)
        ENDDO
        TT = TT + ZTEMP*DCONJG(CAMPT(I))/DENOM(I)   
      ENDDO
      TT = TT/64d0

      GG_Tsin = GG_Tsin - TT

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) - CAMPT(i)*dconjg(CAMPT(i))
      ENDDO

      RETURN
      END


      REAL*8 FUNCTION GG_TBsin(P1,P2,P3a,P3b,P3c,P4a,P4b,P5,NHEL)

!     FOR PROCESS : g g  -> t~ w+ b  

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=8,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),
     &  P4a(0:3),P4b(0:3),P5(0:3)                            
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W6(6),W7(6),W8(6),W9(6),W10(6)
      COMPLEX*16 W11(6),W12(6),W13(6),W15(6)
      COMPLEX*16 W16(6),W17(6) 
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR),CAMPT(NCOLOR)

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Aon,Wdum(6)
      REAL*8 F1MASS,F2MASS,TT

!     Flags

      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC

!     Colour data

      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GWtb_p(1)=GWF(1)*VL            ! tbar production: t in b out
      GWtb_p(2)=GWF(1)*VR
      GWtb_d(1)=GWF(1)*CONJG(VL)     ! tbar decay: b in t out
      GWtb_d(2)=GWF(1)*CONJG(VR)

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)                 ! nu~
      CALL OXXXXX(P3b,F1MASS,NHEL(4),1,W3b)               ! e-
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                  ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                  ! W-
      CALL FVICXX(W3c,W3d,GWtb_d,mt,Gt,W3)                ! t~
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVISmX(W3c,W3d,G2Wbt,mt,Gt,-1,Wdum)          ! t~ anom
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)            ! nu / u
      CALL IXXXXX(P4b,F2MASS,NHEL(7),-1,W4b)        ! e+ / d~
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4)             ! W+

      CALL OXXXXX(P5,mb,NHEL(8),1,W5)

      CALL FVIXXX(W3,W2,GG,mt,Gt,W6)
      CALL FVOXXX(W5,W1,GG,mb,0d0,W7)
      CALL IOVCXX(W6,W7,W4,GWtb_p,AMP(1))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W6,W7,W4,G2Wtb,1,Aon)
        AMP(1)=AMP(1)+Aon
      ENDIF

      CALL FVIXXX(W6,W1,GG,mt,Gt,W8)
      CALL IOVCXX(W8,W5,W4,GWtb_p,AMP(2))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W8,W5,W4,G2Wtb,1,Aon)
        AMP(2)=AMP(2)+Aon
      ENDIF

      CALL FVIXXX(W3,W1,GG,mt,Gt,W9)
      CALL FVOXXX(W5,W2,GG,mb,0d0,W10)
      CALL IOVCXX(W9,W10,W4,GWtb_p,AMP(3))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W9,W10,W4,G2Wtb,1,Aon)
        AMP(3)=AMP(3)+Aon
      ENDIF

      CALL FVIXXX(W9,W2,GG,mt,Gt,W11)
      CALL IOVCXX(W11,W5,W4,GWtb_p,AMP(4))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W11,W5,W4,G2Wtb,1,Aon)
        AMP(4)=AMP(4)+Aon
      ENDIF

      CALL FVOXXX(W7,W2,GG,mb,0d0,W12)
      CALL IOVCXX(W3,W12,W4,GWtb_p,AMP(5))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W3,W12,W4,G2Wtb,1,Aon)
        AMP(5)=AMP(5)+Aon
      ENDIF

      CALL FVOXXX(W10,W1,GG,mb,0d0,W13)
      CALL IOVCXX(W3,W13,W4,GWtb_p,AMP(6))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W3,W13,W4,G2Wtb,1,Aon)
        AMP(6)=AMP(6)+Aon
      ENDIF

      CALL JGGXXX(W1,W2,gs,W15)
      CALL FVIXXX(W3,W15,GG,mt,Gt,W16)
      CALL IOVCXX(W16,W5,W4,GWtb_p,AMP(7))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W16,W5,W4,G2Wtb,1,Aon)
        AMP(7)=AMP(7)+Aon
      ENDIF

      CALL FVOXXX(W5,W15,GG,mb,0d0,W17)
      CALL IOVCXX(W3,W17,W4,GWtb_p,AMP(8))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W3,W17,W4,G2Wtb,1,Aon)
        AMP(8)=AMP(8)+Aon
      ENDIF

!     Amp 1 tt -> 3, 4, 6
!     Amp 2 tt -> 1, 2, 5
!     Amp 3 tt -> 7, 8

      CAMP(1) = -(AMP(3)+AMP(4)+AMP(6))+(AMP(7)+AMP(8))
      CAMP(2) = -(AMP(1)+AMP(2)+AMP(5))-(AMP(7)+AMP(8))

      GG_TBsin = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_TBsin = GG_TBsin + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_TBsin = GG_TBsin/64d0

      CAMPT(1) = -(AMP(4))+(AMP(7))
      CAMPT(2) = -(AMP(2))-(AMP(7))

      TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMPT(J)
        ENDDO
        TT = TT + ZTEMP*DCONJG(CAMPT(I))/DENOM(I)   
      ENDDO
      TT=TT/64d0

      GG_TBsin = GG_TBsin - TT

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) - CAMPT(i)*dconjg(CAMPT(i))
      ENDDO

      RETURN
      END

