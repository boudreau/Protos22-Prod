      REAL*8 FUNCTION BU_TD(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     FOR PROCESS : b u  -> t d

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC
      REAL*8 x1,x2,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,Q,IDIR,IL

!     Couplings and other

      REAL*8 GWF(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 LMASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /1d0/                  
      DATA EIGEN_VEC(1,1) /-1d0 /                  
      DATA EIGEN_VEC(2,1) /-1d0 /                  

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GWtb_p(1)=GWF(1)*CONJG(VL)     ! t production: b in t out
      GWtb_p(2)=GWF(1)*CONJG(VR)
      GWtb_d(1)=GWF(1)*VL            ! t decay: t in b out
      GWtb_d(2)=GWF(1)*VR

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      LMASS=0d0
      IF (IL .EQ. 3) LMASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! b
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)     ! u
      CALL OXXXXX(P4,0d0,NHEL(6),1,W4)     ! d

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)            ! nu
      CALL IXXXXX(P3b,LMASS,NHEL(4),-1,W3b)         ! e+
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)             ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)            ! W+
      CALL FVOCXX(W3c,W3d,GWtb_d,mt,Gt,W3)            ! t
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Wtb,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF
      

      CALL JIOXXX(W2,W4,GWF,MW,GW,W5)
      CALL IOVCXX(W1,W3,W5,GWtb_p,AMP(1))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W1,W3,W5,G2Wbt,-1,AMP(2))
      ELSE
        AMP(2)=0d0
      ENDIF
      
      BU_TD = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          BU_TD = BU_TD+ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



      REAL*8 FUNCTION BDB_TUB(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     FOR PROCESS : b d~  -> t u~  

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC
      REAL*8 x1,x2,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,Q,IDIR,IL

!     Couplings and other

      REAL*8 GWF(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 LMASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /1d0/
      DATA EIGEN_VEC(1,1) /1d0/
      DATA EIGEN_VEC(2,1) /1d0/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GWtb_p(1)=GWF(1)*CONJG(VL)     ! t production: b in t out
      GWtb_p(2)=GWF(1)*CONJG(VR)
      GWtb_d(1)=GWF(1)*VL            ! t decay: t in b out
      GWtb_d(2)=GWF(1)*VR

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      LMASS=0d0
      IF (IL .EQ. 3) LMASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! b
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! dbar
      CALL IXXXXX(P4,0d0,NHEL(6),-1,W4)    ! ubar

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)            ! nu
      CALL IXXXXX(P3b,LMASS,NHEL(4),-1,W3b)         ! e+
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)             ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)            ! W+
      CALL FVOCXX(W3c,W3d,GWtb_d,mt,Gt,W3)            ! t
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Wtb,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF


      CALL JIOXXX(W4,W2,GWF,MW,GW,W5)
      CALL IOVCXX(W1,W3,W5,GWtb_p,AMP(1))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W1,W3,W5,G2Wbt,-1,AMP(2))
      ELSE
        AMP(2)=0d0
      ENDIF

      BDB_TUB = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          BDB_TUB = BDB_TUB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


      REAL*8 FUNCTION BBUB_TBDB(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     FOR PROCESS : b~ u~ -> t~ d~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC
      REAL*8 x1,x2,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,Q,IDIR,IL

!     Couplings and other

      REAL*8 GWF(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 LMASS

C     COLOR DATA

      DATA EIGEN_VAL(1) /1d0/                  
      DATA EIGEN_VEC(1,1) /-1d0 /                  
      DATA EIGEN_VEC(2,1) /-1d0 /                  

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GWtb_p(1)=GWF(1)*VL            ! tbar production: t in b out
      GWtb_p(2)=GWF(1)*VR
      GWtb_d(1)=GWF(1)*CONJG(VL)     ! tbar decay: b in t out
      GWtb_d(2)=GWF(1)*CONJG(VR)

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      LMASS=0d0
      IF (IL .EQ. 3) LMASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL OXXXXX(P1,0d0,NHEL(1),-1,W1)        ! bbar
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)        ! ubar
      CALL IXXXXX(P4,0d0,NHEL(6),-1,W4)        ! dbar

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)               ! nu~
      CALL OXXXXX(P3b,LMASS,NHEL(4),1,W3b)              ! e-
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                ! W-
      CALL FVICXX(W3c,W3d,GWtb_d,mt,Gt,W3)                ! t~
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVISmX(W3c,W3d,G2Wbt,mt,Gt,-1,Wdum)          ! t~ anom
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL JIOXXX(W4,W2,GWF,MW,GW,W5)
      CALL IOVCXX(W3,W1,W5,GWtb_p,AMP(1))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W3,W1,W5,G2Wtb,1,AMP(2))
      ELSE
        AMP(2)=0d0
      ENDIF

      BBUB_TBDB = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          BBUB_TBDB = BBUB_TBDB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END




      REAL*8 FUNCTION BBD_TBU(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     FOR PROCESS : b~ d -> t~ u

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC
      REAL*8 x1,x2,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,Q,IDIR,IL

!     Couplings and other

      REAL*8 GWF(2)
      COMPLEX*16 GWtb_p(2),GWtb_d(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 LMASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /1d0/                  
      DATA EIGEN_VEC(1,1) /-1d0 /                  
      DATA EIGEN_VEC(2,1) /-1d0 /                  

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GWtb_p(1)=GWF(1)*VL            ! tbar production: t in b out
      GWtb_p(2)=GWF(1)*VR
      GWtb_d(1)=GWF(1)*CONJG(VL)     ! tbar decay: b in t out
      GWtb_d(2)=GWF(1)*CONJG(VR)

      G2Wtb(1)=GWF(1)/MW*gL          ! t in b out
      G2Wtb(2)=GWF(1)/MW*gR
      G2Wbt(1)=-GWF(1)/MW*CONJG(gR)  ! b in t out
      G2Wbt(2)=-GWF(1)/MW*CONJG(gL)

      LMASS=0d0
      IF (IL .EQ. 3) LMASS=mtau

      IF (IAN_PROD .EQ. 0) THEN
        GWtb_p(1)=GWF(1)
        GWtb_p(2)=0d0
      ENDIF
      IF (IAN_DEC .EQ. 0) THEN
        GWtb_d(1)=GWF(1)
        GWtb_d(2)=0d0
      ENDIF

!     Code

      CALL OXXXXX(P1,0d0,NHEL(1),-1,W1)       ! bbar
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)        ! d
      CALL OXXXXX(P4,0d0,NHEL(6),1,W4)        ! u

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)               ! nu~
      CALL OXXXXX(P3b,LMASS,NHEL(4),1,W3b)              ! e-
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                ! W-
      CALL FVICXX(W3c,W3d,GWtb_d,mt,Gt,W3)                ! t~
      IF (IAN_DEC .EQ. 1) THEN
        CALL FVISmX(W3c,W3d,G2Wbt,mt,Gt,-1,Wdum)          ! t~ anom
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL JIOXXX(W2,W4,GWF,MW,GW,W5)
      CALL IOVCXX(W3,W1,W5,GWtb_p,AMP(1))
      IF (IAN_PROD .EQ. 1) THEN
        CALL IOVSmX(W3,W1,W5,G2Wtb,1,AMP(2))
      ELSE
        AMP(2)=0d0
      ENDIF

      BBD_TBU = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          BBD_TBU = BBD_TBU + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

