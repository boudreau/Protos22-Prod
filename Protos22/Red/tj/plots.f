      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MW    mt  --  PTj  mtj   --    angles
      DATA varmin  / 50.,120.,0.,   0.,   0.,0.,-1.,-1.,-1.,-1.,-1.
c     &  ,-1.,-1.,-5.,-5./
     &  ,-1.,-1.,0.,0./
      DATA varmax  /110.,240.,1., 500.,2000.,1., 1., 1., 1., 1., 1.
     &  , 1., 1.,5.,5./
      DATA nbintot /  60,  60,10,  50,  100, 10, 40, 40, 40, 40, 40
c     &  , 40, 40,100,100/
     &  , 40, 40,50,50/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO
      
      
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

!     Set to zero dummy bins

      varsig(14,nbintot(14))=0d0
      varsig(15,nbintot(15))=0d0
      
      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      LOGICAL GOODASYM(n_a)
      REAL*8 rbl,rnl,rnb,drbl,drnl,drnb
      REAL*8 beta,Ap,Am,FL,F0,FR

      DATA asym_cent /0.,-0.5874,0.5874,0.,0.,0.,
     &  0.,-0.5874,0.5874,0.,-0.5874,0.5874/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      beta=2**(1d0/3d0)-1d0

      PRINT 1150,asym(1),err_asym(1)
      PRINT 1151,asym(2),err_asym(2)
      PRINT 1152,asym(3),err_asym(3)
      Ap=asym(2)
      Am=asym(3)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1180,FL,F0,FR

      PRINT 1170,asym(7),err_asym(7)
      PRINT 1171,asym(8),err_asym(8)
      PRINT 1172,asym(9),err_asym(9)
      Ap=asym(8)
      Am=asym(9)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1181,FL,F0,FR

      PRINT 1173,asym(10),err_asym(10)
      PRINT 1174,asym(11),err_asym(11)
      PRINT 1175,asym(12),err_asym(12)
      Ap=asym(11)
      Am=asym(12)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1182,FL,F0,FR

      PRINT 1153,asym(4),err_asym(4)
      PRINT 1154,asym(5),err_asym(5)
      PRINT 1155,asym(6),err_asym(6)
      rbl=asym(6)/asym(4)
      drbl=rbl*SQRT((err_asym(6)/asym(6))**2+(err_asym(4)/asym(4))**2)
      rnl=asym(5)/asym(4)
      drnl=rnl*SQRT((err_asym(5)/asym(5))**2+(err_asym(4)/asym(4))**2)
      rnb=asym(5)/asym(6)
      drnb=rnb*SQRT((err_asym(5)/asym(5))**2+(err_asym(6)/asym(6))**2)
      PRINT 1160,rbl,drbl
      PRINT 1161,rnl,drnl
      PRINT 1162,rnb,drnb
      PRINT 999
      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('A+  ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('A-  ',F8.5,' +- ',F8.5,' (MC)')
1153  FORMAT ('Al  ',F8.5,' +- ',F8.5,' (MC)')
1154  FORMAT ('An  ',F8.5,' +- ',F8.5,' (MC)')
1155  FORMAT ('Ab  ',F8.5,' +- ',F8.5,' (MC)')
1160  FORMAT ('rbl ',F8.5,' +- ',F8.5,' (MC)')
1161  FORMAT ('rnl ',F8.5,' +- ',F8.5,' (MC)')
1162  FORMAT ('rnb ',F8.5,' +- ',F8.5,' (MC)')
1170  FORMAT ('AFBT ',F8.5,' +- ',F8.5,' (MC)')
1171  FORMAT ('AT+  ',F8.5,' +- ',F8.5,' (MC)')
1172  FORMAT ('AT-  ',F8.5,' +- ',F8.5,' (MC)')
1173  FORMAT ('AFBN ',F8.5,' +- ',F8.5,' (MC)')
1174  FORMAT ('AN+  ',F8.5,' +- ',F8.5,' (MC)')
1175  FORMAT ('AN-  ',F8.5,' +- ',F8.5,' (MC)')
1180  FORMAT ('--->  F- ~ ',F8.5,'   F0 ~ ',F8.5,'   F+ ~ ',F8.5)
1181  FORMAT ('--->  FT- ~ ',F8.5,'   FT0 ~ ',F8.5,'   FT+ ~ ',F8.5)
1182  FORMAT ('--->  FN- ~ ',F8.5,'   FN0 ~ ',F8.5,'   FN+ ~ ',F8.5)
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 COSVEC,DOT

!     External momenta

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pj(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pj
      REAL*8 Pt(0:3),PW(0:3)
      COMMON /MOMINT/ Pt,PW

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     top or antitop?

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Local kinematical quantities

      REAL*8 PTj,cospsi,cos_lW,cos_lj,cos_nj,cos_bj
      REAL*8 Ptrec(0:3),PWrec(0:3),PjR(0:3),Ptot(0:3)
      REAL*8 PWCM(0:3),PlCM(0:3),PbCM(0:3)
      REAL*8 Pboo(0:3),tspin(0:3),VECN(0:3),VECT(0:3),norm
      REAL*8 Et,PLt,eta

!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3),pdum2(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

      CALL SUMVEC(Pl,Pn,PWrec)
      var(1)=SQRT(DOT(PWrec,PWrec))
      CALL SUMVEC(PW,Pb,Ptrec)
      var(2)=SQRT(DOT(Ptrec,Ptrec))
      
      PTj=SQRT(Pj(1)**2+Pj(2)**2)
      var(4)=PTj
      
      CALL SUMVEC(Ptrec,Pj,Ptot)
      var(5)=SQRT(DOT(Ptot,Ptot))

      cospsi=COSVEC(Pj,Q1)
      var(7)=cospsi

!     lW distribution

      Pboo(0)=Ptrec(0)                   ! LAB to top rest frame
      DO i=1,3
        Pboo(i)=-Ptrec(i)
      ENDDO
      CALL BOOST(Pboo,PWrec,PWCM)       ! W in CM
      CALL BOOST(Pboo,Pl,PlCM)          ! l in CM
      CALL BOOST(Pboo,Pb,PbCM)          ! b in CM
      CALL BOOST(Pboo,Pj,tspin)         ! top spin direction

      Pboo(0)=PWCM(0)                   ! top to W rest frame
      DO i=1,3
        Pboo(i)=-PWCM(i)
      ENDDO
      CALL BOOST(Pboo,PlCM,Pdum1)       ! l in W rest frame
      CALL BOOST(Pboo,PbCM,Pdum2)       ! b in W rest frame

      cos_lW=COSVEC(pdum1,PWCM)
      quant(1)=cos_lW                 ! A_FB
      quant(2)=cos_lW                 ! A+
      quant(3)=cos_lW                 ! A-
      var(8)=cos_lW

!     Normal

      norm=SQRT(tspin(1)**2+tspin(2)**2+tspin(3)**2)
      DO i=1,3
      tspin(i)=tspin(i)/norm
      ENDDO
      tspin(0)=0d0

      CALL CROSSVEC(tspin,PWCM,VECN)
      cos_lW=COSVEC(pdum1,VECN)
      quant(10)=cos_lW                 ! A_FB
      quant(11)=cos_lW                 ! A+
      quant(12)=cos_lW                 ! A-
      var(13)=cos_lW

!     Transverse

      CALL CROSSVEC(PWCM,VECN,VECT)
      cos_lW=COSVEC(pdum1,VECT)
      quant(7)=cos_lW                 ! A_FB
      quant(8)=cos_lW                 ! A+
      quant(9)=cos_lW                 ! A-
      var(12)=cos_lW

!     t rest frame distributions

      Pboo(0)=Ptrec(0)                ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-Ptrec(i)
      ENDDO

      CALL BOOST(Pboo,Pj,PjR)       ! jet direction in t rest frame

      CALL BOOST(Pboo,Pl,pdum1)
      cos_lj=COSVEC(PjR,pdum1)      ! A_l
      CALL BOOST(Pboo,Pn,pdum1)
      cos_nj=COSVEC(PjR,pdum1)      ! A_n
      CALL BOOST(Pboo,Pb,pdum1)
      cos_bj=COSVEC(PjR,pdum1)      ! A_b
      quant(4)=cos_lj
      quant(5)=cos_nj
      quant(6)=cos_bj
      var(9)=cos_lj
      var(10)=cos_nj
      var(11)=cos_bj

!     Top rapidity

      Et=Ptrec(0)
      PLt=Ptrec(3)
      eta=1d0/2d0*LOG((Et+PLt)/(Et-PLt))

      IF (MOD((IPROC-1)/4,2) .EQ. 0) THEN   ! top
c        var(14)=eta
        var(14)=ABS(eta)
        var(15)=15d0
      ELSE
        var(14)=15d0
c        var(15)=eta
        var(15)=ABS(eta)
      ENDIF

      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







