      REAL*8 FUNCTION GG_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     PROCESS : g g  -> t t~  with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      COMPLEX*16 COUP_T(4),COUP_TB(4)
      COMMON /tcoup3/ COUP_T,COUP_TB
      REAL*8 Clqp,CLL(2),CRR(2),CLR(2),CRL(2)
      COMMON /COUP4F/ Clqp,CLL,CRR,CLR,CRL
      INTEGER Id1,Id2,IQi
      COMMON /tflav/ Id1,Id2,IQi

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      INTEGER IAN_T,IAN_TB
      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GWtb_tb(2),GWtb_bt(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      REAL*8 F1MASS,F2MASS
      REAL*8 PL(2),PR(2)
      REAL*8 md1,md2
 
!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GWtb_tb(1)=GWF(1)*COUP_T(1)             ! t in b out: top decay
      GWtb_tb(2)=GWF(1)*COUP_T(2)
      GWtb_bt(1)=GWF(1)*CONJG(COUP_TB(1))     ! b in t out: antitop decay
      GWtb_bt(2)=GWF(1)*CONJG(COUP_TB(2))

      G2Wtb(1)=GWF(1)/MW*COUP_T(3)            ! t in b out: top decay
      G2Wtb(2)=GWF(1)/MW*COUP_T(4)
      G2Wbt(1)=-GWF(1)/MW*CONJG(COUP_TB(4))   ! b in t out: antitop decay
      G2Wbt(2)=-GWF(1)/MW*CONJG(COUP_TB(3))

!     Four-fermion projectors

      PL(1) = 1d0
      PL(2) = 0d0
      PR(1) = 0d0
      PR(2) = 1d0

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

      md1=mb
      IF (Id1 .NE. 3) md1=0d0
      md2=mb
      IF (Id2 .NE. 3) md2=0d0

      IAN_T=1
      IAN_TB=1
      IF (ABS(COUP_T(3))**2+ABS(COUP_T(4))**2 .EQ. 0d0) IAN_T=0
      IF (ABS(COUP_TB(3))**2+ABS(COUP_TB(4))**2 .EQ. 0d0) IAN_TB=0

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                            
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)                            

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)              ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+ / qbar
      CALL OXXXXX(P3c,md1,NHEL(5),1,W3c)               ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOCXX(W3c,W3d,GWtb_tb,mt,Gt,W3)           ! t
      IF (IAN_T .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Wtb,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF
      IF ((IF1 .LE. 3) .AND. (Clqp .NE. 0d0)) THEN
      CALL FVOIOX(W3a,W3b,W3c,PL,PL,mt,Gt,Wdum)       !     Four-fermion
c      W3(1)=-Wdum(1)*2d0*Cqqp/1d6
c      W3(2)=-Wdum(2)*2d0*Cqqp/1d6
c      W3(3)=-Wdum(3)*2d0*Cqqp/1d6
c      W3(4)=-Wdum(4)*2d0*Cqqp/1d6
      W3(1)=W3(1)-Wdum(1)*2d0*Clqp/1d6
      W3(2)=W3(2)-Wdum(2)*2d0*Clqp/1d6
      W3(3)=W3(3)-Wdum(3)*2d0*Clqp/1d6
      W3(4)=W3(4)-Wdum(4)*2d0*Clqp/1d6
      ENDIF

      CALL IXXXXX(P4a,0d0,NHEL(6),-1,W4a)                 ! nu~ / qbar
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)               ! e-  / q
      CALL IXXXXX(P4c,md2,NHEL(8),-1,W4c)                  ! b~
      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4d)                  ! W-
      CALL FVICXX(W4c,W4d,GWtb_bt,mt,Gt,W4)               ! t~
      IF (IAN_TB .EQ. 1) THEN
        CALL FVISmX(W4c,W4d,G2Wbt,mt,Gt,-1,Wdum)          ! t~ anomalous
        W4(1)=W4(1)+Wdum(1)
        W4(2)=W4(2)+Wdum(2)
        W4(3)=W4(3)+Wdum(3)
        W4(4)=W4(4)+Wdum(4)
      ENDIF
      IF ((IF2 .LE. 3) .AND. (Clqp .NE. 0d0)) THEN
      CALL FVIIOX(W4a,W4c,W4b,PL,PL,mt,Gt,Wdum)       !     Four-fermion
c      W4(1)=-Wdum(1)*2d0*Clqp/1d6
c      W4(2)=-Wdum(2)*2d0*Clqp/1d6
c      W4(3)=-Wdum(3)*2d0*Clqp/1d6
c      W4(4)=-Wdum(4)*2d0*Clqp/1d6
      W4(1)=W4(1)-Wdum(1)*2d0*Clqp/1d6
      W4(2)=W4(2)-Wdum(2)*2d0*Clqp/1d6
      W4(3)=W4(3)-Wdum(3)*2d0*Clqp/1d6
      W4(4)=W4(4)-Wdum(4)*2d0*Clqp/1d6
      ENDIF


      CALL FVOXXX(W3,W2,GG,mt,Gt,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mt,Gt,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_TT = GG_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_TT = GG_TT/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END




      REAL*8 FUNCTION UU_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> t t~    with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      COMPLEX*16 COUP_T(4),COUP_TB(4)
      COMMON /tcoup3/ COUP_T,COUP_TB
      REAL*8 Clqp,CLL(2),CRR(2),CLR(2),CRL(2)
      COMMON /COUP4F/ Clqp,CLL,CRR,CLR,CRL
      INTEGER Id1,Id2,IQi
      COMMON /tflav/ Id1,Id2,IQi

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      INTEGER IAN_T,IAN_TB
      REAL*8 GWF(2),GG(2)
      REAL*8 gLL,gRR,gLR,gRL
      COMPLEX*16 GWtb_tb(2),GWtb_bt(2),G2Wtb(2),G2Wbt(2)
      COMPLEX*16 Wdum(6)
      COMPLEX*16 A4LL,A4RR,A4RL,A4LR
      REAL*8 F1MASS,F2MASS
      REAL*8 PL(2),PR(2)
      REAL*8 md1,md2

!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GWtb_tb(1)=GWF(1)*COUP_T(1)             ! t in b out: top decay
      GWtb_tb(2)=GWF(1)*COUP_T(2)
      GWtb_bt(1)=GWF(1)*CONJG(COUP_TB(1))     ! b in t out: antitop decay
      GWtb_bt(2)=GWF(1)*CONJG(COUP_TB(2))

      G2Wtb(1)=GWF(1)/MW*COUP_T(3)            ! t in b out: top decay
      G2Wtb(2)=GWF(1)/MW*COUP_T(4)
      G2Wbt(1)=-GWF(1)/MW*CONJG(COUP_TB(4))   ! b in t out: antitop decay
      G2Wbt(2)=-GWF(1)/MW*CONJG(COUP_TB(3))

!     Four-fermion projectors

      PL(1) = 1d0
      PL(2) = 0d0
      PR(1) = 0d0
      PR(2) = 1d0

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

      md1=mb
      IF (Id1 .NE. 3) md1=0d0
      md2=mb
      IF (Id2 .NE. 3) md2=0d0

      IF (IQi .EQ. 0) THEN
        gLL=0d0
        gRR=0d0
        gLR=0d0
        gRL=0d0
      ELSE
        gLL=CLL(IQi)
        gRR=CRR(IQi)
        gLR=-CLR(IQi)      ! Minus because of Fierz on Cqu
        gRL=-CRL(IQi)      ! The same
      ENDIF

      IAN_T=1
      IAN_TB=1
      IF (ABS(COUP_T(3))**2+ABS(COUP_T(4))**2 .EQ. 0d0) IAN_T=0
      IF (ABS(COUP_TB(3))**2+ABS(COUP_TB(4))**2 .EQ. 0d0) IAN_TB=0
      
!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)                       
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                       

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)              ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+ / qbar
      CALL OXXXXX(P3c,md1,NHEL(5),1,W3c)               ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOCXX(W3c,W3d,GWtb_tb,mt,Gt,W3)           ! t
      IF (IAN_T .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Wtb,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF
      IF ((IF1 .LE. 3) .AND. (Clqp .NE. 0d0)) THEN
      CALL FVOIOX(W3a,W3b,W3c,PL,PL,mt,Gt,Wdum)       !     Four-fermion
c      W3(1)=-Wdum(1)*2d0*Cqqp/1d6
c      W3(2)=-Wdum(2)*2d0*Cqqp/1d6
c      W3(3)=-Wdum(3)*2d0*Cqqp/1d6
c      W3(4)=-Wdum(4)*2d0*Cqqp/1d6
      W3(1)=W3(1)-Wdum(1)*2d0*Clqp/1d6
      W3(2)=W3(2)-Wdum(2)*2d0*Clqp/1d6
      W3(3)=W3(3)-Wdum(3)*2d0*Clqp/1d6
      W3(4)=W3(4)-Wdum(4)*2d0*Clqp/1d6
      ENDIF
      
      CALL IXXXXX(P4a,0d0,NHEL(6),-1,W4a)                 ! nu~ / qbar
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)               ! e-  / q
      CALL IXXXXX(P4c,md2,NHEL(8),-1,W4c)                  ! b~
      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4d)                  ! W-
      CALL FVICXX(W4c,W4d,GWtb_bt,mt,Gt,W4)               ! t~
      IF (IAN_TB .EQ. 1) THEN
        CALL FVISmX(W4c,W4d,G2Wbt,mt,Gt,-1,Wdum)          ! t~ anomalous
        W4(1)=W4(1)+Wdum(1)
        W4(2)=W4(2)+Wdum(2)
        W4(3)=W4(3)+Wdum(3)
        W4(4)=W4(4)+Wdum(4)
      ENDIF
      IF ((IF2 .LE. 3) .AND. (Clqp .NE. 0d0)) THEN
      CALL FVIIOX(W4a,W4c,W4b,PL,PL,mt,Gt,Wdum)       !     Four-fermion in decay
c      W4(1)=-Wdum(1)*2d0*Clqp/1d6
c      W4(2)=-Wdum(2)*2d0*Clqp/1d6
c      W4(3)=-Wdum(3)*2d0*Clqp/1d6
c      W4(4)=-Wdum(4)*2d0*Clqp/1d6
      W4(1)=W4(1)-Wdum(1)*2d0*Clqp/1d6
      W4(2)=W4(2)-Wdum(2)*2d0*Clqp/1d6
      W4(3)=W4(3)-Wdum(3)*2d0*Clqp/1d6
      W4(4)=W4(4)-Wdum(4)*2d0*Clqp/1d6
      ENDIF

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))
      IF (CLL(IQi) .NE. 0d0) CALL IOIOVX(W1,W3,W4,W2,PR,PR,A4LL)  ! (t_L u_L) (u_L t_L)
      IF (CRR(IQi) .NE. 0d0) CALL IOIOVX(W1,W3,W4,W2,PL,PL,A4RR)  ! (t_R u_R) (u_R t_R)
      IF (CLR(IQi) .NE. 0d0) CALL IOIOVX(W1,W2,W4,W3,PL,PR,A4LR)  ! (u_L u_L) (t_R t_R)
      IF (CRL(IQi) .NE. 0d0) CALL IOIOVX(W1,W2,W4,W3,PR,PL,A4RL)  ! (u_R u_R) (t_L t_L)

      AMP(1) = AMP(1)-(A4LL*1d0*gLL+A4RR*1d0*gRR)/1d6       ! -1, fermion exchange
      AMP(1) = AMP(1)+(A4LR*1d0*gLR+A4RL*1d0*gRL)/1d6
      CAMP(1) = -AMP(1)

      UU_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_TT = UU_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_TT = UU_TT/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END

