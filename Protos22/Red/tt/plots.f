      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MW1  mt1  MW1  mt2  mtt   PTt  angles
      DATA varmin  /  0.,120.,  0.,120., 200.,  0.,-1.,-1.,-1.,-1.,-1.
     &  ,-1./
      DATA varmax  /160.,240.,160.,240.,2000.,600., 1., 1., 1.,1. , 1.
     &  , 1./
      DATA nbintot / 80.,  60,  80,  60,   90,  60, 40, 40, 40, 40, 40
     &  , 40/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      LOGICAL GOODASYM(n_a)
      REAL*8 rbl,rblt,drbl,drblt
      REAL*8 beta,Ap,Am,FL,F0,FR

      DATA asym_cent /0.,-0.5874,0.5874,0.,-0.5874,0.5874,0.,0.,0.,
     &  0.,0.,0./

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      beta=2**(1d0/3d0)-1d0

      PRINT 1150,asym(1),err_asym(1)
      PRINT 1151,asym(2),err_asym(2)
      PRINT 1152,asym(3),err_asym(3)

      Ap=asym(2)
      Am=asym(3)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1180,FL,F0,FR

      PRINT 1153,asym(4),err_asym(4)
      PRINT 1154,asym(5),err_asym(5)
      PRINT 1155,asym(6),err_asym(6)

      Ap=asym(5)
      Am=asym(6)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1180,FL,F0,FR

      PRINT 1156,asym(7),err_asym(7)
      PRINT 1157,asym(8),err_asym(8)
      PRINT 1158,asym(9),err_asym(9)
      PRINT 1159,asym(10),err_asym(10)
      PRINT 1160,asym(11),err_asym(11)
      PRINT 1161,asym(12),err_asym(12)

      rbl=asym(11)/asym(7)
      drbl=rbl*SQRT((err_asym(11)/asym(11))**2
     &  +(err_asym(7)/asym(7))**2)
      rblt=asym(12)/asym(8)
      drblt=rbl*SQRT((err_asym(12)/asym(12))**2
     &  +(err_asym(8)/asym(8))**2)
      PRINT 1170,rbl,drbl
      PRINT 1171,rblt,drblt

      PRINT 999
      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB l  ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('A+  l  ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('A-  l  ',F8.5,' +- ',F8.5,' (MC)')
1153  FORMAT ('AFB ql ',F8.5,' +- ',F8.5,' (MC)')
1154  FORMAT ('A+  ql ',F8.5,' +- ',F8.5,' (MC)')
1155  FORMAT ('A-  ql ',F8.5,' +- ',F8.5,' (MC)')
1156  FORMAT ('All    ',F8.5,' +- ',F8.5,' (MC)')
1157  FORMAT ('All~   ',F8.5,' +- ',F8.5,' (MC)')
1158  FORMAT ('Alj    ',F8.5,' +- ',F8.5,' (MC)')
1159  FORMAT ('Alj~   ',F8.5,' +- ',F8.5,' (MC)')
1160  FORMAT ('Alb    ',F8.5,' +- ',F8.5,' (MC)')
1161  FORMAT ('Alb~   ',F8.5,' +- ',F8.5,' (MC)')
1170  FORMAT ('rbl    ',F8.5,' +- ',F8.5,' (MC)')
1171  FORMAT ('rbl~   ',F8.5,' +- ',F8.5,' (MC)')
1180  FORMAT ('--->  FL ~ ',F8.5,'   F0 ~ ',F8.5,'   FR ~ ',F8.5)
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 COSVEC,DOT

!     Variables needed

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3),P5(0:3),P6(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4,P5,P6
      REAL*8 Pt1(0:3),Pt2(0:3),PW1(0:3),PW2(0:3)
      COMMON /MOMINT/ Pt1,Pt2,PW1,PW2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     Local kinematical quantities

      INTEGER IT1LEP
      REAL*8 PW1rec(0:3),PW2rec(0:3),Pt1rec(0:3),Pt2rec(0:3)
      REAL*8 Pt1Z(0:3),Pt2Z(0:3)
      REAL*8 Pl1Z(0:3),Pl2Z(0:3),Pl1R(0:3),Pl2R(0:3)
      REAL*8 Pj1Z(0:3),Pj2Z(0:3),Pj1R(0:3),Pj2R(0:3)
      REAL*8 Pb2Z(0:3),Pb2R(0:3)
      REAL*8 Ptot(0:3)
      REAL*8 Pl(0:3),Pj(0:3),Pb2(0:3)
      REAL*8 cos_lW1,cos_lW2,cos_lW,cos_lqW,cos_l1,cos_l2
      REAL*8 cos_l,cos_j,cos_b2
      REAL*8 cospsi,PTT
      REAL*8 MW1,MW2,mt1,mt2

c      REAL*8 Ptlep1Z(0:3),Ptlep2Z(0:3),Pnu1R(0:3),Plep1R(0:3),Pb1R(0:3)
c      REAL*8 Plep2R(0:3),Pb2R(0:3)
c      REAL*8 cos_nu1,cos_l1l2,cos_nu1l2
      
      
      
!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3),pdum2(0:3)
      REAL*8 Pboo(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

!     Select leptonic and hadronic top

      IF ((IF1 .LE. 3) .AND. (IF2 .GT. 3)) THEN
        IT1LEP=1
      ELSE IF ((IF1 .GT. 3) .AND. (IF2 .LE. 3)) THEN
        IT1LEP=0
      ELSE
        IT1LEP=1         ! Dilepton and full hadronic channel ordered as t, tbar
      ENDIF

      CALL SUMVEC(P1,P2,PW1rec)
      MW1=SQRT(DOT(PW1rec,PW1rec))
      CALL SUMVEC(PW1rec,P3,Pt1rec)
      mt1=SQRT(DOT(Pt1rec,Pt1rec))
      CALL SUMVEC(P4,P5,PW2rec)
      MW2=SQRT(DOT(PW2rec,PW2rec))
      CALL SUMVEC(PW2rec,P6,Pt2rec)
      mt2=SQRT(DOT(Pt2rec,Pt2rec))
      IF (IT1LEP .EQ. 1) THEN
        var(1)=MW1
        var(2)=mt1
        var(3)=MW2
        var(4)=mt2
      ELSE
        var(1)=MW2
        var(2)=mt2
        var(3)=MW1
        var(4)=mt1
      ENDIF
      
      CALL SUMVEC(Pt1rec,Pt2rec,Ptot)
      var(5)=SQRT(DOT(Ptot,Ptot))

      PTt=SQRT(Pt1rec(1)**2+Pt1rec(2)**2)
      var(6)=PTt
      
      cospsi=COSVEC(Pt1rec,Q1)
      var(7)=cospsi


!     lW distribution for top

      Pboo(0)=PW1(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PW1(i)
      ENDDO
      CALL BOOST(Pboo,P2,pdum1)
      CALL BOOST(Pboo,P3,pdum2)
      cos_lW1=-COSVEC(pdum1,pdum2)

!     lW distribution for antitop

      Pboo(0)=PW2(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PW2(i)
      ENDDO
      CALL BOOST(Pboo,P5,pdum1)
      CALL BOOST(Pboo,P6,pdum2)
      cos_lW2=-COSVEC(pdum1,pdum2)

      IF (IT1LEP .EQ. 1) THEN
      cos_lW=cos_lW1
      cos_lqW=cos_lW2
      ELSE
      cos_lW=cos_lW2
      cos_lqW=cos_lW1
      ENDIF

      quant(1)=cos_lW                 ! A_FB
      quant(2)=cos_lW                 ! A+
      quant(3)=cos_lW                 ! A-
      var(8)=cos_lW

      quant(4)=cos_lqW                 ! A_FB
      quant(5)=cos_lqW                 ! A+
      quant(6)=cos_lqW                 ! A-
      var(9)=cos_lqW

!     Momenta in ZMF

      Pboo(0)=Pt1(0)+Pt2(0)                     ! LAB to t t~ CM system
      DO i=1,3
        Pboo(i)=-Pt1(i)-Pt2(i)
      ENDDO

      CALL BOOST(Pboo,Pt1,Pt1Z)
      CALL BOOST(Pboo,Pt2,Pt2Z)
      CALL BOOST(Pboo,P2,Pl1Z)
      CALL BOOST(Pboo,P5,Pl2Z)

      IF (IT1LEP .EQ. 1) THEN
        CALL BOOST(Pboo,P4,Pj1Z)
        CALL BOOST(Pboo,P5,Pj2Z)
        CALL BOOST(Pboo,P6,Pb2Z)
      ELSE
        CALL BOOST(Pboo,P1,Pj1Z)
        CALL BOOST(Pboo,P2,Pj2Z)
        CALL BOOST(Pboo,P3,Pb2Z)
      ENDIF

!     Distributions in t rest frame

      Pboo(0)=Pt1Z(0)                         ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-Pt1Z(i)
      ENDDO

      CALL BOOST(Pboo,Pl1Z,Pl1R)
      cos_l1=COSVEC(Pl1R,Pt1Z)

      IF (IT1LEP .EQ. 1) THEN
        CALL STORE(Pl1R,Pl)
        cos_l=cos_l1
      ELSE
        CALL BOOST(Pboo,Pj1Z,Pj1R)
        CALL BOOST(Pboo,Pj2Z,Pj2R)
        IF (Pj1R(0) .LT. Pj2R(0)) THEN
          CALL STORE(Pj1R,Pj)
        ELSE
          CALL STORE(Pj2R,Pj)
        ENDIF
        cos_j=COSVEC(Pj,Pt1Z)
        CALL BOOST(Pboo,Pb2Z,Pb2R)
        CALL STORE(Pb2R,Pb2)
        cos_b2=COSVEC(Pb2,Pt1Z)
      ENDIF

!     Distributions in tbar rest frame

      Pboo(0)=Pt2Z(0)                         ! LAB to t rest frame
      DO i=1,3
        Pboo(i)=-Pt2Z(i)
      ENDDO

      CALL BOOST(Pboo,Pl2Z,Pl2R)
      cos_l2=COSVEC(Pl2R,Pt2Z)

      IF (IT1LEP .EQ. 1) THEN
        CALL BOOST(Pboo,Pj1Z,Pj1R)
        CALL BOOST(Pboo,Pj2Z,Pj2R)
        IF (Pj1R(0) .LT. Pj2R(0)) THEN
          CALL STORE(Pj1R,Pj)
        ELSE
          CALL STORE(Pj2R,Pj)
        ENDIF
        cos_j=COSVEC(Pj,Pt2Z)
        CALL BOOST(Pboo,Pb2Z,Pb2R)
        CALL STORE(Pb2R,Pb2)
        cos_b2=COSVEC(Pb2,Pt2Z)
      ELSE
        CALL STORE(Pl2R,Pl)
        cos_l=cos_l2
      ENDIF


      quant(7)=cos_l1*cos_l2                ! All
      quant(8)=COSVEC(Pl1R,Pl2R)            ! All~
      quant(9)=cos_l*cos_j                  ! Alj
      quant(10)=COSVEC(Pl,Pj)               ! Alj~
      quant(11)=cos_l*cos_b2                ! Alb
      quant(12)=COSVEC(Pl,Pb2)              ! Alb~

      var(10)=COSVEC(Pl1R,Pl2R)
      var(11)=COSVEC(Pl,Pj)
      var(12)=COSVEC(Pl,Pb2)

      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







