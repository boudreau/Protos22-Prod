      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

10    CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pj,PbX
      REAL*8 Pt(0:3),PW(0:3)
      COMMON /MOMINT/ Pt,PW
      REAL*8 x1,x2,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,Q,IDIR,IL

!     Local variables

      REAL*8 EPS,y1,y2,flux,s
      REAL*8 PCM_LAB(0:3)
      REAL*8 Qt,QW,LMASS
      REAL*8 WT1,WT2,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC

!     --------------------------------------------

      WTPS=0d0

!     Generation of the masses of the virtual t, W
      
      CALL BREIT(mt,Gt,NGt,Qt,WT1)
      CALL BREIT(MW,GW,NGW,QW,WT2)
      IF (Qt .LT. QW+mb) RETURN
      WT_BREIT=WT1*WT2*(2d0*pi)**6

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(Qt+mb)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system
      
      Q1(0)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(3)=ET*x1/2d0
              
      Q2(0)=ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(3)=-ET*x2/2d0

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

      IDIR=0
      IF (IPROC .GT. 8) IDIR=1

!     Select fermion from W decay
  
      IF ((ILEP .GE. 1) .AND. (ILEP .LE. 3)) THEN   ! ILEP = 1,2,3: sel W -> e, mu, tau
        IL=ILEP
      ELSE IF (ILEP .EQ. 4) THEN                    ! ILEP = 4: sum W -> e,mu,tau
        y1=RAN2(idum)
        IF (y1 .LT. 1d0/3d0) THEN
          IL=1
        ELSE IF (y1 .LT. 2d0/3d0) THEN
          IL=2
        ELSE
          IL=3
        ENDIF
      ENDIF

      LMASS=0
      IF (IL .EQ. 3) LMASS=mtau
      
!     Generation

      CALL PHASE3tsym(SQRT(s),Qt,mb,0d0,PCM_LAB,Pt,PbX,Pj,WT,IDIR)
      WT_PROD=(2d0*pi)**4*WT

      CALL PHASE2(Qt,QW,mb,Pt,PW,Pb,WT)                     ! t decay
      WT_DEC=WT

      CALL PHASE2(QW,0d0,LMASS,PW,Pn,Pl,WT)                 ! W decay
      WT_DEC=WT_DEC*WT

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pj,PbX
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      REAL*8 PTbmin
      INTEGER IMATCH
      COMMON /MATCH/ PTbmin,IMATCH

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local variables

      REAL*8 PTbX

!     --------------------------------------------

      INOT=1
      
      PTbX=SQRT(PbX(1)**2+PbX(2)**2)
      IF ((IMATCH .EQ. 2) .AND. (PTbX .LT. PTbmin)) RETURN

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 DOT,GU_TBBD,GDB_TBBUB,GUB_TBBDB,GD_TBBU

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pj,PbX
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,Q,IDIR,IL
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC1,QFAC2
      COMMON /PDFSCALE/ QFAC1,QFAC2

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      REAL*8 Ql1,Ql2,Qg
      INTEGER k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

      Qg=SQRT(PbX(1)**2+PbX(2)**2+mb**2)
      CALL SETALPHAS(Qg)
      Qg=Qg*QFAC2

      Ql1=SQRT(2d0*DOT(Q1,Pj))*QFAC1
      Ql2=SQRT(2d0*DOT(Q2,Pj))*QFAC1
      Q=Ql1
      IF (IDIR .EQ. 1) Q=Ql2
      
      CALL GETPDF_tbj(x1,Ql1,Qg,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF_tbj(x2,Ql2,Qg,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,
     &  ISTAT)
      IF (ISTAT .LT. 0) RETURN

!     q g

      STR(1)=fu1*fg2          ! u  g 
      STR(2)=fc1*fg2          ! c  g
      STR(3)=fds1*fg2         ! d~ g
      STR(4)=fs1*fg2          ! s~ g
      STR(5)=fus1*fg2         ! u~ g
      STR(6)=fc1*fg2          ! c~ g
      STR(7)=fd1*fg2          ! d  g
      STR(8)=fs1*fg2          ! s  g

!     g q

      IF (IPPBAR .EQ. 0) THEN
      STR(9)=fg1*fu2          ! g  u
      STR(10)=fg1*fc2         ! g  c
      STR(11)=fg1*fds2        ! g  d~
      STR(12)=fg1*fs2         ! g  s~
      STR(13)=fg1*fus2        ! g  u~
      STR(14)=fg1*fc2         ! g  c~
      STR(15)=fg1*fd2         ! g  d
      STR(16)=fg1*fs2         ! g  s
      ELSE
      STR(9)=fg1*fus2         ! g  u
      STR(10)=fg1*fc2         ! g  c
      STR(11)=fg1*fd2         ! g  d~
      STR(12)=fg1*fs2         ! g  s~
      STR(13)=fg1*fu2         ! g  u~
      STR(14)=fg1*fc2         ! g  c~
      STR(15)=fg1*fds2        ! g  d
      STR(16)=fg1*fs2         ! g  s
      ENDIF

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IL .EQ. 3) IAMP=IAMP+NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 100) ntry(IAMP) = 100

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF ((IPROC .NE. 1) .AND. (IPROC .NE. 2)
     &  .AND. (IPROC .NE. 9) .AND. (IPROC .NE. 10)) GOTO 12

!     u g   c g   g u   g c  

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GU_TBBD(Q2,Q1,Pn,Pl,Pb,PbX,Pj,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20


12    IF ((IPROC .NE. 3) .AND. (IPROC .NE. 4)
     &  .AND. (IPROC .NE. 11) .AND. (IPROC .NE. 12)) GOTO 14

!     d~ g   s~ g   g d~   g s~  

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GDB_TBBUB(Q2,Q1,Pn,Pl,Pb,PbX,Pj,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

14    IF ((IPROC .NE. 5) .AND. (IPROC .NE. 6)
     &  .AND. (IPROC .NE. 13) .AND. (IPROC .NE. 14)) GOTO 16

!     u~ g   c~ g   g u~   g c~  

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GUB_TBBDB(Q2,Q1,Pn,Pl,Pb,PbX,Pj,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

16    CONTINUE

!     d g   s g   g d   g s  

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 10) THEN
          M1=GD_TBBU(Q2,Q1,Pn,Pl,Pb,PbX,Pj,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END

