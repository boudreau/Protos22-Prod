      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,PbX
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IL
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IL
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_t,ERR_t,SIG_tbar,ERR_tbar
      COMMON /FINALSTATS/ SIG_t,ERR_t,SIG_tbar,ERR_tbar

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables

      INTEGER IDTAB1(4),IDTAB2(4),IDTAB5(4),IDTAB6(4)
      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2
      INTEGER i

      DATA IDTAB1 /2,4,-2,-4/
      DATA IDTAB2 /-1,-3,1,3/
      DATA IDTAB5 /5,5,-5,-5/
      DATA IDTAB6 /-5,-5,5,5/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0
      
      RETURN

10    IF (IMODE .NE. 0) GOTO 11
      
!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)                          ! Already in pb
      IF (XWGTUP .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,XWGTUP)
      NUMEVT=NUMEVT+1

!     Flavour

      ID(1)=IDTAB1(MOD(IPROC,5)+IPROC/5)
      ID(2)=IDTAB2(MOD(IPROC,5)+IPROC/5)
      ID(5)=IDTAB5(MOD(IPROC,5)+IPROC/5)
      ID(6)=IDTAB6(MOD(IPROC,5)+IPROC/5)
      IF (IL .EQ. 1) THEN
        ID(3)=12
        ID(4)=-11
      ELSE IF (IL .EQ. 2) THEN
        ID(3)=14
        ID(4)=-13
      ELSE IF (IL .EQ. 3) THEN
        ID(3)=16
        ID(4)=-15
      ELSE
        PRINT *,'Wrong IL'
        STOP
      ENDIF
      IF (ID(5) .LT. 0) THEN
        ID(3)=-ID(3)
        ID(4)=-ID(4)
      ENDIF

!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

      DO i=1,2
        IF (ID(i) .GT. 0) THEN         ! Initial q / qbar and b / bbar
           ICOL1(i)=1
        ELSE
           ICOL2(i)=1
        ENDIF
      ENDDO

      DO i=5,6
        IF (ID(i) .GT. 0) THEN         ! Initial q / qbar and b / bbar
           ICOL1(i)=2
        ELSE
           ICOL2(i)=2
        ENDIF
      ENDDO

      IF (IDIR .EQ. 0) THEN
        Pz1=Q1(3)
        Pz2=Q2(3)
      ELSE
        Pz1=Q2(3)
        Pz2=Q1(3)
      ENDIF
      
      WRITE (35,3010) NUMEVT,XWGTUP,Q
      IF (Pz1 .GT. 0d0) THEN
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      ELSE
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      ENDIF
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),Pn(1),Pn(2),Pn(3)
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),Pl(1),Pl(2),Pl(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pb(1),Pb(2),Pb(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),PbX(1),PbX(2),PbX(3)
      RETURN

      
      
11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 5
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4010) DREAL(vl),DREAL(vr),DREAL(gl),DREAL(gr)
      WRITE (36,4011) DIMAG(vl),DIMAG(vr),DIMAG(gl),DIMAG(gr)
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_t,ERR_t
      WRITE (36,4050) SIG_tbar,ERR_tbar
      CLOSE (36)
      RETURN



12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)
   
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4010  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Re VL, Re VR, Re gL, Re gR')
4011  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Im VL, Im VR, Im gL, Im gR')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! t cross section and error')
4050  FORMAT (D12.6,'   ',D12.6,
     & '      ! tbar cross section and error')
      END

