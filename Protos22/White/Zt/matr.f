      REAL*8 FUNCTION GQ_TZ(P1,P2,P3a,P3b,P3c,P4a,P4b,NHEL)

!     P1 = g    P2 = q    P3 = t    P4 = Z

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=6,NEIGEN=1,NEXTERNAL=7)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),
     &  P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6)
      COMPLEX*16 Z3a(6),Z3b(6),Z3c(6),Z3d(6),W7(6),W8(6),W9(6),W10(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV
      REAL*8 xl,xr,kl,kr
      COMMON /tcoupF/ xl,xr,kl,kr
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Couplings and other

      INTEGER IDIM4,IDIM5
      REAL*8 cw,gcw,gcwsw2,gwwz
      REAL*8 GWF(2),GZll(2),GZtq(2),GG(2),GZuu(2),GZdd(2),GZvv(2)
      COMPLEX*16 G2Ztq(2),G2Zqt(2),G2Gtq(2),G2Gqt(2)
      REAL*8 F1MASS,F2MASS
      COMPLEX*16 Adum

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/
      DATA EIGEN_VEC(3,1) /-1d0/
      DATA EIGEN_VEC(4,1) /-1d0/
      DATA EIGEN_VEC(5,1) /-1d0/
      DATA EIGEN_VEC(6,1) /-1d0/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2
      gwwz=g*cW

      INCLUDE 'input/coupling.inc'

      IDIM4 = 0
      IF (xL**2+xR**2 .GT. 0d0) IDIM4 = 1
      IDIM5 = 0
      IF (kL**2+kR**2 .GT. 0d0) IDIM5 = 1
      
      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)              ! q

!     top decay to W+ b

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)            ! nu
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)        ! e+
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)             ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)            ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)             ! t

!     Z decay to e+ e-

      CALL IXXXXX(P4a,F2MASS,NHEL(6),-1,W4a)        ! e+
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)         ! e-
      CALL JIOXXX(W4a,W4b,GZll,MZ,GZ,W4)            ! Z

      IF (IMOD .EQ. 1) THEN     ! Z anomalous

      AMP(1)=0d0
      AMP(2)=0d0
      AMP(3)=0d0
      CALL FVOXXX(W3,W1,GG,mt,Gt,W5)
      CALL FVIXXX(W2,W1,GG,0d0,0d0,W6)
      IF (IDIM4 .EQ. 1) THEN
        CALL IOVXXX(W2,W5,W4,GZtq,AMP(1))
        CALL IOVXXX(W6,W3,W4,GZtq,AMP(2))
      ENDIF
      IF (IDIM5 .EQ. 1) THEN
        CALL IOVSmX(W2,W5,W4,G2Zqt,-1,Adum)
        AMP(1)=AMP(1)+Adum
        CALL IOVSmX(W6,W3,W4,G2Zqt,-1,Adum)
        AMP(2)=AMP(2)+Adum
      ENDIF

!     Diagram with gluon splitting to b bbar

      CALL FVOXXX(W3c,W1,GG,mb,0d0,Z3c)
      CALL FVOXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t
      IF (IDIM4 .EQ. 1) THEN
        CALL IOVXXX(W2,W7,W4,GZtq,AMP(3))
      ENDIF
      IF (IDIM5 .EQ. 1) THEN
      CALL IOVSmX(W2,W7,W4,G2Zqt,-1,Adum)
      AMP(3)=AMP(3)+Adum
      ENDIF
      AMP(4)=0d0
      AMP(5)=0d0
      AMP(6)=0d0

      ELSE                      ! Gluon anomalous

      CALL FVIXXX(W2,W4,GZuu,0d0,0d0,W5)
      CALL IOVSmX(W5,W3,W1,G2Gqt,-1,AMP(1))
      CALL FVOXXX(W3,W4,GZuu,mt,Gt,W6)
      CALL IOVSmX(W2,W6,W1,G2Gqt,-1,AMP(2))

!     Diagrams with Z radiation off b, W, e and nu

      CALL FVOXXX(W3c,W4,GZdd,mb,0d0,Z3c)          ! b radiating Z
      CALL FVOXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t
      CALL IOVSmX(W2,W7,W1,G2Gqt,-1,AMP(3))

      CALL JVVXXX(W3d,W4,gwwz,MW,GW,Z3d)           ! W radiating Z
      CALL FVOXXX(W3c,Z3d,GWF,mt,Gt,W8)            ! t
      CALL IOVSmX(W2,W8,W1,G2Gqt,-1,AMP(4))

      CALL FVIXXX(W3b,W4,GZll,F1MASS,0d0,Z3b)      ! e+ radiating Z
      CALL JIOXXX(Z3b,W3a,GWF,MW,GW,Z3d)           ! W+
      CALL FVOXXX(W3c,Z3d,GWF,mt,Gt,W9)            ! t
      CALL IOVSmX(W2,W9,W1,G2Gqt,-1,AMP(5))

      CALL FVOXXX(W3a,W4,GZvv,0d0,0d0,Z3a)         ! nu radiating Z
      CALL JIOXXX(W3b,Z3a,GWF,MW,GW,Z3d)           ! W+
      CALL FVOXXX(W3c,Z3d,GWF,mt,Gt,W10)           ! t
      CALL IOVSmX(W2,W10,W1,G2Gqt,-1,AMP(6))
      ENDIF

      GQ_TZ = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_TZ =GQ_TZ + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP)
      ENDDO
      END



      REAL*8 FUNCTION GQ_TBZ(P1,P2,P3a,P3b,P3c,P4a,P4b,NHEL)

!     P1 = g    P2 = q~    P3 = t~    P4 = Z

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=6,NEIGEN=1,NEXTERNAL=7)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),
     &  P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6)
      COMPLEX*16 Z3a(6),Z3b(6),Z3c(6),Z3d(6),W7(6),W8(6),W9(6),W10(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV
      REAL*8 xl,xr,kl,kr
      COMMON /tcoupF/ xl,xr,kl,kr
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Couplings and other

      INTEGER IDIM4,IDIM5
      REAL*8 cw,gcw,gcwsw2,gwwz
      REAL*8 GWF(2),GZll(2),GZtq(2),GG(2),GZuu(2),GZdd(2),GZvv(2)
      COMPLEX*16 G2Ztq(2),G2Zqt(2),G2Gtq(2),G2Gqt(2)
      REAL*8 F1MASS,F2MASS
      COMPLEX*16 Adum

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /1d0/
      DATA EIGEN_VEC(2,1) /1d0/
      DATA EIGEN_VEC(3,1) /1d0/
      DATA EIGEN_VEC(4,1) /1d0/
      DATA EIGEN_VEC(5,1) /1d0/
      DATA EIGEN_VEC(6,1) /1d0/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2
      gwwz=g*cW

      INCLUDE 'input/coupling.inc'

      IDIM4 = 0
      IF (xL**2+xR**2 .GT. 0d0) IDIM4 = 1
      IDIM5 = 0
      IF (kL**2+kR**2 .GT. 0d0) IDIM5 = 1
      
      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)      ! g
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)       ! q~

!     antitop decay to W- bbar

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)               ! nu~
      CALL OXXXXX(P3b,F1MASS,NHEL(4),1,W3b)             ! e-
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                ! W-
      CALL FVIXXX(W3c,W3d,GWF,mt,Gt,W3)                 ! t~

!     Z decay to e+ e-

      CALL IXXXXX(P4a,F2MASS,NHEL(6),-1,W4a)            ! e+
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)             ! e-
      CALL JIOXXX(W4a,W4b,GZll,MZ,GZ,W4)                ! Z

      IF (IMOD .EQ. 1) THEN     ! Z anomalous

      AMP(1)=0d0
      AMP(2)=0d0
      AMP(3)=0d0
      CALL FVIXXX(W3,W1,GG,mt,Gt,W5)
      CALL FVOXXX(W2,W1,GG,0d0,0d0,W6)
      IF (IDIM4 .EQ. 1) THEN
        CALL IOVXXX(W5,W2,W4,GZtq,AMP(1))
        CALL IOVXXX(W3,W6,W4,GZtq,AMP(2))
      ENDIF
      IF (IDIM5 .EQ. 1) THEN
        CALL IOVSmX(W5,W2,W4,G2Ztq,1,Adum)
        AMP(1)=AMP(1)+Adum
        CALL IOVSmX(W3,W6,W4,G2Ztq,1,Adum)
        AMP(2)=AMP(2)+Adum
      ENDIF

!     Diagram with gluon splitting to b bbar

      CALL FVIXXX(W3c,W1,GG,mb,0d0,Z3c)
      CALL FVIXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t~
      IF (IDIM4 .EQ. 1) THEN
        CALL IOVXXX(W7,W2,W4,GZtq,AMP(3))
      ENDIF
      IF (IDIM5 .EQ. 1) THEN
        CALL IOVSmX(W7,W2,W4,G2Ztq,1,Adum)
      AMP(3)=AMP(3)+Adum
      ENDIF
      AMP(4)=0d0
      AMP(5)=0d0
      AMP(6)=0d0

      ELSE                      ! Gluon anomalous

      CALL FVOXXX(W2,W4,GZuu,0d0,0d0,W5)
      CALL IOVSmX(W3,W5,W1,G2Gtq,1,AMP(1))
      CALL FVIXXX(W3,W4,GZuu,mt,Gt,W6)
      CALL IOVSmX(W6,W2,W1,G2Gtq,1,AMP(2))

!     Diagrams with Z radiation off b, W, e and nu

      CALL FVIXXX(W3c,W4,GZdd,mb,0d0,Z3c)          ! b~ radiating Z
      CALL FVIXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t~
      CALL IOVSmX(W7,W2,W1,G2Gtq,1,AMP(3))

      CALL JVVXXX(W4,W3d,gwwz,MW,GW,Z3d)           ! W radiating Z
      CALL FVIXXX(W3c,Z3d,GWF,mt,Gt,W8)            ! t~
      CALL IOVSmX(W8,W2,W1,G2Gtq,1,AMP(4))

      CALL FVOXXX(W3b,W4,GZll,F1MASS,0d0,Z3b)      ! e- radiating Z
      CALL JIOXXX(W3a,Z3b,GWF,MW,GW,Z3d)           ! W-
      CALL FVIXXX(W3c,Z3d,GWF,mt,Gt,W9)            ! t~
      CALL IOVSmX(W9,W2,W1,G2Gtq,1,AMP(5))

      CALL FVIXXX(W3a,W4,GZvv,0d0,0d0,Z3a)         ! nu~ radiating Z
      CALL JIOXXX(Z3a,W3b,GWF,MW,GW,Z3d)           ! W-
      CALL FVIXXX(W3c,Z3d,GWF,mt,Gt,W10)           ! t~
      CALL IOVSmX(W10,W2,W1,G2Gtq,1,AMP(6))

      ENDIF

      GQ_TBZ = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_TBZ =GQ_TBZ + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END
