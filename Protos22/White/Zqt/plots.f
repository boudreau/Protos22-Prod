      SUBROUTINE PLOTINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Output

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Local variables

      INTEGER j,k
      REAL*8 bin,value
!                   MW   mt1  MZ   mt2   mtt  PTZ  angles
      DATA varmin  / 50.,120., 50.,120., 200.,  0.,-1.,-1.,-1.,-1./
      DATA varmax  /110.,240.,120.,240.,2000.,500., 1., 1., 1., 1./
      DATA nbintot /  60,  60,  70,  60,   90,  50, 40, 40, 40, 40/

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO k=1,nvar
        DO j=1,maxbins
          varsig(k,j)=0d0
        ENDDO
      ENDDO
      
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO k=1,nvar
        IF (NRUNS .EQ. 1) THEN
          OPEN (66,file='plots/'//procname(1:l)//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ELSE
          OPEN (66,file='plots/'//procname(1:l)//'-r'
     &      //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'-'
     &      //char(48+k/10)//char(48+MOD(k,10)))
        ENDIF
        DO j=1,nbintot(k)
          bin=varmin(k)+(FLOAT(j)-1d0)*
     &    (varmax(k)-varmin(k))/float(nbintot(k))
          value=varsig(k,j)
          WRITE (66,1000) bin,value
        ENDDO
        CLOSE (66)
      ENDDO
      RETURN

12    PRINT *,'Wrong IMODE'
      STOP
1000  FORMAT (' ',D10.4,' ',D10.4)
      END




      SUBROUTINE ASINI(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'
      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     For statistics

      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     For logging

      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     Local

      INTEGER i
      REAL*8 ERRp(n_a),ERRm(n_a),err_asym(n_a)
      LOGICAL GOODASYM(n_a)
      REAL*8 beta,Ap,Am,FL,F0,FR

      DATA asym_cent /0.,-0.5874,0.5874,0.,0.,0./

!     Initialise

      IF (IMODE .NE. -1) GOTO 11

      DO i=1,n_a
        SIGp(i)=0d0
        SIGm(i)=0d0
        SIG2p(i)=0d0
        SIG2m(i)=0d0
      ENDDO

      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      DO i=1,n_a
        IF (SIGp(i)+SIGm(i) .GT. 0) THEN
        GOODASYM(i)=.TRUE.
          ERRp(i)=SQRT(SIG2p(i)-SIGp(i)**2/FLOAT(NIN))
          ERRm(i)=SQRT(SIG2m(i)-SIGm(i)**2/FLOAT(NIN))
          asym(i)=(SIGp(i)-SIGm(i))/(SIGp(i)+SIGm(i))
          err_asym(i)=2d0/(SIGp(i)+SIGm(i))**2*
     .      SQRT((SIGm(i)*ERRp(i))**2+(SIGp(i)*ERRm(i))**2)
        ELSE
          GOODASYM(i)=.FALSE.
          asym(i)=0d0
        ENDIF      
      ENDDO
      RETURN

12    IF (IMODE .NE. 2) GOTO 13

      beta=2**(1d0/3d0)-1d0

      PRINT 1150,asym(1),err_asym(1)
      PRINT 1151,asym(2),err_asym(2)
      PRINT 1152,asym(3),err_asym(3)

      Ap=asym(2)
      Am=asym(3)
      FR=1d0/(1d0-beta)+(Am-beta*Ap)/(3d0*beta*(1d0-beta**2))
      FL=1d0/(1d0-beta)-(Ap-beta*Am)/(3d0*beta*(1d0-beta**2))
      F0=-(1d0+beta)/(1d0-beta)+(Ap-Am)/(3d0*beta*(1d0-beta))
      PRINT 1180,FL,F0,FR

      PRINT 1153,asym(4),err_asym(4)
      PRINT 1154,asym(5),err_asym(5)

      PRINT 999
      RETURN

13    PRINT *,'Wrong IMODE'
      STOP
999   FORMAT ('')
1150  FORMAT ('AFB W  ',F8.5,' +- ',F8.5,' (MC)')
1151  FORMAT ('A+  W  ',F8.5,' +- ',F8.5,' (MC)')
1152  FORMAT ('A-  W  ',F8.5,' +- ',F8.5,' (MC)')
1153  FORMAT ('AFB t  -> Zq  ',F8.5,' +- ',F8.5,' (MC)')
1154  FORMAT ('AFB tb -> Zq  ',F8.5,' +- ',F8.5,' (MC)')
1180  FORMAT ('--->  FL ~ ',F8.5,'   F0 ~ ',F8.5,'   FR ~ ',F8.5)
      END





      SUBROUTINE ADDEV(WT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      REAL*8 WT

!     External functions used

      REAL*8 COSVEC,DOT

!     Variables needed

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3),P5(0:3),P6(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4,P5,P6
      REAL*8 Pt1(0:3),Pt2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT/ Pt1,Pt2,PB1,PB2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      INTEGER IFCN_T,IFCN_TB
      COMMON /tflagF/ IFCN_T,IFCN_TB

!     Range of fariables

      REAL*8 varmin(nvar),varmax(nvar)
      INTEGER nbintot(nvar)
      COMMON /PLOTRNG/ varmin,varmax,nbintot
      REAL*8 varsig(nvar,maxbins)
      COMMON /PLOTDATA/ varsig

!     Asymmetry information data

      REAL*8 asym_cent(n_a)
      COMMON /ASYMRNG/ asym_cent
      REAL*8 SIGp(n_a),SIGm(n_a),SIG2p(n_a),SIG2m(n_a)
      COMMON /ASYMDATA/ SIGp,SIGm,SIG2p,SIG2m

!     Local kinematical quantities

      REAL*8 Pl(0:3),Pnu(0:3),Pb(0:3),Plp(0:3),Plm(0:3),Pq(0:3)
      REAL*8 PWrec(0:3),PZrec(0:3),Pt1rec(0:3),Pt2rec(0:3)
      REAL*8 cos_lW,cos_lZ,cospsi,PTZ
      
!     Dummy variables

      INTEGER i,nbin
      REAL*8 var(nvar),val,quant(n_a)
      REAL*8 pdum1(0:3),pdum2(0:3)
      REAL*8 Pboo(0:3)

      DO i=1,n_a
        quant(i)=asym_cent(i)
      ENDDO
      DO i=1,nvar
        var(i)=varmin(i)
      ENDDO

      IF (IFCN_T .EQ. 0) THEN
        CALL STORE(P1,Pnu)
        CALL STORE(P2,Pl)
        CALL STORE(P3,Pb)
        CALL STORE(P4,Plp)
        CALL STORE(P5,Plm)
        CALL STORE(P6,Pq)
      ELSE
        CALL STORE(P1,Plm)
        CALL STORE(P2,Plp)
        CALL STORE(P3,Pq)
        CALL STORE(P4,Pnu)
        CALL STORE(P5,Pl)
        CALL STORE(P6,Pb)
      ENDIF

      CALL SUMVEC(Pl,Pnu,PWrec)
      var(1)=SQRT(DOT(PWrec,PWrec))
      CALL SUMVEC(PWrec,Pb,Pt1rec)
      var(2)=SQRT(DOT(Pt1rec,Pt1rec))

      CALL SUMVEC(Plp,Plm,PZrec)
      var(3)=SQRT(DOT(PZrec,PZrec))
      CALL SUMVEC(PZrec,Pq,Pt2rec)
      var(4)=SQRT(DOT(Pt2rec,Pt2rec))

      CALL SUMVEC(Pt1rec,Pt2rec,Pdum1)
      var(5)=SQRT(DOT(Pdum1,Pdum1))

      PTZ=SQRT(PZrec(1)**2+PZrec(2)**2)
      var(6)=PTZ

      cospsi=COSVEC(Pt1,Q1)
      var(7)=cospsi

!     lW distribution

      Pboo(0)=PWrec(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PWrec(i)
      ENDDO
      CALL BOOST(Pboo,Pl,pdum1)
      CALL BOOST(Pboo,Pb,pdum2)
      cos_lW=-COSVEC(pdum1,pdum2)

      quant(1)=cos_lW                 ! A_FB
      quant(2)=cos_lW                 ! A+
      quant(3)=cos_lW                 ! A-
      var(8)=cos_lW

!     lZ distribution

      Pboo(0)=PZrec(0)                                ! LAB to W rest frame
      DO i=1,3
        Pboo(i)=-PZrec(i)
      ENDDO
      CALL BOOST(Pboo,Plp,pdum1)
      CALL BOOST(Pboo,Pq,pdum2)
      cos_lZ=-COSVEC(pdum1,pdum2)

      IF (IFCN_T .EQ. 1) THEN
      quant(4)=cos_lZ                 ! A_FB
      var(9)=cos_lZ
      ELSE
      quant(5)=cos_lZ                 ! A_FB
      var(9)=-cos_lZ
      ENDIF

      DO i=1,nvar
      val=var(i)
      nbin=INT((val-varmin(i))/(varmax(i)-varmin(i))*
     &  FLOAT(nbintot(i)))+1
      IF (nbin .LT. 1) nbin=1
      IF (nbin .GT. nbintot(i)) nbin=nbintot(i)
      varsig(i,nbin)=varsig(i,nbin)+WT
      ENDDO

      DO i=1,n_a
        IF (quant(i) .GT. asym_cent(i)) THEN
          SIGp(i)=SIGp(i)+WT
          SIG2p(i)=SIG2p(i)+WT**2
        ELSE IF (quant(i) .LT. asym_cent(i)) THEN
          SIGm(i)=SIGm(i)+WT
          SIG2m(i)=SIG2m(i)+WT**2
        ENDIF
      ENDDO
      RETURN
      END







