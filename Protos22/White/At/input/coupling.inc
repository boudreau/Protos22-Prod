      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

c      GWF(1)=-g/2d0
c      GWF(2)=-g/2d0

      GG(1)=-gs
      GG(2)=-gs

      GAuu(1)=-2d0/3d0*e         ! Auu
      GAuu(2)=-2d0/3d0*e

      GAdd(1)=1d0/3d0*e          ! Add
      GAdd(2)=1d0/3d0*e

      GAee(1)=e                  ! Aee
      GAee(2)=e

      G2Atq(1)=-e/mt*laL         ! t in q out: top decay
      G2Atq(2)=-e/mt*laR
      G2Aqt(1)=-G2Atq(2)         ! t out q in: antitop decay
      G2Aqt(2)=-G2Atq(1)

!     two factor to keep standard colour factors for lambda/2 in QCD vertex 
!     (anomalous operator has lambda)

      G2Gtq(1)=-2d0*gs/mt*laL    ! t in q out: top decay / tbar production
      G2Gtq(2)=-2d0*gs/mt*laR
      G2Gqt(1)=-G2Gtq(2)         ! t out q in: antitop decay / t production
      G2Gqt(2)=-G2Gtq(1)

