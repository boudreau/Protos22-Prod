      SUBROUTINE CHGPAR(id,step)
      IMPLICIT NONE

!     Arguments

      INTEGER id
      REAL*8 step

!     Parameters that can be changed

      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 lal,lar
      COMMON /tcoupF2/ lal,lar
      REAL*8 PTAmin,MtAcut
      COMMON /MATCH/ PTAmin,MtAcut

      IF (id .EQ. 1) THEN
        laL=laL+step
      ELSE IF (id .EQ. 2) THEN
        laR=laR+step
      ELSE IF (id .EQ. 3) THEN
        PTAmin=PTAmin+step
      ELSE IF (id .EQ. -1) THEN
        idum0=idum0+1
      ELSE
        print *,id
        PRINT 100
        STOP
      ENDIF

      RETURN

100   FORMAT ('Unsupported parameter scan')
      END


      SUBROUTINE LOGINI(IMODE,id)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE,id

!     Parameters

      REAL*8 lal,lar
      COMMON /tcoupF2/ lal,lar
      REAL*8 PTAmin,MtAcut
      COMMON /MATCH/ PTAmin,MtAcut

!     For logging

      REAL*8 SIG_tot,Rt
      COMMON /logextra/ SIG_tot,Rt
      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l

!     Local

      REAL*8 scpar
c      INTEGER i

      IF (IMODE .EQ. -1) THEN 
        OPEN (40,FILE='output/'//PROCNAME(1:l)//'.log',
     &    status='unknown')
        RETURN
      ELSE IF (IMODE .EQ. 1) THEN
        CLOSE (40)
        RETURN
      ELSE IF (IMODE .NE. 0) THEN
        PRINT 99
        STOP
      ENDIF

!     Write results

      IF ((id .EQ. 0) .OR. (id .EQ. -1)) THEN
        scpar=0d0
      ELSE IF (id .EQ. 1) THEN
        scpar=laL
      ELSE IF (id .EQ. 2) THEN
        scpar=laR
      ELSE IF (id .EQ. 3) THEN
        scpar=PTAmin
      ELSE
        PRINT 100
        STOP
      ENDIF

      WRITE (40,201) scpar,SIG_tot/(1d0+Rt),SIG_tot*Rt/(1d0+Rt)

      RETURN
99    FORMAT ('Error in WRITELOG call')
100   FORMAT ('Unsupported parameter scan')
200   FORMAT (F7.4,' ',D12.6,' ',D12.6,' ',D12.6,' ',D12.6,' ',
     &  D12.6,' ',D12.6,' ',D12.6)
201   FORMAT (F7.4,' ',D12.6,' ',D12.6)
      END


