      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF
      IF ((IMOD .EQ. 1) .AND. (IPROC .GT. NPROC/2)) THEN
        PRINT 1302,IPROC
        RETURN
      ENDIF


      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
1302  FORMAT ('Warning: IPROC = ',I2,' found in model 1, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 PTAmin,MtAcut
      COMMON /MATCH/ PTAmin,MtAcut

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),PA(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,PA
      REAL*8 Pt(0:3),PW(0:3)
      COMMON /MOMINT/ Pt,PW
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      REAL*8 MPH
      COMMON /test/ MPH

!     Local variables

      REAL*8 EPS,y1,y2,flux
      REAL*8 r,Qsq,Qmin,Qmax,y,ymin,ymax
      REAL*8 PCM_LAB(0:3)
      REAL*8 Qt,QW,F1MASS,m1,m2
      REAL*8 WT1,WT2,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC
      INTEGER ILQ

!     --------------------------------------------

      WTPS=0d0
      
!     Generation of the masses of the virtual t, W

      IF (IPROC .LE. 4) THEN           ! At

      CALL BREIT(mt,Gt,NGt,Qt,WT1)
      CALL BREIT(MW,GW,NGW,QW,WT2)
      IF (Qt .LT. QW+mb) RETURN
      WT_BREIT=WT1*WT2*(2d0*pi)**6

      ELSE                             ! direct t

      CALL BREIT(MW,GW,NGW,QW,WT1)
      WT_BREIT=WT1*(2d0*pi)**3

      ENDIF

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      IF (IPROC .LE. 4) THEN           ! At

      EPS=(Qt+MPH)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    

      ELSE                             ! direct t

      Qmin=0d0
      Qmax=MtAcut
      ymin=(atan((Qmin**2-mt**2)/(mt*Gt))/(mt*Gt))
      ymax=(atan((Qmax**2-mt**2)/(mt*Gt))/(mt*Gt))
      y=ymin+(ymax-ymin)*y2
      Qsq=mt**2+mt*Gt*tan(mt*Gt*y)
      r=Qsq/ET**2
      x1=r**(y1)                                                 
      x2=r**(1d0-y1)
      WT_PDF=ABS(LOG(r))/ET**2*(ymax-ymin)*
     &  ((Qsq-mt**2)**2+(mt*Gt)**2)

      ENDIF

      s=x1*x2*ET**2
      flux=2d0*s
      IF (SQRT(s) .LT. QW+mb+MPH) RETURN

!     Initial parton momenta in LAB system

      IDIR = MOD((IPROC-1)/2,2)        ! 0,0,1,1,0,0,1,1

      m1=0d0
      m2=0d0

      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Select fermion from W decay

      IF ((ILEP .GE. 1) .AND. (ILEP .LE. 3)) THEN   ! ILEP1 = 1,2,3: sel W -> e, mu, tau
        ILQ=ILEP
      ELSE IF (ILEP .EQ. 4) THEN                     ! ILEP1 = 4: sum W -> e,mu,tau
        y1=RAN2(idum)
        IF (y1 .LT. 1d0/3d0) THEN
          ILQ=1
        ELSE IF (y1 .LT. 2d0/3d0) THEN
          ILQ=2
        ELSE
          ILQ=3
        ENDIF
      ELSE IF (ILEP .EQ. 5) THEN
        y1=RAN2(idum)
        IF (y1 .LT. 0.5d0) THEN
          ILQ=4
        ELSE
          ILQ=5
        ENDIF
      ENDIF

      IF1=ILQ            ! W decay products

      F1MASS=0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Generation

      IF (IPROC .LE. 4) THEN            ! At

!      CALL PHASE2sym(SQRT(s),Qt,MPH,PCM_LAB,Pt,PA,WT1)
      CALL PHASE2tsym(SQRT(s),Qt,MPH,PCM_LAB,Pt,PA,WT1,1-IDIR)
      WT_PROD=(2d0*pi)**4*WT1
      CALL PHASE2(Qt,QW,mb,Pt,PW,Pb,WT)                      ! t decay
      WT_DEC=WT
      CALL PHASE2(QW,0d0,F1MASS,PW,Pn,Pl,WT)                 ! W decay
      WT_DEC=WT_DEC*WT

      ELSE                              ! direct t modelled as 
                                        ! gq -> t -> Wb; W -> f f gamma

      CALL PHASE2sym(SQRT(s),QW,mb,PCM_LAB,PW,Pb,WT)
      WT_PROD=(2d0*pi)**4*WT

      CALL PHASE3(QW,0d0,F1MASS,0d0,PW,Pn,Pl,PA,WT)          ! W decay
      WT_DEC=WT

      ENDIF
      
      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),PA(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,PA
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV
      REAL*8 PTAmin,MtAcut
      COMMON /MATCH/ PTAmin,MtAcut
      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     External functions used

      REAL*8 RLEGO,DOT

!     Local

      REAL*8 PTA,PTb,PTl,Mtot
      REAL*8 PW(0:3),Pt(0:3),Ptot(0:3)

!     --------------------------------------------

      INOT=1
      
      CALL SUMVEC(Pl,Pn,PW)
      CALL SUMVEC(PW,Pb,Pt)
      CALL SUMVEC(Pt,PA,Ptot)
      Mtot=SQRT(DOT(Ptot,Ptot))

!     Division of invariant mass regions by processes
!     for gluon anomalous coupling

      IF (IMOD .EQ. 2) THEN
      IF ((IPROC .LE. 4) .AND. (Mtot .LT. MtAcut)) RETURN
      IF ((IPROC .GE. 5) .AND. (Mtot .GT. MtAcut)) RETURN
      ENDIF

!     Kinematical cuts to remove divergences

      PTA=SQRT(PA(1)**2+PA(2)**2)
      PTb=SQRT(Pb(1)**2+Pb(2)**2)
      PTl=SQRT(Pl(1)**2+Pl(2)**2)
      IF (IMOD .EQ. 2) THEN
        IF (PTA .LT. PTAmin) RETURN
        IF (RLEGO(PA,Pl) .LT. 0.4d0) RETURN
      ENDIF

c      Pboo(0)=Pt(0)                                ! LAB to W rest frame
c      DO i=1,3
c        Pboo(i)=-Pt(i)
c      ENDDO
c      CALL BOOST(Pboo,PA,PAr)
c      IF (PAr(0) .LT. 10d0) RETURN

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 GQ_TA,GQ_TBA

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),PA(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,PA
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC
      REAL*8 MPH
      COMMON /test/ MPH

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=(mt+MPH)*QFAC

      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .LT. 0) RETURN

      IF ((IQFLAV .EQ. 1) .AND. (IPPBAR .EQ. 0)) THEN
        STR(1)=fg1*fu2          ! g  u 
        STR(2)=fg1*fus2         ! g  u~
        STR(3)=fu1*fg2          ! u  g
        STR(4)=fus1*fg2         ! u~ g
      ELSE IF ((IQFLAV .EQ. 1) .AND. (IPPBAR .EQ. 1)) THEN
        STR(1)=fg1*fus2         ! g  u 
        STR(2)=fg1*fu2          ! g  u~
        STR(3)=fu1*fg2          ! u  g
        STR(4)=fus1*fg2         ! u~ g
      ELSE
        STR(1)=fg1*fc2          ! g  c 
        STR(2)=fg1*fc2          ! g  c~
        STR(3)=fc1*fg2          ! c  g
        STR(4)=fc1*fg2          ! c~ g
      ENDIF

      STR(5)=STR(1)
      STR(6)=STR(2)
      STR(7)=STR(3)
      STR(8)=STR(4)

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IF1 .EQ. 3) IAMP=IAMP+NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 100) ntry(IAMP) = 100

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (MOD(IPROC-1,2) .EQ. 1) GOTO 12
 
!     g  q    q  g 

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GQ_TA(Q1,Q2,Pn,Pl,Pb,PA,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      
      GOTO 20

12    CONTINUE

!     g  q~    q~  g 

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GQ_TBA(Q1,Q2,Pn,Pl,Pb,PA,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END
