      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      INTEGER IQFLAV
      COMMON /QFLAV/ IQFLAV
      REAL*8 etal,etar
      COMMON /tcoupF4/ etal,etar

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry

!     Local variables

      INTEGER i,j
      REAL*8 xx,cw2,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 GFCN
      REAL*8 xW,xb,EW,qW,xH
      REAL*8 gevpb
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb                          ! Cross section in pb
        IF (ILEP .EQ. 4) THEN                  ! W all leptonic
          fact(i)=fact(i)*3d0
        ELSE IF (ILEP .EQ. 5) THEN             ! W all hadronic
          fact(i)=fact(i)*6d0 
        ENDIF
      ENDDO
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(mt)
      CALL SETALPHAS(mt)
      
!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)
      NGW=0.95*MW/GW

!     Z boson

      cW2=1d0-sW2
      cw=SQRT(cW2)
      GZ_uc=6d0*g**2/(96d0*pi*cw2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt
      NGZ=0.95*MZ/GZ

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)

      xH=MH/mt
      IF (xH .LT. 1) THEN
        GFCN = mt/(64d0*pi) * (1d0-xH**2)**2 * (etaL**2 + etaR**2)
      ELSE
        GFCN=0
      ENDIF
      NGt=0.95*mt/Gt

!     Higgs  -- not used

      GH=1d0

      PRINT 1100,mt,Gt,MH
      PRINT 1101,etaL,etaR
      PRINT 1103,GFCN/Gt
      PRINT 999
      IF (GFCN .GT. 0.01*Gt) THEN
        PRINT 1301
        PRINT 999
      ENDIF
!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO
      CALL SETPOL(istep,nhel,ncomb)
      DO i=1,4*NPROC
        DO j=1,ncomb
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

      PRINT 980,ILEP
      PRINT 981,IQFLAV
      PRINT 999

      RETURN

500   FORMAT ('______          _            ')
501   FORMAT ('| ___ \\        | |           ')           
502   FORMAT ('| |_/ / __ ___ | |_ ___  ___ ')
503   FORMAT ('|  __/ ''__/ _ \\| __/ _ \\/ __|')
504   FORMAT ('| |  | | | (_) | || (_) \\__ \\')
505   FORMAT ('\\_|  |_|  \\___/ \\__\\___/|___/')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
980   FORMAT ('Decay mode: ILEP = ',I1)
981   FORMAT ('Light quark flavour = ',I1)
999   FORMAT ('')
1100  FORMAT ('mt = ',F6.1,'   Gt = ',F7.3,'   MH = ',F6.1)
1101  FORMAT ('etaL  = ',F7.4,'   etaR  = ',F7.4)
1103  FORMAT ('Br(t -> Hq) = ',D9.4)
1301  FORMAT ('Warning!!! FCN BR larger than 1% of top width')
      END


      SUBROUTINE SETPOL(nspin,hel,nhel)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,NPART
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,NPART
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=NPART
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,NPART
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END


