      PROGRAM UNW
      IMPLICIT NONE

!     --------------------------------------------------------------------
!     Process codes used
!
!     Red:     3 = tj     4 = tbj    5 = tb    6 = tW   7 = tWb   8 = tt
!     White:   9 = Zqt   10 = Zt    11 = Aqt  12 = At  13 = gqt  14 = t
!             15 = Hqt   16 = Ht    17 = llqt 18 = tt
!     Rose:   51 = TT    52 = BB    53 = XX   54 = YY
!             61 = Tj    62 = Tbj   63 = Bj   64 = Bbj  65 = Yj   66 = Ybj
!             67 = Ttj   69 = Xtj
!     Triada: 31 = EE    32 = EN    33 = NN   36 = Zp   38 = Wp
!             41 = D2D2  42 = D2D1  43 = D1D1
!     --------------------------------------------------------------------

!     External

      REAL*8 ran2
      INTEGER LONGITUD
      INTEGER IDUM

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     Local      

      INTEGER l
      CHARACTER*100 procname
      INTEGER IPROC,nruns,irun
      INTEGER NUMEVT,NTOT

      OPEN (32,FILE='input/run.dat',status='old')
      READ (32,*) PROCNAME
      l=LONGITUD(PROCNAME)
      READ (32,*) NRUNS
      CLOSE (32)
      
      NTOT=0
      DO 200 irun=1,nruns    

      IF (nruns .EQ. 1) THEN
      FILE0i='../events/'//procname(1:l)//'.par'
      FILE1i='../events/'//procname(1:l)//'.wgt'
      FILE0o='../events/'//procname(1:l)//'_unw.par'
      FILE1o='../events/'//procname(1:l)//'.unw'
      ELSE
      FILE0i='../events/'//procname(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      FILE1i='../events/'//procname(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
      FILE0o='../events/'//procname(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'_unw.par'
      FILE1o='../events/'//procname(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.unw'
      ENDIF


      PRINT *,'Unweighting file #',irun
      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      CLOSE (36)

      NUMEVT=0
      IF ((IPROC .GE. 3) .AND. (IPROC .LE. 8)) THEN
        CALL UNW_Red(NUMEVT)
      ELSE IF ((IPROC .GE. 9) .AND. (IPROC .LE. 16)) THEN
        CALL UNW_White(NUMEVT)
      ELSE IF ((IPROC .EQ. 17) .OR. (IPROC .EQ. 18)) THEN
        CALL UNW_4F(NUMEVT)
      ELSE IF ((IPROC .GE. 51) .AND. (IPROC .LE. 54)) THEN
        CALL UNW_Rose2(NUMEVT)
      ELSE IF ((IPROC .GE. 61) .AND. (IPROC .LE. 69)) THEN
        CALL UNW_Rose1(NUMEVT)
      ELSE IF ((IPROC .GE. 31) .AND. (IPROC .LE. 36)) THEN
        CALL UNW_2HL(NUMEVT)
      ELSE IF (IPROC .EQ. 38) THEN
        CALL UNW_1HL(NUMEVT)
      ELSE IF ((IPROC .GE. 41) .AND. (IPROC .LE. 43)) THEN
        CALL UNW_DDll(NUMEVT)
      ELSE
        PRINT *,'Wrong process'
        STOP
      ENDIF
      NTOT=NTOT+NUMEVT
       

200   CONTINUE


      IF (nruns .GT. 1) PRINT *,'Total: ',NTOT
      STOP



      END 



